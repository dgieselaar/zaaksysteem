var glob = require('glob'),
	apps;

apps = glob.sync('src/*', { ignore: 'src/shared' }).map(function ( app ) {
	return app.split('src/')[1];
});

module.exports = apps;
