import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../shared/api/resource/composedReducer';
import assign from 'lodash/object/assign';
import capitalize from 'lodash/string/capitalize';
import get from 'lodash/object/get';
import shortid from 'shortid';
import isEmpty from './../../../../shared/vorm/util/isEmpty';
import './styles.scss';

export default
	angular.module('zsCaseAboutView', [
		composedReducerModule
	])
		.directive('zsCaseAboutView', [ 'composedReducer', 'dateFilter', ( composedReducer, dateFilter ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					caseResource: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						groupReducer;

					groupReducer = composedReducer( { scope }, ctrl.caseResource())
						.reduce( ( caseObj ) => {

							return [
								{
									name: 'info',
									label: 'Algemene zaakinformatie',
									fields: [
										{
											label: 'Zaaknummer',
											value: caseObj.instance.number
										},
										{
											label: 'Zaak publiekelijk in te zien',
											value: null
										},
										{
											label: 'Naam zaaktype',
											value: caseObj.instance.casetype.instance.name
										},
										{
											label: 'Zaaktypeversie',
											value: caseObj.instance.casetype.instance.version
										},
										{
											label: 'Generieke categorie',
											value: caseObj.instance.casetype.instance.generic_category
										},
										{
											label: 'Extra informatie',
											value: caseObj.instance.subject
										},
										{
											label: 'Zaakniveau',
											value: caseObj.instance.number_parent ?
												'B (Deelzaak)'
												: 'A (Hoofdzaak)'
										},
										{
											label: 'Handelingsinitiator',
											value: capitalize(caseObj.instance.casetype.instance.initiator_type)
										},
										{
											label: 'Trigger',
											value: caseObj.instance.casetype.instance.initiator_source.indexOf('extern') !== -1 ?
												'Extern'
												: 'Intern'
										},
										{
											label: 'Wettelijke grondslag',
											value: caseObj.instance.principe_local
										},
										{
											label: 'Lokale grondslag',
											value: caseObj.instance.principle_national
										},
										{
											label: 'Ontvangen via',
											value: capitalize(caseObj.instance.channel_of_contact)
										},
										{
											label: 'Verificatie aanvrager',
											value: null
										},
										{
											label: 'Verantwoordingsrelatie',
											value: caseObj.instance.casetype.instance.supervisor_relation
										},
										{
											label: 'Openbaarheid',
											value: caseObj.instance.confidentiality.mapped
										},
										{
											label: 'Afhandeltermijn wettelijk',
											value: `${caseObj.instance.lead_time_legal} werkdagen`
										},
										{
											label: 'Afhandeltermijn norm',
											value: `${caseObj.instance.lead_time_service} werkdagen`
										},
										{
											label: 'Uiterste vernietigingsdatum',
											value: caseObj.instance.date_of_destruction ?
												dateFilter(caseObj.instance.date_of_destruction, 'dd-MM-yyyy')
												: null
										},
										{
											label: 'Archiefnominatie',
											value: caseObj.instance.archival_state
										},
										{
											label: 'Procesbeschrijving',
											value: caseObj.instance.process_description
										},
										{
											label: 'Aanvrager',
											value: caseObj.instance.requestor.instance.name
										},
										{
											label: 'Zaakcoordinator',
											value: get(caseObj.instance, 'coordinator.instance.coordinator')
										},
										{
											label: 'Huidige behandelaar',
											value: get(caseObj.instance.assignee, 'instance.assignee')
										},
										{
											label: 'Registratiedatum',
											value: dateFilter(caseObj.instance.date_of_registration, 'dd-MM-yyyy')
										},
										{
											label: 'Afhandelen voor',
											value: dateFilter(caseObj.instance.date_target, 'dd-MM-yyyy')
										},
										{
											label: 'Afgehandeld op',
											value: caseObj.instance.date_of_completion ?
												dateFilter(caseObj.instance.date_of_completion, 'dd-MM-yyyy')
												: null
										},
										{
											label: 'Zaak bedrag',
											value: caseObj.instance.price
										},
										{
											label: 'Status online betaling',
											value: {
												pending: 'Wachten op bevestiging',
												success: 'Geslaagd',
												failed: 'Niet geslaagd',
												[null]: null
											}[caseObj.instance.payment_status]
										}


									]
								}
							]
								.map(group => {
									return assign(
										{ id: shortid() },
										group,
										{
											fields: group.fields.map(
												field => {
													return assign({ id: shortid() }, field, { value: isEmpty(field.value) ? '-' : field.value });
												}
											)
										}
									);
								});

						});

					ctrl.getGroups = groupReducer.data;

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
