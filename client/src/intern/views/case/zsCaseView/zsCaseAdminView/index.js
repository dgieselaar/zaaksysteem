import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../../shared/api/resource/composedReducer';
import vormFieldsetModule from './../../../../../shared/vorm/vormFieldset';
import inputModule from './../../../../../shared/vorm/types/input';
import radioModule from './../../../../../shared/vorm/types/radio';
import formModule from './../../../../../shared/vorm/types/form';
import selectModule from './../../../../../shared/vorm/types/select';
import vormRolePickerModule from './../../../../../shared/zs/vorm/vormRolePicker';
import vormObjectSuggestModule from './../../../../../shared/object/vormObjectSuggest';
import vormValidatorModule from './../../../../../shared/vorm/util/vormValidator';
import caseAttrTemplateCompilerModule from './../../../../../shared/case/caseAttrTemplateCompiler';
import zsCaseAdminAttributeListModule from './zsCaseAdminAttributeList';
import caseActions from './../../../../../shared/case/caseActions';
import actionsModule from './actions';
import mapValues from 'lodash/object/mapValues';
import indexBy from 'lodash/collection/indexBy';
import get from 'lodash/object/get';
import find from 'lodash/collection/find';
import seamlessImmutable from 'seamless-immutable';
import './styles.scss';

export default
	angular.module('zsCaseAdminView', [
		composedReducerModule,
		vormFieldsetModule,
		actionsModule,
		inputModule,
		radioModule,
		formModule,
		selectModule,
		vormRolePickerModule,
		vormObjectSuggestModule,
		vormValidatorModule,
		caseAttrTemplateCompilerModule,
		zsCaseAdminAttributeListModule
	])
		.directive('zsCaseAdminView', [ '$state', '$animate', 'composedReducer', 'vormValidator', 'caseAttrTemplateCompiler', ( $state, $animate, composedReducer, vormValidator, caseAttrTemplateCompiler ) => {

			let compiler = caseAttrTemplateCompiler.clone();

			compiler.registerType('case-admin-attribute-list', {
				control:
					angular.element(
						`<zs-case-admin-attribute-list
							ng-model
							compiler="vm.compiler()"
						>
						</zs-case-admin-attribute-list>`
					)
			});

			return {
				restrict: 'E',
				template,
				scope: {
					action: '&',
					caseResource: '&',
					casetypeResource: '&',
					user: '&',
					onSubmit: '&',
					onClose: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						submitting = false,
						values = seamlessImmutable({}).merge(
							mapValues(
								indexBy(ctrl.action().fields, 'name'),
								'defaults'
							)
						),
						needsCasetype = (ctrl.action().name === 'fase' || ctrl.action().name === 'afhandelen'),
						actionReducer,
						fieldReducer,
						validityReducer;

					actionReducer = composedReducer({ scope, waitUntilResolved: needsCasetype }, ctrl.action, ctrl.caseResource(), ctrl.casetypeResource(), ctrl.user)
						.reduce( ( action, caseObj, casetype, user ) => {

							// todo: throw error if action not available (due to permissions)

							let updatedAction;

							if (
								caseObj && (
									casetype
									|| !needsCasetype
								)
							) {
								updatedAction = find(caseActions({ caseObj, casetype, user, $state }), { name: action.name });
							}

							return updatedAction;

						});

					fieldReducer = composedReducer({ scope }, actionReducer)
						.reduce( ( action ) => {

							let fields = get(action, 'fields', []);

							return seamlessImmutable(fields).asMutable( { deep: true });
						});

					validityReducer = composedReducer({ scope }, ( ) => values, fieldReducer)
						.reduce( ( vals, fields ) => {
							let validation = vormValidator(fields, vals);

							return validation;
						});

					ctrl.getFields = fieldReducer.data;

					ctrl.isFormValid = ( ) => get(validityReducer.data(), 'valid', false);

					ctrl.getValidity = validityReducer.data;

					ctrl.getSubmitLabel = composedReducer({ scope }, ctrl.action)
						.reduce( ( action ) => action.verb || action.label)
						.data;

					ctrl.handleSubmit = ( ) => {

						let action = actionReducer.data(),
							mutation = action.mutate(values),
							promise,
							opts = typeof action.options === 'function' ? action.options(values) : action.options;

						if (!opts) {
							opts = {};
						}

						submitting = true;

						promise = ctrl.caseResource().mutate(mutation.type, mutation.data).asPromise()
							.finally(( ) => {
								submitting = false;
							});

						ctrl.onSubmit({ $promise: promise, $reload: !!opts.reloadRoute, $willRedirect: !!opts.willRedirect });

					};

					ctrl.getValues = ( ) => values;

					ctrl.handleChange = ( name, value ) => {

						let action = actionReducer.data();

						values = values.merge({ [name]: value });

						if (action.processChange) {
							values = action.processChange(name, value, values);
						}


					};

					ctrl.isSubmitting = ( ) => submitting;

					ctrl.isLoading = ( ) => {
						let isCaseResolved = ctrl.caseResource().state() === 'resolved',
							isCasetypeResolved = ctrl.casetypeResource().state() === 'resolved';

						return needsCasetype ?
							!(isCaseResolved && isCasetypeResolved)
							: !isCaseResolved;

					};

					ctrl.getCompiler = ( ) => compiler;

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
