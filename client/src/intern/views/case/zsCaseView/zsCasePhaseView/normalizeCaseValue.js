import isArray from 'lodash/lang/isArray';

export default ( val ) => {

	let normalized = val;

	if (!isArray(normalized)) {
		normalized = [ normalized ];
	}

	return normalized.map(
		value => {
			return value === undefined ? null : value;
		}
	).filter(value => value !== null);

};
