import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../../../shared/api/resource/composedReducer';
import capitalize from 'lodash/string/capitalize';
import shortid from 'shortid';
import './styles.scss';

export default
	angular.module('zsCaseResult', [
		composedReducerModule
	])
		.directive('zsCaseResult', [ 'composedReducer', ( composedReducer ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					results: '&',
					result: '&',
					onResultChange: '&',
					canChange: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this;

					ctrl.getResultOptions = composedReducer({ scope }, ctrl.result, ctrl.results)
						.reduce( ( result, results ) => {

							return (results || []).map(
								resultOption => {

									let preservation =
											resultOption.period_of_preservation < 365 ?
												`${Math.round(resultOption.period_of_preservation / 30)} maanden`
												: (resultOption.period_of_preservation >= 99999 ?
													'Bewaren'
													: `${Number((Math.floor(resultOption.period_of_preservation / 365 * 2) / 2).toFixed(1))} jaar`),
										help =
											[
												{
													label: 'Naam (omschrijving)',
													value: resultOption.label
												},
												{
													label: 'Resultaattype-generiek',
													value: capitalize(resultOption.type)
												},
												{
													label: 'Selectielijst',
													value: 	resultOption.selection_list
												},
												{
													label: 'Activeert bewaartermijn',
													value: resultOption.trigger_archival ?
														'Ja'
														: 'Nee'
												},
												{
													label: 'Archiefnominatie',
													value: resultOption.type_of_archiving
												},
												{
													label: 'Bewaartermijn',
													value: preservation
												},
												{
													label: 'Brondatum archiefprocedure',
													value: capitalize(resultOption.archive_procedure)
												},
												{
													label: 'Toelichting',
													value: resultOption.comments
												}
											];

									return {
										id: shortid(),
										value: resultOption.type,
										label: resultOption.label || capitalize(resultOption.type),
										selected: result === resultOption.type,
										help:
											help.map(
												row => `<b>${row.label}</b>: ${row.value || row.value === 0 ? row.value : '-' }`
											).join('<br/>')
									};
								}
							);

						})
						.data;

					ctrl.hasResult = ( ) => !!ctrl.result();

					ctrl.clearResult = ( ) => {
						ctrl.onResultChange( { $result: null });
					};

					ctrl.handleResultChange = ( result ) => {
						ctrl.onResultChange({ $result: result });
					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
