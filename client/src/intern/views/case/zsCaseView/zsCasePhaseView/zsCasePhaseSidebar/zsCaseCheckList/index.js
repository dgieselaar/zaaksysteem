import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../../../../shared/api/resource/composedReducer';
import shortid from 'shortid';

export default
	angular.module('zsCaseCheckList', [
		composedReducerModule
	])
		.directive('zsCaseCheckList', [ 'composedReducer', ( composedReducer ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					items: '&',
					onItemToggle: '&',
					onItemRemove: '&',
					onItemAdd: '&',
					disabled: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this;

					ctrl.newItemContent = '';

					ctrl.getItems = composedReducer( { scope: $scope }, ctrl.items, ctrl.disabled)
						.reduce( ( items, isDisabled ) => {

							return (items || []).map(item => {
								return {
									$id: shortid(),
									id: item.id,
									label: item.label,
									checked: item.checked,
									togglable: !isDisabled && true,
									deletable: !isDisabled && !!item.user_defined,
									item,
									classes: {
										disabled: isDisabled,
										checked: item.checked
									}
								};
							});
						})
						.data;

					ctrl.handleItemToggle = ( item ) => {
						ctrl.onItemToggle({ $item: item.item, $checked: !item.checked });
					};

					ctrl.handleItemRemove = ( item ) => {
						ctrl.onItemRemove({ $item: item.item });
					};

					ctrl.handleItemAdd = ( content ) => {
						ctrl.onItemAdd({ $content: content });
						ctrl.newItemContent = '';
					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
