import angular from 'angular';
import template from './template.html';
import './styles.scss';

export default
	angular.module('zsCasePhaseSidebarTabList', [
	])
		.directive('zsCasePhaseSidebarTabList', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					tabs: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
