export default ( resource, scope, valueGetter ) => {

	return resource(
		( ) => {

			let role = valueGetter().role;
			
			return role ?
				{
					url: `/zaak/${valueGetter().caseId}/update/betrokkene/suggestion`,
					params: {
						rol: role
					}
				}
				: null;
		},
		{ scope, cache: { disabled: true } }
	);

};
