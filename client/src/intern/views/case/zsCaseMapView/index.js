import angular from 'angular';
import angularUiRouter from 'angular-ui-router';
import zsMapModule from './../../../../shared/ui/zsMap';
import zsMapSearchModule from './../../../../shared/ui/zsMap/zsMapSearch';
import zsModalModule from './../../../../shared/ui/zsModal';
import composedReducerModule from './../../../../shared/api/resource/composedReducer';
import assign from 'lodash/object/assign';
import indexBy from 'lodash/collection/indexBy';
import get from 'lodash/object/get';
import first from 'lodash/array/first';
import identity from 'lodash/utility/identity';
import last from 'lodash/array/last';
import shortid from 'shortid';
import template from './template.html';
import extendedTemplate from './extendedTemplate.html';
import popupTemplate from './../../../../shared/ui/zsMap/popup-template.html';
import './styles.scss';

export default
	angular.module('zsCaseMapView', [
		angularUiRouter,
		zsMapModule,
		zsMapSearchModule,
		composedReducerModule,
		zsModalModule
	])
		.directive('zsCaseMapView', [
				'$compile', '$state', 'composedReducer', 'zsModal',
				( $compile, $state, composedReducer, zsModal ) => {

			const TYPES = [
				{
					key: 'nummeraanduiding',
					label: 'BAG Nummeraanduiding'
				},
				{
					key: 'verblijfsobject',
					label: 'BAG Verblijfsobject'
				},
				{
					key: 'pand',
					label: 'BAG Pand'
				},
				{
					key: 'openbareruimte',
					label: 'BAG Openbare ruimte'
				},
				{
					key: 'standplaats',
					label: 'BAG Standplaats'
				},
				{
					key: 'ligplaats',
					label: 'BAG Ligplaats'
				}
			];

			const FIELDS = [
				{
					key: 'human_identifier',
					label: 'Locatie'
				},
				{
					key: 'bag_id',
					label: 'Identificatie',
					format: ( value ) => last(value.split('-'))
				},
				{
					key: 'meta_data.surface',
					label: 'Oppervlakte',
					format: ( value ) => `${value} m²`
				},
				{
					key: 'meta_data.usage',
					label: 'Gebruik'
				},
				{
					key: 'meta_data.year_of_construction',
					label: 'Bouwjaar'
				}
			];

			return {
				restrict: 'E',
				template,
				scope: {
					caseResource: '&',
					showMore: '&'
				},
				bindToController: true,
				controller: [ '$rootScope', '$scope', function ( $rootScope, scope ) {

					let ctrl = this,
						locationReducer = composedReducer( { scope }, ctrl.caseResource())
							.reduce(( caseObj ) => {

								return caseObj.instance.location ?
									TYPES.map(
										type => {
											let location = caseObj.instance.location.instance[type.key];

											if (location) {
												return assign({ location }, type);
											}

											return null;
										}
									)
										.filter(identity)
									: [];

							}),
						extendedReducer = composedReducer( { scope }, locationReducer)
							.reduce(( locations ) => {

								return locations.map(
									location => {
										return {
											id: shortid(),
											label: location.label,
											fields: FIELDS.map(
												field => {

													let value = get(location.location, field.key);

													return value ?
														{
															label: field.label,
															value: field.format ? field.format(value) : value
														}
														: null;

												}
											).filter(identity)
										};
									}
								).filter(location => location.fields.length);

							}),
						markerTemplate = angular.element(popupTemplate),
						markerReducer,
						center = null;

					angular.element(markerTemplate[0].querySelector('.map-popup-body')).append(
						`<a href="${$state.href('case.location', { more: 'meer' }, { inherit: true })}" class="btn btn-flat">Meer informatie</a>`
					);

					markerReducer =
						composedReducer( { scope }, locationReducer)
							.reduce( locations => {

								let location = get(first(locations), 'location'),
									markers,
									keys = [
										{
											name: 'woonplaats',
											label: 'Stad'
										},
										{
											name: 'postcode',
											label: 'Postcode'
										},
										{
											name: 'straat',
											label: 'Straat'
										}
									];

								markers = location ?
									[
										{
											coordinates:
												indexBy(
													get(location, 'address_data.gps_lat_lon', '')
														.split(',')
														.map(val => val.replace(/[()]/, '')),
													( value, index ) => {
														return index === 0 ? 'lat' : 'lng';
													}
												),
											title: location.human_identifier,
											description:
												keys.map(
													key => {
														let value = get(location.address_data, key.name);

														return value ?
															`${key.label}: ${value}`
															: '';
													}
												)
													.filter(identity)
													.join('<br/>'),
											template: markerTemplate

										}
									]
									: [];

								return markers;

							});

					ctrl.getMarkers = markerReducer.data;

					ctrl.getLocations = extendedReducer.data;

					ctrl.getCenter = ( ) => center;

					ctrl.openModal = ( ) => {
	
						let modalScope = scope.$new(),
							modal = zsModal({
								title: 'Uitgebreide adresinformatie',
								el: $compile(extendedTemplate)(modalScope)
							});

						let close = ( ) => {
							modal.close();
							modalScope.$destroy();
						};

						modal.open();

						modal.onClose(( ) => {
							$state.go('case.location', { more: null }, { inherit: true });
							return false;
						});

						$rootScope.$on('$stateChangeSuccess', ( ) => {
							close();
						});
						
					};

					scope.$watch(( ) => ctrl.showMore(), more => {

						if (more) {
							ctrl.openModal();
						}

					});

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
