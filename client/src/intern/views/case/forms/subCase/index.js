import angular from 'angular';
import propCheck from './../../../../../shared/util/propCheck';
import get from 'lodash/object/get';
import first from 'lodash/array/first';
import identity from 'lodash/utility/identity';

export default ( data/*, options*/ ) => {

	let capabilities = {
			expand: true
		},
		subjectOptions =
			[
				{
					value: 'natuurlijk_persoon',
					label: 'Burger'
				},
				{
					value: 'bedrijf',
					label: 'Organisatie'
				}
			],
		typeOptions =
			[
				{
					value: 'deelzaak',
					label: 'Deelzaak'
				},
				{
					value: 'gerelateerd',
					label: 'Gerelateerde zaak'
				}
			];

	propCheck.throw(
		propCheck.shape({
			requestorName: propCheck.string.optional,
			phaseOptions: propCheck.array,
			isLastPhase: propCheck.bool
		}),
		data
	);

	if (data.isLastPhase) {
		typeOptions = [
			{
				value: 'vervolgzaak_datum',
				label: 'Vervolgzaak (datum)'
			},
			{
				value: 'vervolgzaak',
				label: 'Vervolgzaak (periode)'
			}
		].concat(typeOptions);
	}

	return {
		getDefaults: ( ) => {

			return {
				subcase_requestor_type: 'aanvrager',
				requestor_type: 'natuurlijk_persoon',
				type: 'deelzaak',
				resolve_in_phase: get(first(data.phaseOptions), 'value')
			};

		},
		getCapabilities: ( ) => capabilities,
		processChange: ( name, value, values ) => {

			let vals = values;

			if (name === 'requestor_type') {
				vals = vals.merge({ requestor: null });
			}

			return vals.merge({ [name]: value });

		},
		fields: ( ) => {
			return [
				{
					name: 'allocation',
					label: 'Toewijzing',
					template: 'org-unit',
					required: true
				},
				{
					name: 'subcase_requestor_type',
					label: 'Aanvrager deel-/vervolgzaak',
					template: 'select',
					data: {
						options: [
							{
								value: 'aanvrager',
								label: `Aanvrager van de huidige zaak (${data.requestorName})`
							},
							{
								value: 'anders',
								label: 'Andere aanvrager'
							},
							{
								value: 'behandelaar',
								label: 'Behandelaar van de huidige zaak'
							},
							{
								value: 'ontvanger',
								label: 'Ontvanger van de huidige zaak'
							}
						]
					},
					required: true
				},
				{
					name: 'requestor_type',
					label: 'Andere aanvrager type',
					template: 'radio',
					data: {
						options: subjectOptions
					},
					required: true,
					when: [ '$values', ( values ) => {
						return values.subcase_requestor_type === 'anders';
					}]
				},
				{
					name: 'requestor',
					label: 'Andere aanvrager',
					template: 'object-suggest',
					data: {
						objectType: [ '$values', ( values ) => values.requestor_type ]
					},
					when: [ '$values', ( values ) => {
						return values.subcase_requestor_type === 'anders';
					}]
				},
				{
					name: 'type',
					template: 'select',
					label: 'Soort',
					data: {
						options: typeOptions
					},
					when: [ 'expanded', identity ]
				},
				{
					name: 'resolve_in_phase',
					template: 'select',
					label: 'Afhandelen in fase',
					data: {
						options: data.phaseOptions
					},
					required: true,
					when: [ '$values', 'expanded', ( values, expanded ) => expanded && values.type === 'deelzaak' ]
				},
				{
					name: 'copy_attributes',
					template: 'checkbox',
					label: 'Kenmerken kopiëren',
					when: [ 'expanded', identity ]
				},
				{
					name: 'automatic_assignment',
					template: 'checkbox',
					label: 'Zaak automatisch in behandeling nemen',
					when: [ 'expanded', identity ]
				},
				{
					name: 'start_date',
					template: 'date',
					label: 'Starten na',
					when: [ '$values', 'expanded', ( vals, expanded ) => expanded && vals.type === 'vervolgzaak_datum' ],
					required: true
				},
				{
					name: 'start_after',
					template: {
						inherits: 'number',
						control: ( el ) => {
							return angular.element(
								`<div>
									${el[0].outerHTML}
									dagen
								</div>`
							);
						}
					},
					label: 'Starten na',
					when: [ '$values', 'expanded', ( vals, expanded ) => expanded && vals.type === 'vervolgzaak' ],
					required: true
				}
				// {
				// 	name: 'add_related_subject',
				// 	template: 'checkbox',
				// 	label: 'Betrokkene toevoegen',
				// 	when: [ 'expanded', identity ]
				// },
				// {
				// 	name: 'related_subject_',
				// 	template: 'radio',
				// 	label: 'Betrokkene type',
				// 	data: {
				// 		options: subjectOptions
				// 	},
				// 	when: [ '$values', 'expanded', ( values, expanded ) => expanded && !!values.add_related_subject ],
				// 	required: true
				// },
				// {
				// 	name: 'related_subject',
				// 	template: 'object-suggest',
				// 	label: 'Betrokkene',
				// 	data: {
				// 		objectType: [ '$values', ( values ) => values.related_subject_type ]
				// 	},
				// 	when: [ '$values', 'expanded', ( values, expanded ) => expanded && !!values.add_related_subject ],
				// 	required: true
				// },
				// {
				// 	name: 'related_subject_role',
				// 	template:
				// }
			];
		}
	};

};
