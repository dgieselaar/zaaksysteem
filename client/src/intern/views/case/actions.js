import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import mutationServiceModule from './../../../shared/api/resource/mutationService';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';
import seamlessImmutable from 'seamless-immutable';
import propCheck from './../../../shared/util/propCheck';
import parseAttributeValue from './../../../shared/zs/case/parseAttributeValue';
import get from 'lodash/object/get';
import mapValues from 'lodash/object/mapValues';
import indexBy from 'lodash/collection/indexBy';
import assign from 'lodash/object/assign';
import find from 'lodash/collection/find';
import mapKeys from 'lodash/object/mapKeys';
import confidentiality from './../../../shared/zs/case/confidentiality';
import omit from 'lodash/object/omit';
import values from 'lodash/object/values';
import pick from 'lodash/object/pick';
import identity from 'lodash/utility/identity';
import capitalize from 'lodash/string/capitalize';
import shortid from 'shortid';

let edit = ( type, caseObj, id, data ) => {
	return caseObj.merge({
		case: {
			[type]: {
				by_milestone:
					mapValues(
						caseObj.case[type].by_milestone,
						( items ) => {
							return items.map(
								item => {

									if (item.id === id) {
										return item.merge(data);
									}

									return item;

								}
							);
						}
					)
			}
		}
	}, { deep: true });
};

let editAction = ( caseObj, id, data ) => {

	return edit('case_actions', caseObj, id, data);

};

let editChecklist = ( caseObj, id, data ) => {

	return edit('checklist', caseObj, id, data);

};

export default
	angular.module('zsCaseViewActions', [
		mutationServiceModule,
		snackbarServiceModule,
		angularUiRouterModule
	])
		.factory('zsCaseViewActions', [ '$q', '$state', 'snackbarService', ( $q, $state, snackbarService ) => {

			return [
				{
					type: 'CASE_ATTRIBUTE_UPDATE',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								phaseId: propCheck.number,
								updates: propCheck.arrayOf(propCheck.shape({
									name: propCheck.string,
									value: propCheck.any,
									attribute: propCheck.object,
									implied: propCheck.object
								}))
							}),
							mutationData
						);

						let attrChanges =
								mapValues(
									indexBy(mutationData.updates, ( update ) => `attribute.${update.name}`),
									( update ) => {
										return parseAttributeValue(update.attribute, update.value);
									}
								),
							impliedChanges =
								indexBy(
									assign({}, ...(mutationData.updates.map(update => update.implied))),
									( value, key ) => `attribute.${key}`
								);

						return {
							url: `/api/case/${mutationData.caseId}/attribute/save`,
							data: {
								fields:
									assign(
										attrChanges,
										impliedChanges
									),
								phases: [ mutationData.phaseId.toString() ]
							}
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(caseObj => {
							return caseObj.merge({
								values:
									assign(
										mapValues(
											indexBy(mutationData.updates, ( update ) => `attribute.${update.name}`),
											( update ) => update.value
										),
										indexBy(
											assign({}, ...(mutationData.updates.map(update => update.implied))),
											( value, key ) => `attribute.${key}`
										)
									)
									
							}, { deep: true });
						});

					},
					wait: ( mutationData, promise ) => {

						return snackbarService.wait(
							'Bezig met het opslaan van de kenmerken',
							{
								collapse: 0,
								promise,
								then: ( ) => '',
								catch: ( ) => 'Niet alle wijzigingen konden worden opgeslagen. Neem contact op met uw beheerder voor meer informatie.'
							}
						);

					}
				},
				{
					type: 'CASE_FILE_REMOVE',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								attributeName: propCheck.string,
								fileId: propCheck.string
							}),
							mutationData
						);

						return {
							url: '/api/bulk/file/update',
							data: {
								files: {
									[mutationData.fileId]: {
										action: 'update_properties',
										data: {
											deleted: true
										}
									}
								}
							}
						};

					},
					reduce: ( data, mutationData ) => {

						let key = `case.${mutationData.attributeName}`;

						return data.map(
							caseObj => {
								return seamlessImmutable(caseObj)
									.merge(
									{
										values: {
											[key]:
												(get(caseObj, key) || [])
													.filter(file => file.uuid !== mutationData.fileId)
										}
									},
									{ deep: true });
							}
						);
					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'CASE_CONFIDENTIALITY_CHANGE',
					request: ( mutationData ) => {

						let data;

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								confidentiality: propCheck.oneOf(confidentiality.map(option => option.name))
							}),
							mutationData
						);

						data = {
							fields: {
								'case.confidentiality': mutationData.confidentiality
							}
						};

						return {
							url: `/api/case/${mutationData.caseId}/attribute/save`,
							data
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(caseObj => {

							let option = find(confidentiality, { name: mutationData.confidentiality });

							return caseObj.merge({
								values: {
									'case.confidentiality': {
										mapped: option.label,
										original: option.name
									}
								}
							}, { deep: true });

						});

					}
				},
				{
					type: 'CASE_RESULT_CHANGE',
					request: ( mutationData ) => {

						let data;

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								result: propCheck.string
							}),
							mutationData
						);

						data = {
							fields: {
								'case.result': mutationData.result || ''
							}
						};

						return {
							url: `/api/case/${mutationData.caseId}/attribute/save`,
							data
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {

								return caseObj.merge(
									{
										values: {
											'case.result': mutationData.result
										}
									},
									{ deep: true }
								);

							}
						);

					}
				},
				{
					type: 'CASE_ACTION_UPDATE',
					request: ( mutationData ) => {

						let data;

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								actionId: propCheck.number,
								automatic: propCheck.bool.optional,
								data: propCheck.object.optional
							}),
							mutationData
						);

						data = {
							automatic: mutationData.automatic,
							id: mutationData.actionId
						};

						return {
							url: `/zaak/${mutationData.caseId}/action/update`,
							data
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {
								return editAction(
									caseObj,
									mutationData.actionId,
									{ automatic: mutationData.automatic, tainted: true }
								);
							}
						);

					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'CASE_ACTION_UNTAINT',
					request: ( mutationData ) => {

						let data;

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								actionId: propCheck.number
							}),
							mutationData
						);

						data = {
							id: mutationData.actionId
						};

						return {
							url: `/zaak/${mutationData.caseId}/action/untaint`,
							data
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {
								return editAction(
									caseObj,
									mutationData.actionId,
									{ tainted: false }
								);
							}
						);

					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'CASE_ACTION_TRIGGER',
					request: ( mutationData ) => {

						let data;

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								actionId: propCheck.number,
								data: propCheck.object,
								type: propCheck.string,
								trigger: propCheck.oneOf( ['save', 'execute' ] )
							}),
							mutationData
						);

						data = {
							id: mutationData.actionId,
							update: 1,
							trigger: mutationData.trigger === 'save' ?
								'commit'
								: 'execute'
						};

						switch (mutationData.type) {
							default:
							data = assign(data, mutationData.data);
							break;

							case 'case':
							data = assign(data, mapKeys(mutationData.data, ( value, key ) => `relaties_${key}`));
							break;

							case 'email':
							data = assign(data, mapKeys(mutationData.data, ( value, key ) => `notificaties_${key}`));
							break;
						}

						return {
							url: `/zaak/${mutationData.caseId}/action/data/`,
							data
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {
								return editAction(
									caseObj,
									mutationData.actionId,
									{ tainted: true, data: mutationData.data }
								);
							}
						);

					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'CASE_CHECKLIST_ADD',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								label: propCheck.string,
								milestone: propCheck.number
							}),
							mutationData
						);

						return {
							url: `/zaak/${mutationData.caseId}/checklist/add_item`,
							data: {
								label: mutationData.label,
								milestone: mutationData.milestone
							}
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {
								return caseObj.merge({
									case: {
										checklist: {
											by_milestone: {
												[String(mutationData.milestone)]:
													caseObj.case.checklist.by_milestone[String(mutationData.milestone)].concat(
														{
															checked: false,
															label: mutationData.label,
															user_defined: true
														}
													)
											}
										}
									}
								}, { deep: true });
							}
						);
					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'CASE_CHECKLIST_REMOVE',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								itemId: propCheck.number
							}),
							mutationData
						);

						return {
							url: `/zaak/${mutationData.caseId}/checklist/remove_item`,
							data: {
								item_id: mutationData.itemId
							}
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {
								return caseObj.merge({
									case: {
										checklist: {
											by_milestone:
												mapValues(
													caseObj.case.checklist.by_milestone,
													( items/*, milestone*/ ) => {
														return items.filter(
															item => item.id !== mutationData.itemId
														);
													}
												)
										}
									}
								}, { deep: true });
							}
						);

					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'CASE_CHECKLIST_TOGGLE',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								itemId: propCheck.number,
								checked: propCheck.bool
							}),
							mutationData
						);

						return {
							url: `/zaak/${mutationData.caseId}/checklist/update`,
							data: {
								data: {
									[mutationData.itemId]: mutationData.checked
								}
							}
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {
								return editChecklist(caseObj, mutationData.itemId, { checked: mutationData.checked });
							}
						);
					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'CASE_ADVANCE',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								nextPhaseName: propCheck.any,
								redirect: propCheck.shape({
									resolved: propCheck.bool,
									allocationChanged: propCheck.bool
								})
							}),
							mutationData
						);

						return {
							url: `/zaak/${mutationData.caseId}/status/advance`,
							data: {
								nowrapper: 1
							}
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {
								return caseObj.merge({
									values: {
										'case.phase': mutationData.nextPhaseName
									}
								}, { deep: true });
							}
						);

					},
					options: {
						reloadOnComplete: true
					},
					wait: ( mutationData, promise ) => {

						let objName = mutationData.nextPhaseName ? 'fase' : 'zaak',
							redirectReasons = pick(mutationData.redirect, identity),
							shouldRedirect = values(redirectReasons).length > 0,
							message = `${capitalize(objName)} is succesvol afgerond`;

						if (shouldRedirect) {
							message = redirectReasons.resolved ?
								'De zaak is afgehandeld'
								: `De toewijzing is gewijzigd`;

							message += '. U bent doorverwezen naar het dashboard.';
						}

						return snackbarService.wait(`De ${objName} wordt afgerond`, {
							promise,
							then: ( ) => message,
							catch: ( data ) => {

								let result = get(data, 'data.result[0].data[0]', {}),
									messages = {
										checklist: 'Checklist',
										documents: 'Documenten',
										fields: 'Kenmerken',
										object_mutations: 'Objectmutaties',
										owner: 'Eigenaar',
										pip_updates: 'Wijzigingsverzoeken',
										result: 'Resultaat',
										subcases: 'Deelzaken'
									},
									errors,
									msg = `De ${objName} kon niet worden afgerond`;

								errors =
									values(
										mapValues(
											pick(result, ( value ) => value === 0),
											( value, key ) => {
												return messages[key.replace('_complete', '')];
											}
										)
									);


								if (errors.length) {
									msg += ' De volgende onderdelen zijn niet of niet correct ingevuld:<br/><br/>';
									msg += values(errors).map(error => {
										return `- ${error}<br/>`;
									}).join('');
								}

								return msg;

							}
						})
							.then( ( ) => {

								if (shouldRedirect) {
									return $state.go('home');
								}

								return $q.when();

							});

					}
				},
				{
					type: 'CASE_PENDING_RESOLVE',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								bibliotheekKenmerkenId: propCheck.string,
								accepted: propCheck.bool,
								name: mutationData.accepted ? propCheck.string : propCheck.any.optional,
								value: propCheck.any.optional
							}),
							mutationData
						);

						return {
							url: `/api/case/attributeupdaterequest/${mutationData.accepted ? 'approve' : 'reject' }`,
							data: {
								case_id: mutationData.caseId,
								bibliotheek_kenmerken_id: mutationData.bibliotheekKenmerkenId
							}
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj	=> {

								let merged =
									caseObj.setIn(
										[ 'case', 'pending_changes' ],
										omit(
											caseObj.case.pending_changes,
											mutationData.bibliotheekKenmerkenId
										)
									);

								if (mutationData.accepted) {
									merged = merged.merge({
										values: merged.values.merge({
											[`attribute.${mutationData.name}`]: mutationData.value
										})
									});
								}

								return merged;
							}
						);

					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'CASE_ATTRIBUTE_REQUEST_UPDATE',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								catalogueId: propCheck.string,
								value: propCheck.any,
								description: propCheck.string.optional,
								requestorId: propCheck.string,
								requestorName: propCheck.string
							}),
							mutationData
						);

						return {
							url: `/api/case/${mutationData.caseId}/request_attribute_update/${mutationData.catalogueId}`,
							data: {
								[`kenmerk_id_${mutationData.catalogueId}`]: mutationData.value,
								toelichting: mutationData.description,
								update: `kenmerk_id_${mutationData.catalogueId}`
							}
						};

					},
					reduce: ( data, mutationData ) => {

						return data.map(
							caseObj => {

								return caseObj.setIn(
									['case', 'pending_changes' ],
									caseObj.case.pending_changes.merge({
										[mutationData.catalogueId]: {
											bibliotheek_kenmerken_id: mutationData.catalogueId,
											case_id: mutationData.caseId,
											created_by: mutationData.requestorId,
											created_by_name: mutationData.requestorName,
											reason: mutationData.description,
											value: mutationData.value
										}
									})
								);
							}
						);

					},
					wait: ( mutationData, promise ) => {

						return snackbarService.wait(
							'De wijziging wordt aangevraagd',
							{
								promise,
								collapse: 0,
								catch: ( ) => 'De wijziging kon niet worden opgeslagen'
							}
						);

					},
					options: {
						reloadOnComplete: true
					}
				},
				{
					type: 'CASE_MUTATION_ADD',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								label: propCheck.string,
								mutationType: propCheck.oneOf([ 'create', 'relate', 'update', 'delete' ]),
								objectId: mutationData.mutationType === 'create' ?
									propCheck.any.optional
									: propCheck.string,
								objectType: propCheck.string,
								values: mutationData.mutationType === 'create' || mutationData.mutationType === 'update' ?
									propCheck.object
									: propCheck.any.optional
							}),
							mutationData
						);

						return {
							url: `/api/case/${mutationData.caseId}/mutations/add`,
							data: {
								mutation: {
									type: mutationData.mutationType,
									values: indexBy(
										mutationData.values,
										( value, key ) => `attribute.${key}`
									),
									object_uuid: mutationData.objectId,
									label: mutationData.label
								},
								object_type: mutationData.objectType
							}
						};

					},
					reduce: ( data, mutationData ) => {

						let key = `object.${mutationData.objectType}`;

						return data.map(
							caseObj => {
								return caseObj.merge({
									values: {
										[key]:
											get(caseObj.values, key, [])
												.concat(
													{
														complete: null,
														id: shortid(),
														label: mutationData.label,
														messages: null,
														object_uuid: mutationData.objectId || null,
														read_only: false,
														state: 'pending',
														type: mutationData.mutationType,
														values: indexBy(
															mutationData.values,
															( value, k ) => `attribute.${k}`
														)
													}
												)
									}
								}, { deep: true });
							}
						);

					},
					wait: ( mutationData, promise ) => {

						return snackbarService.wait('Bezig met het toevoegen van de mutatie', {
							promise,
							then: ( ) => 'Mutatie toegevoegd',
							catch: ( data ) => {

								let msg = '';

								switch (get(data, 'data.result[0].type')) {

									default:
									msg = 'De mutatie kon niet worden toegevoegd. Neem contact op met uw beheerder voor meer informatie.';
									break;

									case 'object/mutation/target_locked':
									msg = 'De mutatie kon niet worden toegevoegd. Het object staat elders al in bewerking.';
									break;
								}

								return msg;

							}
						});
					}
				},
				{
					type: 'CASE_MUTATION_REMOVE',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								objectType: propCheck.string,
								mutationId: propCheck.string
							}),
							mutationData
						);

						return {
							url: `/api/case/${mutationData.caseId}/mutations/delete`,
							data: {
								id: mutationData.mutationId
							}
						};

					},
					reduce: ( data, mutationData ) => {

						let key = `object.${mutationData.objectType}`;

						return data.map(
							caseObj => {
								return caseObj.merge({
									values: {
										[key]: get(caseObj.values, key, []).filter(
											mutation => mutation.id !== mutationData.mutationId
										)
									}
								}, { deep: true });
							}
						);

					}
				},
				{
					type: 'CASE_MUTATION_UPDATE',
					request: ( mutationData ) => {

						propCheck.throw(
							propCheck.shape({
								caseId: propCheck.number,
								objectType: propCheck.string,
								mutation: propCheck.shape({
									id: propCheck.string,
									label: propCheck.string,
									type: propCheck.oneOf([ 'create', 'update', 'delete', 'relate' ]),
									values: propCheck.object
								})
							}),
							mutationData
						);

						return {
							url: `/api/case/${mutationData.caseId}/mutations/update`,
							data: {
								mutation: assign(
									{},
									mutationData.mutation,
									{
										values: indexBy(
											mutationData.mutation.values,
											( value, key ) => `attribute.${key}`
										)
									}
								)
							}
						};

					},
					reduce: ( data, mutationData ) => {

						let key = `object.${mutationData.objectType}`;

						return data.map(
							caseObj => {
								return caseObj.merge({
									values: {
										[key]: get(caseObj.values, key, [])
											.map(mutation => {

												if (mutation.id === mutationData.mutation.id) {
													return mutation.merge(mutationData.mutation)
														.merge({
															values: indexBy(
																mutationData.mutation.values,
																( value, k ) => `attribute.${k}`
															)
														});
												}

												return mutation;

											})
									}
								}, { deep: true });
							}
						);

					}
				}
			];
		}])
		.run([ 'mutationService', 'zsCaseViewActions', ( mutationService, zsCaseViewActions ) => {

			zsCaseViewActions.forEach(( action ) => {
				mutationService.register(action);
			});

		}])
		.name;
