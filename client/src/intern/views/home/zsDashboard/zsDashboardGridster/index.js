import angular from 'angular';
import mapValues from 'lodash/object/mapValues';
import assign from 'lodash/object/assign';
import indexBy from 'lodash/collection/indexBy';

export default
	angular.module('zsDashboardGridster', [
	])
		.directive('zsDashboardGridster', [ ( ) => {

			return {
				restrict: 'A',
				require: 'gridster',
				link: ( scope, element, attrs, gridster ) => {

					let getKey = ( key ) => key.replace('widget.', '');

					// modified from angular-gridster's autoSetItemPosition, which modifies the grid
					
					let getItemPosition = ( sizeX, sizeY ) => {
						// walk through each row and column looking for a place it will fit
						for (let rowIndex = 0; rowIndex < gridster.maxRows; ++rowIndex) {
							for (let colIndex = 0; colIndex < gridster.columns; ++colIndex) {
								// only insert if position is not already taken and it can fit
								let items = gridster.getItems(rowIndex, colIndex, sizeX, sizeY, [ ]);

								if (items.length === 0 && gridster.canItemOccupy({ sizeX, sizeY }, rowIndex, colIndex)) {
									return {
										row: rowIndex,
										col: colIndex
									};
								}
							}
						}

						return { row: 0, col: 0 };
					};

					scope.vm.onItemAdd.push(item => {

						let customItemMap = scope.vm.customItemMap,
							widgetData = mapValues(customItemMap, ( value/*, key*/ ) => item[getKey(value)]),
							position = getItemPosition(widgetData.sizeX, widgetData.sizeY);

						assign(
							item,
							indexBy(
								mapValues(
									indexBy([ 'row', 'col' ]),
									( value, key ) => position[key]
								),
								( value, key ) => getKey(customItemMap[key])
							)
						);


					});

				}
			};

		}])
		.name;
