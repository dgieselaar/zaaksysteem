import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import template from './template.html';
import resourceModule from './../../../../../../../../shared/api/resource';
import composedReducerModule from './../../../../../../../../shared/api/resource/composedReducer';
import savedSearchesServiceModule from './../../../../../../../../shared/ui/zsSpotEnlighter/savedSearchesService';
import zqlEscapeFilterModule from './../../../../../../../../shared/object/zql/zqlEscapeFilter';
import get from 'lodash/object/get';
import assign from 'lodash/object/assign';
import first from 'lodash/array/first';
import mapValues from 'lodash/object/mapValues';
import identity from 'lodash/utility/identity';
import find from 'lodash/collection/find';
import uniq from 'lodash/array/uniq';
import includes from 'lodash/collection/includes';
import shortid from 'shortid';
import merge from 'lodash/object/merge';
import compact from 'lodash/array/compact';
import initial from 'lodash/array/initial';
import last from 'lodash/array/last';
import isArray from 'lodash/lang/isArray';
import zsTableModule from './../../../../../../../../shared/ui/zsTable';
import immutable from 'seamless-immutable';
import sessionServiceModule from './../../../../../../../../shared/user/sessionService';
import zsSpinnerModule from './../../../../../../../../shared/ui/zsSpinner';
import zsProgressBarModule from './../../../../../../../../shared/ui/zsProgressBar';
import caseActions from './../../../../../../../../shared/case/caseActions';
import zsCaseAdminViewModule from './../../../../../../case/zsCaseView/zsCaseAdminView';
import zsModalModule from './../../../../../../../../shared/ui/zsModal';
import zsConfirmModule from './../../../../../../../../shared/ui/zsConfirm';
import zsPaginationModule from './../../../../../../../../shared/ui/zsPagination';
import snackbarServiceModule from './../../../../../../../../shared/ui/zsSnackbar/snackbarService';
import zsCaseIntakeActionListModule from './../../../../../../../../shared/case/zsCaseIntakeActionList';
import zsTruncateModule from '../../../../../../../../shared/ui/zsTruncate';
import zsCaseStatusIconModule from './../../../../../../../../shared/case/zsCaseStatusIcon';
import convertv0Case from './../../../../../../../../shared/api/mock/convertv0Case';
import zsCaseSearchResultCompactModule from './zsCaseSearchResultCompact';
import defaultCompactTemplate from './default-compact-template.html';
import './styles.scss';

export default angular.module('zsDashboardWidgetSearchResult', [
		angularUiRouterModule,
		resourceModule,
		composedReducerModule,
		savedSearchesServiceModule,
		zqlEscapeFilterModule,
		zsPaginationModule,
		zsTableModule,
		sessionServiceModule,
		zsSpinnerModule,
		zsProgressBarModule,
		zsCaseAdminViewModule,
		zsModalModule,
		zsConfirmModule,
		snackbarServiceModule,
		zsCaseIntakeActionListModule,
		zsTruncateModule,
		zsCaseSearchResultCompactModule,
		zsCaseStatusIconModule
	])
		.directive('zsDashboardWidgetSearchResult', [ '$compile', '$parse', '$state', '$timeout', '$http', 'resource', 'composedReducer', 'savedSearchesService', 'sessionService', 'zqlEscapeFilter', 'zsModal', 'zsConfirm', 'snackbarService', ( $compile, $parse, $state, $timeout, $http, resource, composedReducer, savedSearchesService, sessionService, zqlEscapeFilter, zsModal, zsConfirm, snackbarService ) => {

			let templates =
				{
					'intake_actions': {
						template:
							`<zs-case-intake-action-list data-case="item.data.case" data-user="item.data.user" on-self-assign="item.data.onSelfAssign()"></zs-case-intake-action-list>`
					}
				};

			return {
				restrict: 'E',
				scope: {
					widgetData: '&',
					widgetTitle: '&',
					widgetLoading: '&',
					onDataChange: '&',
					filterQuery: '&',
					compact: '&'
				},
				template,
				bindToController: true,
				controller: [ '$element', '$scope', function ( $element, $scope ) {

					let ctrl = this,
						searchReducer,
						configResource,
						userResource = sessionService.createResource($scope),
						columnReducer,
						zqlReducer,
						resultResource,
						itemReducer,
						sortReducer,
						filterByReducer,
						userSort = immutable({});

					let getSearchId = ( ) => get(ctrl.widgetData(), 'search_id');

					let getSearchFromPredefined = ( ) => {
						return savedSearchesService.getPredefinedSearches()
							.filter( search => search.id === getSearchId())[0];
					};

					let createCaseResource = ( caseObj ) => {
						return resource(
							`/api/case/${caseObj.values['case.number']}`,
							{
								scope: $scope
							}
						)
							.reduce( ( requestOptions, data ) => {
								return first((data || []).map(convertv0Case));
							});
					};

					let handleActionModal = ( caseObj, action ) => {

						let scope = $scope.$new(true),
							modal;

						let cleanup = ( ) => {
							scope.caseResource.destroy();
							scope.casetypeResource.destroy();
						};

						scope.action = action;

						scope.close = ( promise ) => {

							modal.hide();

							promise.then( ( ) => {
								
								resultResource.reload();
								modal.close();
								cleanup();

							})
								.catch(( ) => {
									modal.show();
								});
						};

						scope.user = userResource.data().instance.logged_in_user;

						scope.caseResource = createCaseResource(caseObj);

						scope.casetypeResource = resource(
							{
								url: `/api/v1/casetype`,
								params: {
									zql: `SELECT {} FROM casetype WHERE casetype.id = ${caseObj.values['case.casetype.id']}`
								}
							},
							{
								scope: $scope
							}
						)
							.reduce( ( requestOptions, data ) => {
								return first(data);
							});

						modal = zsModal({
							title: action.label,
							el: $compile(
								`<zs-case-admin-view
									data-action="action"
									data-user="user"
									case-resource="caseResource"
									casetype-resource="casetypeResource"
									on-submit="close($promise)"
								>
								</zs-case-admin-view>`
							)(scope)
						});

						modal.open();

						modal.onClose( ( ) => {
							cleanup();
						});
					};

					let handleActionConfirm = ( caseObj, action ) => {

						let caseResource = createCaseResource(caseObj),
							mutation = action.mutate();

						zsConfirm(action.confirm.label, action.confirm.verb)
							.then( ( ) => {
								return caseResource.mutate(mutation.type, mutation.data).asPromise();
							})
							.finally( ( ) => {
								caseResource.destroy();
							});

					};

					let handleActionClick = ( caseObj, action ) => {

						if (action.confirm) {
							handleActionConfirm(caseObj, action);
						} else {
							handleActionModal(caseObj, action);
						}
						
					};

					searchReducer = composedReducer({ scope: $scope },
						getSearchFromPredefined()
						|| resource(
							( ) => {

								let zql = `SELECT {} FROM saved_search WHERE object.uuid = ${zqlEscapeFilter(getSearchId())}`;

								return { url: '/api/object/search', params: { zql } };
							},
							{ scope: $scope }
						)
							.reduce( ( requestOptions, data ) => {

								let search = first(data);

								if (search) {
									search = savedSearchesService.parseLegacyFilter(search);
								}

								return search;
							})
					)
						.reduce(identity);

					filterByReducer = composedReducer({ scope: $scope }, searchReducer, ctrl.filterQuery)
						.reduce( ( search, filterQuery ) => {
							let filterBy = [ filterQuery ],
								searchZql = get(search, 'values.query.zql', '');

							if (searchZql) {

								let match = searchZql.match(/MATCHING "(.*?)"/);

								if (match) {
									filterBy = filterBy.concat(match[1]);
								}
							}

							return compact(filterBy);
						});

					configResource = resource(
							( ) => {
								let objectTypeId = get(searchReducer.data(), 'values.query.objectType.object_type'),
									opts = objectTypeId ? { url: `/api/search/config/${objectTypeId}` } : null;

								return opts;
							},
							{ scope: $scope }
						)
						.reduce( (requestOptions, data ) => first(data));

					columnReducer = composedReducer({ scope: $scope }, searchReducer, configResource)
						.reduce(( search, config ) => {

								let columnConfig = merge(
										{},
										get(config, 'columns.templates'),
										templates
									),
									columns = [],
									defaults = get(config, 'columns.default', immutable([]));

								if (search && search.values.query.columns) {
									columns = search.values.query.columns;
								} else if (search) {
									columns =
										defaults.map( ( columnId ) => {
												return { id: columnId };
											});
								}

								if (search.id === 'intake') {
									columns = immutable(
										initial(columns)
											.concat([
												{ id: 'intake_actions' }
											])
											.concat(last(columns))
									);
								}

								columns =
									// notifications are now in the status column, so filter
									// out the column if it's in a saved search
									columns.filter(column => column.id !== 'notifications')
										.map( columnToMap => {

											let column = columnToMap,
												data = columnConfig[column.id] || {},
												resolve;

											if (data.template) {
												data = assign({}, data, { template: savedSearchesService.convertLegacyTemplate(data.template) });
											}

											column = column.merge(data);

											if (!column.template
												|| (includes(defaults, column.id) && !data.template)) {

												resolve = column.resolve ? `item.${column.resolve}` : `item.values['${column.id}']`;

												if (column.filter) {
													resolve += ` | ${column.filter}`;
												}

												column = column.merge({
													template: `<span>{{::${resolve}}}</span>`
												});
											}

											// make sure everything is wrapped in a span
											if (column.template.indexOf('<') !== 0) {
												column = column.merge({
													template: `<span>${column.template}</span>`
												});
											}

											return column;
										});


								return columns;

						});

					sortReducer = composedReducer({ scope: $scope }, searchReducer, columnReducer, configResource, ( ) => userSort)
						.reduce( ( search, columns, config, userSortData ) => {

							let sort,
								searchSort = get(search, 'values.query.options.sort') || immutable({}),
								defaultSort = get(config, 'sort') || immutable({});

							sort = defaultSort.merge([ searchSort, userSortData ]);

							sort = sort.merge( { type: get(find(columns, { id: sort.by }), 'sort.type', 'alphanumeric') });

							if (!sort.by) {
								sort = {
									by: get(first(columns), 'id'),
									order: 'asc'
								};
							}

							return sort;
						});


					zqlReducer = composedReducer({ scope: $scope }, searchReducer, columnReducer, configResource, userResource, sortReducer, filterByReducer)
						.reduce(( search, columns, config, user, sort, matching ) => {

							let	zql = get(search, 'zql'),
								columnsToFetch = columns,
								opts;

							if (!zql && search && user && sort) {

								columnsToFetch =
									columns.flatMap( ( column ) => get(column, 'data.columns', column.id))
									.concat(get(config, 'always', []));

								if (get(search, 'values.query.objectType.object_type') === 'case') {
									columnsToFetch = columnsToFetch.concat([ 'case.casetype.id', 'case.requestor.id', 'case.status', 'case.assignee.id' ]);
								}

								if (search.id === 'intake') {
									columnsToFetch = columnsToFetch.concat('case.assignee.department', 'case.casetype.department', 'case.casetype.id');
								}

								opts =
									{
										user,
										sort,
										columns: uniq(columnsToFetch)
									};

								if (matching) {
									opts.matching = matching.join(' ');
								}

								zql = savedSearchesService.getZql(search, opts);
							}

							return zql;

						});

					resultResource = resource(
						( ) => {
							let zql = zqlReducer.data(),
								limit = get(ctrl.widgetData(), 'limit', 10),
								opts;

							if (zql && resultResource) {
								opts = { url: '/api/object/search', params: { zql, zapi_num_rows: limit } };
							}

							return opts;
						},
						{ scope: $scope }
					);

					itemReducer = composedReducer({ scope: $scope }, resultResource, configResource, userResource, searchReducer);

					itemReducer.reduce( ( resultData, config, user, search ) => {

						let items = resultData,
							classConfig =
								mapValues(
									get(config, 'style.classes'),
									( expr ) => $parse(expr)
								),
							objectType = get(search, 'values.query.objectType.object_type', 'case');

						// make sure we'll able to differentiate between empty and non-existent result sets
						if (!items) {
							return null;
						}

						if (objectType === 'case') {
							items = items.map(
								item => {

									let caseId = item.values['case.number'];

									return item.merge({
										id: shortid(),
										href: $state.href('case', { caseId }),
										style: mapValues(classConfig, ( getter ) => getter(item)),
										itemActions: caseActions({
											caseObj: convertv0Case(item),
											casetype: null,
											user: user.instance.logged_in_user,
											$state
										})
											.filter(action => action.context !== 'admin')
											.map(action => {
												return {
													name: action.name,
													label: action.label,
													click: ( ) => {
														handleActionClick(item, action);
													}
												};
											}),
										data: {
											case: convertv0Case(item),
											user,
											onSelfAssign: ( ) => {

												snackbarService.wait('Zaak wordt in behandeling genomen.', {
													promise: $http({
														url: `/zaak/${caseId}/open`,
														method: 'POST'
													}),
													then: ( ) => {
														return 'Zaak is in behandeling genomen.';
													},
													catch: ( ) => {
														return 'Zaak kon niet in behandeling worden genomen. Neem contact op met uw beheeerder voor meer informatie.';
													}
												})
													.then( ( ) => {
														$state.go('case', { caseId });
													});
												
											}
										},
										values: mapValues(item.values, ( value/*, key*/ ) => {
											return isArray(value) ?
												first(value)
												: value;
										})
									});
								}
							);
						} else {
							items = items.map(
								item => {

									return item.merge({
										id: shortid(),
										href: `/object/${item.id}`
									});
								}
							);
						}
						
						return items;

					});

					ctrl.getRows = itemReducer.data;

					ctrl.getColumns = composedReducer({ scope: $scope }, columnReducer, sortReducer, configResource, ctrl.compact)
						.reduce( ( columns, sort, config, isCompact ) => {

							let visibleColumns = columns;

							if (isCompact) {

								visibleColumns = immutable([
									{
										id: 'compact',
										label: '',
										template: get(config, 'columns.compact', defaultCompactTemplate)
									}
								]);

								visibleColumns = visibleColumns.concat(
									columns.filter(col => col.id === 'actions' || col.id === 'intake_actions')
								);

							}

							let styledColumns = visibleColumns.map( ( column ) => {

								let sortedOn = column.id === get(sort, 'by'),
									sortReversed = sortedOn && get(sort, 'order', 'asc') === 'desc',
									iconType;

								if (sortedOn) {
									iconType = `chevron-${sortReversed ? 'up' : 'down'}`;
								}

								return column.merge({
									style: {
										'column-sort-active': sortedOn,
										'column-sort-reversed': sortReversed,
										'column-sort-supported': column.sort
									},
									iconType
								});
							});

							return styledColumns;

						}).data;

					ctrl.handleColumnClick = ( columnId ) => {
						let sort = sortReducer.data(),
							column = find(columnReducer.data(), { id: columnId });

						if (!get(column, 'sort', null)) {
							return;
						}

						if (sort.by === columnId) {
							sort = sort.merge({ order: sort.order === 'desc' ? 'asc' : 'desc' });
						} else {
							sort = sort.merge({ by: columnId, order: 'asc' });
						}

						userSort = sort;
					};

					ctrl.getCurrentPage = ( ) => resultResource.cursor();
					
					ctrl.handlePageChange = ( page ) => {
						resultResource.request(resultResource.cursor(page));
					};

					ctrl.handleLimitChange = ( limit ) => {
						ctrl.onDataChange({ $data: assign({ }, ctrl.widgetData(), { limit }) });
						resultResource.request(resultResource.cursor(1));
					};

					ctrl.hasNextPage = ( ) => !!resultResource.next();
					
					ctrl.hasPrevPage = ( ) => !!resultResource.prev();

					ctrl.getLimit = ( ) => resultResource.limit();

					ctrl.isLoading = ( ) => zqlReducer.state() === 'pending';

					ctrl.isResultLoading = ( ) => !ctrl.isLoading() && (itemReducer.state() === 'pending' || resultResource.fetching());

					ctrl.isEmpty = ( ) => get(ctrl.getRows(), 'length', false) === 0;

					ctrl.widgetTitle({
						$getter:
							( ) => {
								let label = get(searchReducer.data(), 'label') || '';

								if (resultResource.totalRows()) {
									label += ` (${resultResource.totalRows()})`;
								}

								return label;
							}
					});

					ctrl.widgetLoading( {
						$getter: ctrl.isResultLoading
					});
					
					return ctrl;
				}],
				controllerAs: 'vm'
			};
		}])
		.name;

