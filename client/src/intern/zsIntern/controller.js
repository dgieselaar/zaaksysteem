import angular from 'angular';
import zsTopBarModule from './../../shared/navigation/zsTopBar';
import zsClickOutsideModule from './../../shared/ui/zsClickOutside';
import zsContextualActionMenuModule from './../../shared/ui/zsContextualActionMenu';
import zsActiveSubjectModule from './zsActiveSubject';
import get from 'lodash/object/get';
import first from 'lodash/array/first';
import take from 'lodash/array/take';
import zsBackdropServiceModule from '../../shared/navigation/zsBackdropService';

import './styles.scss';

export default
	angular.module('zsInternController', [
		zsTopBarModule,
		zsClickOutsideModule,
		zsContextualActionMenuModule,
		zsBackdropServiceModule,
		zsActiveSubjectModule
	])
		.controller('zsInternController', [ 'resource', 'zsBackdropService', '$scope', function ( resource, zsBackdropService, $scope ) {

			let ctrl = this,
				userResource = resource({ url: '/api/v1/session/current', headers: { 'Disable-Digest-Authentication': 1 } }, { scope: $scope, cache: { every: 15 * 60 * 1000 	} })
					.reduce( ( requestOptions, data ) => first(data)),
				development,
				instanceId;

			ctrl.isContentVisible = ( ) => true;

			ctrl.isMenuOpen = ( ) => {
				return zsBackdropService.isEnabled();
			};

			ctrl.getCompany = ( ) => get(userResource.data(), 'instance.account');

			ctrl.getUser = ( ) => get(userResource.data(), 'instance.logged_in_user');

			userResource.onUpdate(( ) => {
				let spl = get(userResource.source(), 'request_id', '').split('-');
				
				development = !!get(userResource.source(), 'development', true);
				instanceId = take(spl, spl.length - 2).join('-');

			});

			ctrl.isDevelopment = ( ) => development;

			ctrl.getInstanceId = ( ) => instanceId;

		}])
		.name;
