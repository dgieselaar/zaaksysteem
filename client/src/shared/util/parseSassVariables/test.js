import parseSassVariables from '.';

describe('parseSassVariables', ( ) => {

	it('should parse a single line', ( ) => {

		let vars = parseSassVariables(`$small-screen-up: 601px;`);

		expect(vars['small-screen-up']).toBeDefined();

	});

	it('should strip !default from the variable value', ( ) => {

		let vars = parseSassVariables(`$small-screen-up: 601px !default;`);

		expect(vars['small-screen-up']).toEqual('601px');

	});

	it('should parse multiline', ( ) => {

		let vars = parseSassVariables(`
			$small-screen-up: 601px !default;
			$medium-screen-up: 993px;
			$large-screen-up: 1201px;
		`);

		expect(vars['small-screen-up']).toEqual('601px');
		expect(vars['medium-screen-up']).toEqual('993px');
		expect(vars['large-screen-up']).toEqual('1201px');

	});

	it('should ignore comments and whitespace', ( ) => {

		let vars = parseSassVariables(`
			// Media Query Ranges
			$small-screen-up: 601px !default;
			$medium-screen-up: 993px !default;
			$large-screen-up: 1201px !default;
			$small-screen: 600px !default;
			$medium-screen: 992px !default;
			$large-screen: 1200px !default;

			$medium-and-up: "only screen and (min-width : #{$small-screen-up})" !default;
		`);

		expect(Object.keys(vars).length).toBe(7);

	});

	it('should strip quotes', ( ) => {

		let vars = parseSassVariables(`$medium-and-up: "only screen and (min-width : 601px)" !default;`);

		expect(vars['medium-and-up']).toEqual('only screen and (min-width : 601px)');

	});

	it('should replace references', ( ) => {

		let vars = parseSassVariables(`
			// Media Query Ranges
			$small-screen-up: 601px !default;
			$medium-and-up: "only screen and (min-width : #{$small-screen-up})" !default;`
		);

		expect(vars['medium-and-up']).toBe('only screen and (min-width : 601px)');

	});

	it('should throw an error when a reference is undefined', ( ) => {

		expect(
			parseSassVariables.bind(null, `$medium-and-up: "only screen and (min-width : #{$small-screen-up})" !default;`)
		).toThrow();

	});


});
