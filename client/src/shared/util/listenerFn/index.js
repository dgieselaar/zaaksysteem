import invoke from 'lodash/collection/invoke';
import pull from 'lodash/array/pull';
import once from 'lodash/function/once';

export default ( options = { immediate: false } ) => {

	let listeners = [],
		{ immediate } = options,
		args = [];

	return {
		register: ( fn, opts = { once: false, immediate }) => {

			let proxy = fn,
				remove = ( ) => {
					pull(listeners, proxy);
				};

			if (opts.once) {
				proxy = once(( ...rest ) => {
					fn(...rest);
					remove();
				});
			}

			listeners.push(proxy);

			if (opts.immediate !== false || !!immediate) {
				proxy(...args);
			}

			return remove;

		},
		invoke: ( ...rest ) => {
			args = rest;
			invoke(listeners, 'call', null, ...args);
		},
		listeners,
		destroy: ( ) => {
			args = null;
			listeners = null;
		}
	};

};
