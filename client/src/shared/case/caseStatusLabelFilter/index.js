import angular from 'angular';

export default
	angular.module('caseStatusLabelFilter', [
	])
		.filter('caseStatusLabel', [ ( ) => {

			return ( status ) => {

				return {
					resolved: 'Afgehandeld',
					stalled: 'Opgeschort',
					new: 'Nieuw',
					open: 'In behandeling'
				}[status];

			};

		}])
		.name;
