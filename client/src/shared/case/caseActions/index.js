import get from 'lodash/object/get';
import capitalize from 'lodash/string/capitalize';
import merge from 'lodash/object/merge';
import findIndex from 'lodash/array/findIndex';
import includes from 'lodash/collection/includes';
import dateformat from 'dateformat';
import flatten from 'lodash/array/flatten';
import mapKeys from 'lodash/object/mapKeys';
import indexBy from 'lodash/collection/indexBy';
import termOptions from './../termOptions';
import pick from 'lodash/object/pick';
import identity from 'lodash/utility/identity';
import keys from 'lodash/object/keys';

let dateTypeOptions =
	[
		{
			value: 'calendar_days',
			label: 'Kalenderdagen'
		},
		{
			value: 'work_days',
			label: 'Werkdagen'
		},
		{
			value: 'weeks',
			label: 'Weken'
		},
		{
			value: 'fixed_date',
			label: 'Vaste einddatum'
		}
	];

let getCaseId = ( caseObj ) => {
	return get(caseObj, 'instance.number');
};

export default ( opts ) => {

	let { caseObj, casetype, user, $state } = opts,
		isAdmin = includes(caseObj.instance.permissions, 'zaak_beheer') || caseObj.instance.permissions.length === 0,
		isEditor = includes(caseObj.instance.permissions, 'zaak_edit') || caseObj.instance.permissions.length === 0,
		isManagableAndUnresolved =
			caseObj.instance.status !== 'resolved'
			// we need case permissions in search results
			&& (
				( isEditor && !!caseObj.instance.assignee)
				|| isAdmin
			),
		casePhases = get(casetype, 'instance.phases', []),
		today;

	today = new Date();

	today = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1, -1);

	return [
		{
			name: 'toewijzing',
			label: 'Toewijzing wijzigen',
			visible: isManagableAndUnresolved,
			fields: [
				{
					name: 'allocation_type',
					label: 'Type toewijzing',
					template: 'radio',
					data: {
						options: [
							{
								value: 'org-unit',
								label: 'Rol of afdeling'
							},							{
								value: 'assignee',
								label: 'Specifieke behandelaar'
							},

							{
								value: 'assign_to_self',
								label: 'Zelf in behandeling nemen'
							}
						]
					},
					required: true,
					defaults: 'org-unit'
				},
				{
					name: 'assignee',
					label: 'Nieuwe behandelaar',
					template: 'object-suggest',
					data: {
						objectType: 'medewerker'
					},
					required: true,
					when: [ '$values', ( values ) => values.allocation_type === 'assignee' ]
				},
				{
					name: 'org-unit',
					label: 'Rol of afdeling',
					template: 'org-unit',
					required: true,
					data: {
						depth: 1
					},
					when: [ '$values', ( values ) => values.allocation_type === 'org-unit' ]
				},
				{
					name: 'change_department',
					label: 'Ook afdeling wijzigen',
					template: 'checkbox',
					when: [ '$values', ( values ) => values.allocation_type === 'assignee' ]
				},
				{
					name: 'send_email',
					label: 'Verstuur e-mail',
					template: 'checkbox',
					when: [ '$values', ( values ) => values.allocation_type === 'assignee' ],
					defaults: true
				}
			],
			mutate: ( values ) => {

				let params = {
						caseId: get(caseObj, `instance.number`),
						allocationType: values.allocation_type,
						notify: values.allocation_type === 'assignee' ?
							!!values.send_email
							: false
					};

				if (params.allocationType === 'assignee') {
					params = merge(params, { assigneeId: values.assignee.id, changeDepartment: !!values.change_department });
				} else if (params.allocationType === 'org-unit') {
					params = merge(params, { orgUnitId: values['org-unit'].unit, roleId: values['org-unit'].role });
				} else if (params.allocationType === 'assign_to_self') {
					params = merge(params, { assigneeId: user.id });
				}

				return {
					type: 'case/allocate',
					data: params
				};
			},
			options: ( values ) => {

				let redirect =
						pick({
							case: (values.allocation_type === 'assign_to_self' && $state.current.name.indexOf('home') === 0),
							home: (values.allocation_type !== 'assign_to_self' && $state.current.name.indexOf('case.') === 0)
						}, identity);

				return {
					willRedirect: keys(redirect).length > 0
				};

			}
		},
		{
			name: 'opschorten',
			label: 'Opschorten',
			fields: [
				{
					name: 'reason',
					label: 'Reden opschorting',
					template: 'text',
					required: true,
					defaults: ''
				},
				{
					name: 'term',
					label: 'Termijn',
					template: 'form',
					data: {
						fields: [
							{
								name: 'term_type',
								template: 'radio',
								data: {
									options: [
										{
											value: 'indeterminate',
											label: 'Onbepaald'
										},
										{
											value: 'determinate',
											label: 'Bepaald, namelijk'
										}
									]
								},
								required: true
							},
							{
								name: 'term_spec',
								template: 'form',
								data: {
									fields: [
										{
											name: 'term_amount_type',
											template: 'select',
											data: {
												options: dateTypeOptions
											},
											required: true
										},
										{
											name: 'term_amount',
											template: 'number',
											required: true,
											when: [ '$values', ( values ) => get(values, 'term_amount_type') && get(values, 'term_amount_type') !== 'fixed_date' ]
										},
										{
											name: 'term_date',
											template: 'date',
											required: true,
											when: [ '$values', ( values ) => get(values, 'term_amount_type') === 'fixed_date' ]
										}
									]
								},
								when: [ '$values', ( values ) => get(values, 'term_type') === 'determinate' ],
								required: true
							}
						]
					},
					defaults: {
						term_type: 'determinate',
						term_spec: {
							term_amount_type: 'weeks',
							term_amount: 6,
							term_date: null
						}
					}
				}
			],
			visible: isManagableAndUnresolved && get(caseObj, 'instance.status') !== 'stalled',
			mutate: ( values ) => {

				let params = {
					caseId: get(caseObj, 'instance.number'),
					reason: values.reason,
					termType: get(values, 'term.term_type')
				};

				if (get(values, 'term.term_type') === 'determinate') {

					let amountType = get(values, 'term.term_spec.term_amount_type');

					merge(params, {
						termAmountType: amountType,
						termAmount: amountType === 'fixed_date' ?
							get(values, 'term.term_spec.term_date')
							: get(values, 'term.term_spec.term_amount')
					});

				}

				return {
					type: 'case/suspend',
					data: params
				};

			}
		},
		{
			name: 'hervatten',
			label: 'Hervatten',
			fields: [
				{
					name: 'reason',
					label: 'Reden hervatten',
					template: 'text',
					required: true
				},
				{
					name: 'start_date',
					label: 'Ingangsdatum opschorting',
					template: 'date',
					required: true,
					defaults: new Date(caseObj.instance.stalled_since),
					data: {
						max: today
					}
				},
				{
					name: 'end_date',
					label: 'Einddatum opschorting',
					template: 'date',
					required: true,
					defaults: new Date(),
					data: {
						max: today,
						min: [ '$values', ( values ) => values.start_date ]
					}
				}
			],
			visible: isManagableAndUnresolved && get(caseObj, 'instance.status') === 'stalled',
			mutate: ( values ) => {

				return {
					type: 'case/resume',
					data: {
						caseId: getCaseId(caseObj),
						reason: values.reason,
						stalledSince: values.start_date,
						stalledUntil: values.end_date
					}
				};

			}
		},
		{
			name: 'afhandelen',
			label: 'Vroegtijdig afhandelen',
			visible: isManagableAndUnresolved,
			fields: [
				{
					name: 'reason',
					label: 'Reden afhandeling',
					template: 'text',
					required: true
				},
				{
					name: 'result',
					label: 'Resultaat',
					template: 'radio',
					data: {
						options: get(casetype, 'instance.results', [])
							.map( ( result ) => {
								return {
									value: result.type,
									label: capitalize(result.label || result.type)
								};
							})
					},
					required: true
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: get(caseObj, 'instance.number'),
					reason: values.reason,
					result: values.result
				};

				return {
					type: 'case/resolve_prematurely',
					data
				};

			}
		},
		{
			name: 'termijn',
			label: 'Termijn wijzigen',
			visible: isManagableAndUnresolved,
			fields: [
				{
					name: 'reason',
					label: 'Reden wijzigen termijn',
					template: 'text',
					required: true
				},
				{
					name: 'type_term_change',
					label: 'Type wijziging',
					template: 'radio',
					data: {
						options: [
							{
								value: 'fixedDate',
								label: 'Verlengen'
							},
							{
								value: 'changeTerm',
								label: 'Behandeltermijn wijzigen'
							}
						]
					},
					required: true
				},
				{
					name: 'new_term',
					label: 'Nieuwe termijn',
					template: 'form',
					data: {
						fields: [
							{
								name: 'term_amount',
								template: 'number',
								required: true
							},
							{
								name: 'term_amount_type',
								template: 'select',
								data: {
									options: dateTypeOptions.filter(option => option.value !== 'fixed_date')
								},
								required: true
							}
						]
					},
					when: [ '$values', ( values ) => values.type_term_change === 'changeTerm' ],
					defaults: {
						term_amount: 1,
						term_amount_type: 'calendar_days'
					}
				},
				{
					name: 'new_term_date',
					label: 'Nieuwe streefafhandeldatum',
					template: 'date',
					required: true,
					when: [ '$values', ( values ) => values.type_term_change === 'fixedDate' ]
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: get(caseObj, 'instance.number'),
					reason: values.reason,
					prolongationType: values.type_term_change
				};

				if (values.type_term_change === 'fixedDate') {
					data.prolongationDate = values.new_term_date;
				} else {
					data.termType = values.new_term.term_amount_type;
					data.termAmount = values.new_term.term_amount;
				}
				
				return {
					type: 'case/prolong',
					data
				};
			}
		},
		{
			name: 'relateren',
			label: 'Zaak relateren',
			visible: isManagableAndUnresolved,
			fields: [
				{
					name: 'case_to_relate',
					label: 'Te relateren zaak',
					template: 'object-suggest',
					data: {
						objectType: 'case'
					},
					required: true
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: get(caseObj, 'instance.number'),
					relatedCaseId: values.case_to_relate.data.id
				};

				return {
					type: 'case/relate',
					data
				};

			}
		},
		{
			name: 'kopieren',
			label: 'Zaak kopiëren',
			type: 'confirm',
			visible:
				isAdmin || (isEditor && !!caseObj.instance.assignee),
			confirm: {
				label: 'Weet u zeker dat u deze zaak wilt kopiëren?',
				verb: 'Kopieer zaak'
			},
			mutate: ( ) => {

				let data = {
					caseId: get(caseObj, 'instance.number')
				};

				return {
					type: 'case/copy',
					data
				};

			}
		},
		{
			name: 'aanvrager',
			label: 'Aanvrager wijzigen',
			fields: [
				{
					name: 'type',
					label: 'Type',
					template: 'radio',
					data: {
						options: [
							{
								value: 'natuurlijk_persoon',
								label: 'Burger'
							},
							{
								value: 'bedrijf',
								label: 'Organisatie'
							},
							{
								value: 'medewerker',
								label: 'Medewerker'
							}
						]
					},
					required: true,
					defaults: get(caseObj, 'instance.requestor.instance.subject_type')
				},
				{
					name: 'requestor',
					label: 'Aanvrager',
					template: 'object-suggest',
					data: {
						objectType: [ '$values', ( values ) => values.type ]
					},
					required: true,
					defaults: caseObj.instance.requestor ?
						{
							label: caseObj.instance.requestor.instance.name,
							data: {
								id: caseObj.instance.requestor.instance.id.match(/\d+$/)[0]
							}
						}
						: null
				}
			],
			processChange: ( name, value, values ) => {

				let vals = values.merge({ [name]: value });

				if (name === 'type') {

					vals = vals.merge({ requestor: null });

				}

				return vals;

			},
			mutate: ( values ) => {

				let data = {
						caseId: getCaseId(caseObj),
						settings: {
							aanvrager_id: `betrokkene-${values.type}-${values.requestor.data.id}`
						}
					};

				return {
					type: 'case/set_setting',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'coordinator',
			label: 'Coordinator wijzigen',
			fields: [
				{
					name: 'coordinator',
					label: 'Coordinator',
					template: 'object-suggest',
					data: {
						objectType: 'medewerker'
					},
					required: true
				}
			],
			context: 'admin',
			mutate: ( values ) => {

				let data = {
					caseId: caseObj.instance.number,
					settings: {
						coordinator_id: `betrokkene-medewerker-${values.coordinator.data.id}`
					}
				};

				return {
					type: 'case/set_setting',
					data
				};

			}
		},
		{
			name: 'afdeling',
			label: 'Afdeling en rol wijzigen',
			fields: [
				{
					name: 'org_unit',
					label: '',
					template: 'org-unit',
					required: true,
					data: {
						depth: 1
					},
					defaults: caseObj.instance.route_ou === 1 ?
						null
						: {
							unit: String(caseObj.instance.route_ou),
							role: String(caseObj.instance.route_role)
						}
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: getCaseId(caseObj),
					settings: {
						ou_id: Number(values.org_unit.unit),
						role_id: Number(values.org_unit.role),
						change_only_route_fields: 1,
						wijzig_route_force: 1
					}
				};

				return {
					type: 'case/set_setting',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'registratiedatum',
			label: 'Registratiedatum wijzigen',
			fields: [
				{
					name: 'registratiedatum',
					label: 'Registratiedatum',
					template: 'date',
					required: true,
					defaults: caseObj.instance.date_of_registration ?
						new Date(caseObj.instance.date_of_registration)
						: null
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: getCaseId(caseObj),
					settings: {
						registratiedatum: dateformat(values.registratiedatum, 'dd-mm-yyyy')
					}
				};

				return {
					type: 'case/set_setting',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'streefafhandeldatum',
			label: 'Streefafhandeldatum wijzigen',
			fields: [
				{
					name: 'streefafhandeldatum',
					template: 'date',
					required: true,
					defaults: caseObj.instance.date_target ?
						new Date(caseObj.instance.date_target)
						: null
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: getCaseId(caseObj),
					settings: {
						streefafhandeldatum: dateformat(values.streefafhandeldatum, 'dd-mm-yyyy')
					}
				};

				return {
					type: 'case/set_setting',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'afhandeldatum',
			label: 'Afhandeldatum wijzigen',
			fields: [
				{
					name: 'afhandeldatum',
					template: 'date',
					required: true,
					defaults: caseObj.instance.date_of_completion ?
						new Date(caseObj.instance.date_of_completion)
						: null
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: getCaseId(caseObj),
					settings: {
						streefafhandeldatum: dateformat(values.streefafhandeldatum, 'dd-mm-yyyy')
					}
				};

				return {
					type: 'case/set_setting',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'vernietigingsdatum',
			label: 'Vernietigingsdatum wijzigen',
			fields: [
				{
					name: 'vernietigingsdatum',
					template: 'form',
					data: {
						fields: [
							{
								name: 'change',
								template: 'radio',
								label: '',
								data: {
									options: [
										{
											value: 'change',
											label: 'Nieuwe vernietigingsdatum'
										}
									]
								}
							},
							{
								name: 'date',
								template: 'date',
								label: ''
							}
						]
					},
					defaults: {
						date:
							caseObj.instance.date_destruction ?
							new Date(caseObj.instance.date_destruction)
							: null,
						change: 'change'
					},
					required: true,
					valid: [ '$values', ( vals ) => !!(!vals.vernietigingsdatum.change || vals.vernietigingsdatum.date) ]
				},
				{
					name: 'vernietigingstermijn',
					template: 'form',
					data: {
						fields: [
							{
								name: 'change',
								template: 'radio',
								data: {
									options: [
										{
											value: 'change',
											label: 'Herberekenen'
										}
									]
								},
								required: false
							},
							{
								name: 'term',
								template: 'select',
								label: '',
								data: {
									options: termOptions
								},
								required: [ '$values', ( vals ) => vals.change === 'change' ]
							}
						]
					},
					defaults: {
						date:
							caseObj.instance.date_destruction ?
							new Date(caseObj.instance.date_destruction)
							: null,
						change: false
					},
					required: true,
					valid: [ '$values', ( vals ) => !!(!vals.vernietigingstermijn.change || vals.vernietigingstermijn.term) ]
				},
				{
					name: 'vernietigingsdatum_reden',
					label: 'Rede van wijziging',
					template: 'select',
					required: true,
					data: {
						options: [
							'In belang van de aanvrager',
							'Uniek of bijzonder karakter voor de organisatie',
							'Bijzondere tijdsomstandigheid of gebeurtenis',
							'Beeldbepalend karakter',
							'Samenvatting van gegevens',
							'Betrokkene(n) is van bijzondere betekenis geweest',
							'Vervanging van stukken bij calamiteit',
							'Aanleiding van algemene regelgeving',
							'Verstoring van logische samenhang'
						]
							.map(reason => {
								return {
									value: reason,
									label: reason
								};
							})
					},
					defaults: 'In belang van de aanvrager'
				}
			],
			processChange: ( name, value, values ) => {

				let vals = values;

				if (name === 'vernietigingsdatum' || name === 'vernietigingstermijn') {
					vals = values.merge({
						vernietigingstermijn: {
							change: name === 'vernietigingstermijn' ? 'change' : null
						},
						vernietigingsdatum: {
							change: name === 'vernietigingsdatum' ? 'change' : null
						}
					}, { deep: true });
				}

				return vals;

			},
			valid: ( values ) => {
				return values.vernietigingsdatum.change || values.vernietigingstermijn.change;
			},
			mutate: ( values ) => {

				let settings = {
						vernietigingsdatum_reden: values.vernietigingsdatum_reden
					},
					data;

				if (values.vernietigingsdatum.change) {
					settings.vernietigingsdatum = dateformat(values.vernietigingsdatum.date, 'dd-mm-yyyy');
					settings.vernietigingsdatum_type = 'termijn';
				} else {
					settings.vernietigingsdatum_type = 'bewaren';
					settings.vernietigingsdatum_recalculate = values.vernietigingstermijn.term;
				}

				data = {
					caseId: getCaseId(caseObj),
					settings
				};

				return {
					type: 'case/set_setting',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'kenmerken',
			label: 'Kenmerken wijzigen',
			fields: [
				{
					name: 'attributes',
					template: 'case-admin-attribute-list',
					data: {
						available:
							flatten(
								get(casetype, 'instance.phases', []).map(phase => phase.fields)
							),
						defaults: caseObj.instance.attributes
					},
					required: true
				}
			],
			mutate: ( values ) => {

				let fields =
						indexBy(
							flatten(
								get(casetype, 'instance.phases', []).map(phase => phase.fields)
							),
							'magic_string'
						),
					settings =
						mapKeys(
							values.attributes,
							( value, key ) => {

								let field = fields[key];

								return `bibliotheek_kenmerk_${field.catalogue_id}`;

							}
						);

				return {
					type: 'case/set_setting',
					data: {
						caseId: getCaseId(caseObj),
						settings
					}
				};

			},
			context: 'admin'
		},
		{
			name: 'fase',
			label: 'Fase wijzigen',
			fields: [
				{
					name: 'phase',
					template: 'select',
					data: {
						options:
							casePhases.slice(
								1,
								caseObj.instance.phase ?
									findIndex(casePhases, { name: caseObj.instance.phase }) + 1
									: casePhases.length
							)
								.map(phase => {
									return {
										value: phase.seq,
										label: phase.name
									};
								}),
						notSelectedLabel: caseObj.instance.phase ?
							null
							: '[Afgehandeld]'
					},
					defaults: caseObj.instance.phase ?
						findIndex(casePhases, { name: caseObj.instance.phase }) + 1
						: null,
					required: true
				}
			],
			mutate: ( values ) => {

				let data = {
						caseId: getCaseId(caseObj),
						settings: {
							// milestone is zero-indexed
							milestone: values.phase - 1
						}
					};

				return {
					type: 'case/set_setting',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'status',
			label: 'Status wijzigen',
			fields: [
				{
					name: 'status',
					template: 'select',
					required: true,
					data: {
						options: [
							{
								value: 'new',
								label: 'Nieuw'
							},
							{
								value: 'open',
								label: 'In behandeling'
							},
							{
								value: 'stalled',
								label: 'Opgeschort'
							},
							{
								value: 'resolved',
								label: 'Afgehandeld'
							}
						]
					},
					defaults: caseObj.instance.status
				}
			],
			mutate: ( values ) => {

				let data = {
					caseId: getCaseId(caseObj),
					settings: values
				};

				return {
					type: 'case/set_setting',
					data
				};

			},
			context: 'admin'
		},
		{
			name: 'zaaktype',
			label: 'Zaaktype wijzigen',
			fields: [
				{
					name: 'casetype',
					label: 'Zaaktype',
					template: 'object-suggest',
					data: {
						objectType: 'casetype'
					},
					required: true
				}
			],
			mutate: ( values ) => {

				let data =
					{
						caseId: getCaseId(caseObj),
						settings: {
							zaaktype_id: get(values, 'casetype.data.values.casetype_id')
						}
					};

				return {
					type: 'case/set_setting',
					data
				};

			},
			options: {
				reloadRoute: true
			},
			context: 'admin'
		},
		{
			name: 'result',
			label: 'Resultaat wijzigen',
			fields: [
				{
					name: 'result',
					label: 'Resultaat',
					template: 'radio',
					data: {
						options: get(casetype, 'instance.results', [])
							.map( ( result ) => {
								return {
									// we need id, not type for set_setting
									value: result.resultaat_id,
									label: capitalize(result.label || result.type)
								};
							})
					},
					required: true
				}
			],
			mutate: ( values ) => {

				let data = {
						caseId: getCaseId(caseObj),
						settings: {
							resultaat: values.result
						}
					};

				return {
					type: 'case/set_setting',
					data
				};

			},
			visible: isAdmin && caseObj.instance.status === 'resolved',
			context: 'admin'
		}
	]
		.filter(action => {

			let visible;

			if (action.visible !== undefined) {
				visible = action.visible;
			} else if (action.context === 'admin') {
				visible = isAdmin;
			} else {
				visible = true;
			}

			return visible;
		});
};
