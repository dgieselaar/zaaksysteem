/*eslint-disable*/
export default ( $compile, $interpolate ) => {

	let registrations = {},
		defaultWrapper,
		compilers = {},
		linkFns = {};

	let reduceEl = ( type, reducers ) => {

	};

	let createLinkFn = ( type ) => {
		
		let registration = registrations[type],
			localCompilers =
				assign(
					{
						wrapper: compilers.defaults.wrapper
					},
					mapValues(
						indexBy('display control'.split(' '),
						( value, key ) => {
							return {
								[key]:
									registrations[type][key] ?
										getControlCompiler(key, registrations[type][key])
										: compilers.defaults[key]
							}; 
						}
					)
				)
			),
			fn;

		if (!registration) {
			throw new Error(`Cannot create link fn for ${type}. No registration was found.`);
		}

		fn = ( scope, element ) => {

			localCompilers.wrapper(scope, ( el ) => {

				'display control'.split(' ').forEach( controlType => {
					localCompilers[controlType](scope, ( e ) => {
						el.find(`vorm-${controlType}`).replaceWith(e);
					});
				});

			});
		};

	};

	let getTypeLinkFn = ( type ) => {

		return linkFns[type]
			|| createLinkFn(type);
		
	};

	let registerType = ( name, template ) => {

		registrations[name] = template;
		
	};

	let compileTo = ( template, scope, element ) => {

		if (typeof template === 'string') {
			getTypeLinkFn(template)(scope, element);
		}
		
	};
	
};

