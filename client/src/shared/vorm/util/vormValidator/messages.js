import dateformat from 'dateformat';

export default
	{
		email: 'Dit is geen geldig email-adres.',
		number: 'Dit is geen valide getal.',
		required: 'Dit veld is verplicht.',
		minDate: [ '$minDate', ( $minDate ) => {
			return `De eerst mogelijke datum is ${dateformat($minDate, 'dd-mm-yyyy')}.`;
		}],
		maxDate: [ '$maxDate', ( $maxDate ) => {
			return `De laatst mogelijke datum is ${dateformat($maxDate, 'dd-mm-yyyy')}.`;
		}]
	};
