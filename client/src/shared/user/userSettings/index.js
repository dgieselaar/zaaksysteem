import angular from 'angular';
import resourceModule from './../../api/resource';
import actionsModule from './actions';
import get from 'lodash/object/get';
import toPath from 'lodash/internal/toPath';
import set from 'lodash/object/set';
import first from 'lodash/array/first';
import assign from 'lodash/object/assign';

export default
	angular.module('userSettings', [
		resourceModule,
		actionsModule
	])
		.factory('userSettings', [ '$rootScope', 'resource', ( $rootScope, resource ) => {

			let createResource = ( scope ) => {
				return resource(
					'/api/user/settings',
					{
						scope,
						cache: {
							// only fetch once every five minutes
							every: 1000 * 60 * 5
						}
					}
				);
			};

			let settings = createResource($rootScope);

			return {
				set: ( key, value ) => {

					let path = toPath(key),
						data = assign(
							{},
							first(settings.data()),
							set({}, key, value)
						),
						actualKey = first(path),
						actualValue = get(data, actualKey);

					settings.mutate('set_setting', { key: actualKey, value: actualValue });
				},
				get: ( key ) => {
					return get(settings.data(), `[0]${key}`);
				},
				createResource
			};

		}])
		.name;
