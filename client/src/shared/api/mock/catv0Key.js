import get from 'lodash/object/get';
import indexBy from 'lodash/collection/indexBy';
import startsWith from 'lodash/string/startsWith';
import pick from 'lodash/object/pick';

export default( valueKey, val, idKey = 'id' ) => {

	let idVal = get(val, `case.${valueKey}.${idKey}`);

	if (idVal === undefined || idVal === null) {
		return null;
	}

	return {
		instance: indexBy(
			pick(
				val,
				( value, key ) => startsWith(key, `case.${valueKey}`)
			),
			( value, key ) => {

				return startsWith(key, `case.${valueKey}.`) ?
					key.replace(`case.${valueKey}.`, '')
					: key.replace('case.', '');
			}
		)
	};
};
