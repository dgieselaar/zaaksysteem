import mockData from './success.json';
import merge from 'lodash/object/assign';

export default {
	mockSuccess: ( data, options = { version: 'v1' } ) => {
		let response;

		switch (options.version) {
			case 'v1':
			response = merge({}, mockData.v1, { result: { instance: { rows: data } } });
			break;

			case 'v0':
			response = merge({}, mockData.v0, { result: data } );
			break;

			case 'legacy':
			response = merge({}, mockData.legacy, { json: { entries: data } } );
			break;
		}

		return response;
	}
};
