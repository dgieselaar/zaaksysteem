import angular from 'angular';
import urlParser from 'url';
import find from 'lodash/collection/find';
import assign from 'lodash/object/assign';
import pick from 'lodash/object/pick';
import pluck from 'lodash/collection/pluck';
import omit from 'lodash/object/omit';
import isEqual from 'lodash/lang/isEqual';
import shortid from 'shortid';
import invoke from 'lodash/collection/invoke';
import propCheck from './../../util/propCheck';
import indexBy from 'lodash/collection/indexBy';
import cachedCollectionModule from './../../util/cachedCollection';

export default
	angular.module('shared.api.cacher', [
		cachedCollectionModule
	])
		.provider('apiCacher', [ ( ) => {

			let limit = 4 * 1024 * 1024,
				comparator = isEqual;

			return {
				$get: [ '$rootScope', 'cachedCollection', ( $rootScope, cachedCollection ) => {

					let cacher = {},
						updateListeners = [],
						responses = cachedCollection.array('apiResponses'),
						cachesById = {};

					let split = ( url, params ) => {
						let urlObj = urlParser.parse(url, true);

						return assign({ $pathname: urlObj.pathname.replace(/\/$/, '') }, params, urlObj.query);
					};

					let dispatchUpdate = ( ids ) => {
						invoke(updateListeners, 'call', null, ids);
					};

					let compact = ( collection ) => {
						return collection.map(response => pick(response, '$id', '$lastUpdated'));
					};

					let onUpdate = ( now, prev ) => {

						let prevResponses = compact(prev),
							diff;

						diff = now.filter( response => {
							return !find(prevResponses, pick(response, '$id', '$lastUpdated'));
						});

						cachesById = indexBy(now, '$id');

						dispatchUpdate(pluck(diff, '$id'));

					};

					let findCache = ( from, req ) => {
						return find(from, ( cache ) => {
							return isEqual(req, omit(cache, '$data', '$id', '$lastUpdated'));
						});
					};

					cacher.get = ( preferredOptions ) => {

						let options = preferredOptions,
							spl = split(options.url, options.params);

						propCheck.throw(
							propCheck.oneOfType([
								propCheck.shape({
									url: propCheck.string,
									params: propCheck.object.optional
								}),
								propCheck.string
							]),
							options
						);

						if (typeof options === 'string') {
							options = { url: options };
						}

						return findCache(responses.value(), spl);
					};

					cacher.store = ( preferredOptions, data ) => {

						let caches = responses.value(),
							options = preferredOptions,
							update;

						propCheck.throw(
							propCheck.shape({
								data: propCheck.any,
								options: propCheck.oneOfType([
									propCheck.shape({
										url: propCheck.string,
										params: propCheck.object.optional
									}),
									propCheck.string
								])
							}),
							{
								data,
								options
							}
						);

						if (typeof options === 'string') {
							options = { url: options };
						}

						let { url, params } = options,
							spl = split(url, params),
							response = findCache(caches, spl);

						if (!response) {
							response = assign(spl, {
								$id: shortid()
							});
						} else {
							caches = caches.filter(r => r !== response);
						}

						update = !comparator(data, response.$data);

						response = assign({}, response, {
							$data: data,
							$lastUpdated: Date.now()
						});

						caches = [ response ].concat(caches);

						while (caches.length > 0 && (JSON.stringify(caches).length > limit)) {
							caches = caches.slice(0, -1);
						}

						responses.replace(caches, update);

					};

					cacher.clear = ( ) => {
						responses.replace([]);
					};

					responses.onUpdate.push(onUpdate);

					cacher.caches = ( ) => responses.value();

					cacher.getCacheById = ( id ) => cachesById[id];

					cacher.onUpdate = updateListeners;

					return cacher;

				}],
				setLimit: ( l ) => {
					propCheck.throw(propCheck.number, l);
					limit = l;
				},
				setIsEqual: ( fn ) => {
					comparator = fn;
				}
			};

		}])
		.name;
