import angular from 'angular';
import template from './template.html';
import controller from './controller';
import zsPositionFixedModule from './../zsPositionFixed';
import zsIcon from './../zsIcon';
import './dropdown-menu.scss';

export default
	angular.module('shared.ui.zsDropdownMenu', [
			zsIcon,
			zsPositionFixedModule
		])
		.directive('zsDropdownMenu', [ '$animate', '$document', 'capabilities', ( $animate, $document, capabilities ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					options: '&',
					positionOptions: '&',
					buttonIcon: '@',
					buttonLabel: '@',
					buttonStyle: '&',
					mode: '&',
					isFixed: '&',
					onOpen: '&',
					onClose: '&',
					emptyLabel: '@'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					controller.call(this, $document, $scope, $element, $animate, capabilities());

				}],
				controllerAs: 'vm'
			};

	}]).name;
