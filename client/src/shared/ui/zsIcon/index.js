import angular from 'angular';

export default
	angular.module('shared.ui.zsIcon', [
	])
	.directive('zsIcon', ( ) => {

		return {
			restrict: 'E',
			scope: {
				iconType: '@'
			},
			template: `<i class="mdi mdi-{{iconType}}"></i>`
		};

	})
		.name;
