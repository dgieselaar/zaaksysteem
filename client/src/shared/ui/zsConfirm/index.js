import angular from 'angular';
import zsModalModule from './../zsModal';
import template from './template.html';

export default
	angular.module('zsConfirm', [
		zsModalModule
	])
		.factory('zsConfirm', [ '$interpolate', '$rootScope', '$compile', '$q', 'zsModal', ( $interpolate, $rootScope, $compile, $q, zsModal ) => {

			let tpl = template;

			if ($interpolate.startSymbol() !== '{{') {
				tpl = tpl.replace(/{{/g, $interpolate.startSymbol())
					.replace(/}}/g, $interpolate.endSymbol());
			}


			return ( label, verb = 'Bevestigen' ) => {

				return $q( ( resolve, reject ) => {

					let scope = $rootScope.$new(true),
						modal;

					let cleanup = ( ) => {
						scope.$destroy();
					};

					scope.vm = {
						label,
						verb,
						onConfirm: ( ) => {
							modal.close();
							cleanup();
							resolve();
						},
						onCancel: ( ) => {
							modal.close();
							cleanup();
							reject();
						}
					};

					modal = zsModal({
						title: 'Bevestigen',
						el: $compile(tpl)(scope),
						classes: 'confirm'
					});

					modal.onClose(( ) => {
						cleanup();
						reject();
					});

					modal.open();

				});

			};

		}])
		.name;
