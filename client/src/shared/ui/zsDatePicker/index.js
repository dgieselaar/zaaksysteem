import angular from 'angular';
import once from 'lodash/function/once';
import dateformat from 'dateformat';

export default
	angular.module('zsDatePicker', [
	])
		.directive('zsDatePicker', [ '$q', ( $q ) => {

			let load = once(
				( ) => {
					return $q( ( resolve/*, reject*/ ) => {
						require([ 'moment', 'pikaday', './styles.scss' ], ( ...rest ) => {
							resolve(rest);
						});
					});
				}
			);

			return {
				restrict: 'A',
				require: 'ngModel',
				link: ( scope, element, attrs, ngModel ) => {

					let picker;

					let setDatePicker = ( value ) => {

						let val = value === undefined ? ngModel.$modelValue : value;

						if (picker) {
							picker.setDate(val ? dateformat('DD-MM-YYYY') : val);
						}

					};

					ngModel.$parsers.unshift(( val ) => {
						let value = val;

						if (val && val.match(/^\d{1,2}-\d{1,2}-\d{4}$/)) {

							let values = val.split('-');

							value = new Date(values[2], values[1] - 1, values[0]);

							value = dateformat(value, 'yyyy-mm-dd');
						}

						setDatePicker(value);

						return value;
					});

					ngModel.$formatters.unshift(( val ) => {
						let value = val ?
							dateformat(val, 'dd-mm-yyyy')
							: val;

						return value;
					});

					load().then(( libs ) => {

						let Pikaday = libs[1];

						picker = new Pikaday({
							field: element[0],
							format: 'DD-MM-YYYY',
							i18n: {
								previousMonth: 'Vorige maand',
								nextMonth: 'Volgende maand',
								months: [ 'Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December' ],
								weekdays: [ 'Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag' ],
								weekdaysShort: [ 'Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Zat']
							}
						});

						setDatePicker();

					});

				}
			};

		}])
		.name;
