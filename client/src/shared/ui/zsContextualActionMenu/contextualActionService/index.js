import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import auxiliaryRouteModule from './../../../util/route/auxiliaryRoute';
import viewRegistrarModule from './../../../util/route/viewRegistrar';
import getActiveStates from './../../../util/route/getActiveStates';
import seamlessImmutable from 'seamless-immutable';
import find from 'lodash/collection/find';
import flatten from 'lodash/array/flatten';
import assign from 'lodash/object/assign';
import without from 'lodash/array/without';
import includes from 'lodash/collection/includes';
import invoke from 'lodash/collection/invoke';
import pull from 'lodash/array/pull';

export default
	angular.module('contextualActionService', [
		angularUiRouterModule,
		auxiliaryRouteModule,
		viewRegistrarModule
	])
		.config([ '$stateProvider', ( $stateProvider ) => {

			let orig = $stateProvider.state;

			$stateProvider.state = ( ...rest ) => {

				let name, opts;

				if (rest.length === 1) {
					opts = rest[0];
					name = opts.name;
				} else {
					name = rest[0];
					opts = rest[1];
				}

				orig(...rest);

				if (opts.actions) {

					let auxName = name === 'root' ? 'add' : `${name}-add`;

					orig(auxName, {
						auxiliary: true,
						url: '/toevoegen/:actionName',
						onActivate: [ '$state', '$stateParams', 'contextualActionService', ( $state, $stateParams, contextualActionService ) => {

							let action = find(contextualActionService.getAvailableActions(), { name: $stateParams.actionName }),
								fn = ( ) => {
									pull(contextualActionService.onClose, fn);

									$state.go('^');

									return false;
								};

							if (action) {
								contextualActionService.openAction(action);
							}

							contextualActionService.onClose.push(fn);

						}],
						onDeactivate: [ '$stateParams', 'contextualActionService', ( $stateParams, contextualActionService ) => {

							let action = find(contextualActionService.getOpenActions(), { name: $stateParams.actionName });

							if (action) {
								contextualActionService.closeAction(action);
							}

						}]
					});
				}

				return $stateProvider;
			};

		}])
		.factory('contextualActionService', [ '$state', '$rootScope', 'viewRegistrar', ( $state, $rootScope, viewRegistrar ) => {

			let service,
				openActions = seamlessImmutable([]),
				availableActions = seamlessImmutable([]),
				onClose = [];

			service = {
				getAvailableActions: ( ) => availableActions,
				getOpenActions: ( ) => openActions,
				openAction: ( action, params ) => {

					openActions = seamlessImmutable(
						[ assign({}, action, { params }) ]
					);

				},
				closeAction: ( action ) => {
					openActions = seamlessImmutable(without(openActions, action));
					invoke(onClose, 'call', null, action);
				},
				closeAllActions: ( ) => {
					
					invoke(onClose, 'call', null, openActions);

					openActions = seamlessImmutable([]);
				},
				findActionByName: name => find(service.getAvailableActions(), { name }),
				onClose
			};

			let setAvailableActions = ( ) => {

				let states = getActiveStates($state.$current);

				availableActions = flatten(
					states.map(( state, index ) => {

						let type;

						switch (states.length - index) {
							case 2:
							type = 'primary';
							break;

							default:
							type = 'secondary';
							break;
						}

						return state.self.actions ?
							state.self.actions.map(
								action => {

									let view = viewRegistrar.getView(state.self.name),
										controller = view ? view.controller() : null,
										merged;

									merged = assign({}, action, {
										click: ( ) => {
											
											if (!includes(openActions, action)) {
												service.openAction(merged);
											}
										},
										type,
										controller
									});

									return merged;
								}
							).reverse()
							: [];
					})

				).reverse();

				availableActions = seamlessImmutable(availableActions);

			};

			$rootScope.$on('$viewContentLoaded', setAvailableActions);

			return service;

		}])
		.name;
