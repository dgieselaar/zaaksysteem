import angular from 'angular';
import composedReducerModule from './../../api/resource/composedReducer';
import shortid from 'shortid';
import seamlessImmutable from 'seamless-immutable';
import merge from 'lodash/object/merge';
import includes from 'lodash/collection/includes';
import without from 'lodash/array/without';
import template from './template.html';
import './styles.scss';

export default
	angular.module('zsRichTextArea', [
		composedReducerModule
	])
		.directive('zsRichTextArea', [ '$rootScope', '$sce', '$q', '$document', 'composedReducer', ( $rootScope, $sce, $q, $document, composedReducer ) => {

			let interactedPromise = $q.defer(),
				hasInteracted = false,
				load = ( ) => {

					return interactedPromise.promise.then(( ) => {

						hasInteracted = true;

						return $q(( resolve/*, reject*/ ) => {

							require([ 'quill' ], ( ...rest ) => {

								$rootScope.$evalAsync(( ) => resolve(rest));

							});

						});

					});

				};

			return {
				restrict: 'E',
				template,
				scope: {
					formats: '&',
					onChange: '&',
					html: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope, element ) {

					let ctrl = this,
						editor,
						id = shortid().replace(/^\d+/, ''),
						optionReducer,
						formats = 'bold italic strike underline size link bullet list align paste fullscreen'.split(' '),
						settings = seamlessImmutable({ pastePlain: true }),
						updateScheduled,
						loaded = false,
						loading = false,
						sanitizedHtml,
						tempWatcher;

					let scheduleUpdateOnBlur = ( ) => {

						if (!updateScheduled) {

							updateScheduled = true;

							$document[0].activeElement.addEventListener('blur', ( ) => {
								updateScheduled = false;
								editor.setHTML(ctrl.html());
							});
						}

					};

					optionReducer = composedReducer({ scope }, ctrl.formats, ( ) => settings)
						.reduce( ( availableFormats = formats, currentSettings ) => {

							let opts =
								[
									{
										name: 'bold',
										type: 'toggle',
										label: 'Dikgedrukt',
										icon: 'format-bold'
									},
									{
										name: 'italic',
										type: 'toggle',
										label: 'Cursief',
										icon: 'format-italic'
									},
									{
										name: 'strike',
										type: 'toggle',
										label: 'Doorgestreept',
										icon: 'format-strikethrough'
									},
									{
										name: 'underline',
										type: 'toggle',
										label: 'Onderstreept',
										icon: 'format-underline'
									},
									{
										name: 'size',
										type: 'dropdown',
										label: 'Grootte',
										data: {
											options: [ 0.9, 1, 1.1, 1.2 ]
												.map( ( size, index ) => {

													let label =
														[ 'Klein', 'Normaal', 'Groot', 'Extra groot' ][index];

													return {
														name: size,
														label,
														click: ( ) => {

															let range,
																format = {
																	size: `${size}em`
																};

															// editor needs focus, else range is null

															editor.focus();

															range = editor.getSelection();

															if (range) {
																editor.formatText(range.start, range.end, format);
																ctrl.triggerChange();
															}
															
														}
													};
												})
										},
										icon: 'format-size'
									},
									{
										name: 'link',
										type: 'toggle',
										label: 'Link',
										icon: 'link'
									},
									{
										name: 'bullet',
										type: 'toggle',
										label: 'Lijst',
										icon: 'format-list-bulleted'
									},
									{
										name: 'list',
										type: 'toggle',
										label: 'Genummerde lijst',
										icon: 'format-list-numbers'
									},
									{
										name: 'align',
										type: 'dropdown',
										label: 'Uitlijning',
										data: {
											options: [ 'left', 'right', 'center', 'justify' ]
												.map(align => {

													let label = {
														left: 'Links',
														right: 'Rechts',
														center: 'Centreren',
														justify: 'Uitvullen'
													}[align];


													return {
														name: align,
														iconClass: `format-align-${align}`,
														label,
														click: ( ) => {

															let range,
																format = {
																	align
																};

															// editor needs focus, else range is null

															editor.focus();

															range = editor.getSelection();

															if (range) {
																editor.formatLine(range.start, range.end, format);
																ctrl.triggerChange();
															}

														}
													};
												})
										},
										icon: 'format-align-left'
									},
									{
										name: 'paste',
										type: 'click',
										label: 'Behoud opmaak bij plakken',
										icon: 'clipboard-text',
										click: ( ) => {

											ctrl.handlePasteToggle();

										},
										format: false,
										buttonClasses: {
											'ql-active': !currentSettings.pastePlain
										}
									},
									{
										name: 'fullscreen',
										type: 'click',
										label: settings.fullscreen ?
											'Normale weergave'
											: 'Volledig scherm',
										click: ( ) => {

											settings = settings.merge({
												fullscreen: !settings.fullscreen
											});
										},
										icon: settings.fullscreen ?
											'fullscreen-exit'
											: 'fullscreen'
									}
								];

							return opts.filter(option => !availableFormats || includes(availableFormats, option.name))
								.map(
									option => {

										let opt =
											merge(
												{ id: shortid() },
												{
													buttonClasses: {
														[`ql-${option.name}`]: true
													}
												},
												option
											);

										return opt;

									}
								);

						});

					ctrl.triggerChange = ( ) => {
						ctrl.onChange({ $html: editor.getHTML() });
					};

					ctrl.getUuid = ( ) => id;

					ctrl.getToolbarOptions = optionReducer.data;

					ctrl.handlePasteToggle = ( ) => {
						settings = settings.merge({ pastePlain: !settings.pastePlain });
					};

					ctrl.isFullscreen = ( ) => !!settings.fullscreen;

					ctrl.getSanitizedHtml = ( ) => sanitizedHtml;

					ctrl.isLoaded = ( ) => loaded;

					ctrl.isLoading = ( ) => loading;

					let loadEditor = ( ) => {

						loading = true;

						load().then(( libs ) => {

							let [ Quill ] = libs;

							editor = new Quill(`#${id}-editor`, {
								formats:
									without(
										ctrl.formats() ?
											ctrl.formats()
											: formats,
										'paste',
										'fullscreen'
									),
								modules: {
									'toolbar': {
										container: `#${id}-toolbar`
									},
									'link-tooltip': {
										template:
											`<span class="title">
												Bezoek link:&nbsp;
											</span>
											<a href="#" class="url" target="_blank" href="about:blank"></a>
											<input class="input" type="text"/>
											<span>&nbsp;&#45;&nbsp;</span>
											<a href="javascript:;" class="change">Wijzig</a>
											<a href="javascript:;" class="remove">Verwijder</a>
											<a href="javascript:;" class="done">Klaar</a>`
									},
									'paste-manager': {
										onConvert ( ...rest ) {

											let pasteManager = editor.getModule('paste-manager'),
												delta = pasteManager._onConvert.call(pasteManager, ...rest); //eslint-disable-line

											if (settings.pastePlain) {
												delta.ops = delta.ops.map(
													op => {

														delete op.attributes;

														return op;
													}
												);
											}

											return delta;

										}
									}
								}
							});

							if (tempWatcher) {
								tempWatcher();
							}

							scope.$watch(ctrl.html, ( html ) => {
								
								if (editor.getHTML() !== html) {

									// prevent field from being updated when focused

									if (element[0].contains($document[0].activeElement)) {
										scheduleUpdateOnBlur();
									} else {
										editor.setHTML(html || '');
									}
									
								}
							});

							editor.on('text-change', ( delta, source ) => {

								if (source === 'user') {
									ctrl.triggerChange();
								}

							});

							let onKeyUp = ( event ) => {

								if (event.keyCode === 27 && settings.fullscreen) {
									scope.$evalAsync( ( ) => {
										settings = settings.merge({ fullscreen: false });
									});
								}


							};

							$document.bind('keyup', onKeyUp);

						})
							.catch( ( err ) => {

								console.error(err);

							})
							.finally(( ) => {

								loading = false;

								loaded = true;

							});

					};

					let onMouseOver = ( ) => {

						element.unbind('mouseover', onMouseOver);

						if (!loaded) {
							scope.$evalAsync(( ) => {
								interactedPromise.resolve();
								loadEditor();
							});
						}

					};

					if (!hasInteracted) {

						tempWatcher = scope.$watch(( ) => ctrl.html(), val => {
							sanitizedHtml = $sce.trustAsHtml(val);
						});

						element.bind('mouseover', onMouseOver);

					} else {
						loadEditor();
					}

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
