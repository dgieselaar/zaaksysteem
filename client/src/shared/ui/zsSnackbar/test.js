import angular from 'angular';
import 'angular-mocks';
import SomaEvents from 'soma-events';
import zsSnackbarModule from '.';
import snackbarServiceModule from './snackbarService';

describe('zsSnackbar', ( ) => {

	let $rootScope,
		$compile,
		scope,
		el,
		ctrl,
		snackbarService;

	beforeEach(angular.mock.module(zsSnackbarModule, snackbarServiceModule));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', 'snackbarService', ( ...rest ) => {

		[ $rootScope, $compile, snackbarService ] = rest;

		scope = $rootScope.$new();

		el = angular.element(`<zs-snackbar/>`);

		$compile(el)(scope);

		ctrl = el.controller('zsSnackbar');

	}]));

	it('should return the snacks from the service', ( ) => {

		expect(ctrl.getSnacks()).toBe(snackbarService.getSnacks());

	});

	it('should remove a snack when clicked', ( ) => {

		let snack = snackbarService.info('foo'),
			event = new SomaEvents.Event('click');

		ctrl.handleCloseClick(snack, event);

		expect(snackbarService.getSnacks()).not.toContain(snack);

	});

	it('should expand', ( ) => {

		ctrl.expand();

		expect(ctrl.isExpanded()).toBe(true);

	});

	it('should contract', ( ) => {

		ctrl.contract();

		expect(ctrl.isExpanded()).toBe(false);

	});

	it('should not be expanded by default', ( ) => {

		expect(ctrl.isExpanded()).toBe(false);

	});
	
	it('should call the click handler of an action and remove the snack', ( ) => {

		let action =
				{
					label: 'foo',
					id: 'foo',
					click: jasmine.createSpy('click')
				},
			snack =
				snackbarService.info('foo', {
					actions: [
						action
					]
				});

		ctrl.handleActionClick(snack, action);

		expect(action.click).toHaveBeenCalled();

		expect(snackbarService.getSnacks()).not.toContain(snack);

	});

	it('should not return a style object when expanded', ( ) => {

		let snack =
			snackbarService.info('foo');

		ctrl.expand();

		expect(ctrl.getSnackStyle(snack, 0)).toBeUndefined();

	});

	it('should return a style object when contracted', ( ) => {

		let snack =
			snackbarService.info('foo'),
			style;

		ctrl.contract();

		style = ctrl.getSnackStyle(snack, 0);

		expect(style).toBeDefined();

		expect('transform' in style).toBe(true);

	});


});
