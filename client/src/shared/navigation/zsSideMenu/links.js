import includes from 'lodash/collection/includes';

export default ( opts ) => {

	let { user, $state, auxiliaryRouteService } = opts;

	let aboutStateName = auxiliaryRouteService.append($state.current, 'about');

	return [
		{
			name: 'user',
			children: [
				{
					name: 'dashboard',
					label: 'Dashboard',
					href: $state.href('home') || '/intern',
					icon: 'home',
					when: includes(user.system_permissions, 'dashboard')
				},
				{
					name: 'intake',
					label: 'Documentintake',
					href: '/zaak/intake?scope=documents',
					icon: 'file',
					when: includes(user.system_permissions, 'documenten_intake_subject')
				},
				{
					name: 'search',
					label: 'Uitgebreid zoeken',
					href: '/search',
					icon: 'magnify',
					when: includes(user.system_permissions, 'search')
				},
				{
					name: 'search_contact',
					label: 'Contacten zoeken',
					href: '/betrokkene/search',
					icon: 'account-search',
					when: includes(user.system_permissions, 'contact_search')
				}
			]
		},
		{
			name: 'admin',
			children: [
				{
					name: 'library',
					label: 'Catalogus',
					href: '/beheer/bibliotheek',
					icon: 'puzzle',
					when: includes(user.system_permissions, 'beheer_zaaktype_admin')
				},
				{
					name: 'users',
					label: 'Gebruikers',
					href: '/medewerker',
					icon: 'account-multiple',
					when: includes(user.system_permissions, 'admin')
				},
				{
					name: 'log',
					label: 'Logboek',
					href: '/beheer/logging',
					icon: 'console',
					when: includes(user.system_permissions, 'admin')
				},
				{
					name: 'transactions',
					label: 'Transactieoverzicht',
					href: '/beheer/sysin/transactions',
					icon: 'swap-vertical',
					when: includes(user.system_permissions, 'admin')
				},
				{
					name: 'interfaces',
					label: 'Koppelingconfiguratie',
					href: '/beheer/sysin/overview',
					icon: 'link',
					when: includes(user.system_permissions, 'admin')
				},
				{
					name: 'config',
					label: 'Configuratie',
					href: '/beheer/configuration',
					icon: 'settings',
					when: includes(user.system_permissions, 'admin')
				}
			]
		},
		{
			name: 'info',
			children: [
				{
					name: 'info',
					label: 'Over Zaaksysteem.nl',
					href: $state.href(aboutStateName) || '/intern/!over',
					icon: 'information'
				}
			]
		}
	];

};
