import angular from 'angular';
import snackbarServiceModule from './../../../ui/zsSnackbar/snackbarService';
import resourceModule from './../../../api/resource';
import composedReducerModule from './../../../api/resource/composedReducer';
import zsTruncateModule from './../../../ui/zsTruncate';
import first from 'lodash/array/first';
import template from './template.html';
import './styles.scss';

export default
	angular.module('zsNotificationList', [
		snackbarServiceModule,
		resourceModule,
		composedReducerModule,
		zsTruncateModule
	])
		.directive('zsNotificationList', [
			'$http', '$q', '$document', '$animate', '$state', 'snackbarService', 'resource', 'composedReducer', 'dateFilter',
			( $http, $q, $document, $animate, $state, snackbarService, resource, composedReducer, dateFilter ) => {


			return {
				restrict: 'E',
				template,
				scope: {
					onReadMore: '&',
					notifications: '&',
					isLoading: '&',
					onNavigate: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope ) {

					let ctrl = this,
						notificationReducer;

					notificationReducer = composedReducer({ scope }, ctrl.notifications)
						.reduce( notifications => {

							return notifications.map(
								notification => {

									let isRead = notification.is_read === 1,
										type = notification.logging_id.event_type,
										category = first(type.split('/')),
										title = notification.logging_id.description,
										icon,
										link,
										caseId = notification.logging_id.case_id,
										description,
										message;

									if (caseId) {
										link = {
											href: $state.href('case', { caseId } ) || `/intern/zaak/${caseId}/`,
											label: `Zaak ${caseId}`
										};
									}

									switch (type) {
										case 'user/apply':
										icon = 'account-star-variant';
										break;

										case 'case/update/field':
										case 'case/pip/updatefield':
										icon = 'pencil';
										break;

										default:
										case 'email/send':
										icon = 'email';
										break;

										case 'case/document/create':
										icon = 'file';
										break;

										case 'case/document/accept':
										icon = 'file-check';
										break;

										case 'case/pip/feedback':
										icon = 'message-text';
										message = {
											sender: notification.aanvrager,
											content: notification.logging_id.content
										};
										break;
									}

									return {
										id: notification.id,
										title,
										description,
										type,
										category,
										icon,
										link,
										message,
										classes: {
											read: isRead
										},
										timestamp: dateFilter(notification.logging_id.timestamp, 'dd-MM-yyyy, HH:mm')
									};
								}
							);

						});

					ctrl.getNotifications = notificationReducer.data;

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
