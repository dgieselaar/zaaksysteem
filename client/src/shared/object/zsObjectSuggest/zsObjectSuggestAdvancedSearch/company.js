import assign from 'lodash/object/assign';
import mapKeys from 'lodash/object/mapKeys';
import omit from 'lodash/object/omit';
import pick from 'lodash/object/pick';
import identity from 'lodash/utility/identity';

export default
	{
		request: ( values ) => {

			return {
				method: 'POST',
				url: !values.remote ?
					'/api/v1/subject'
					: `/api/v1/subject/remote_search/`,
				data:
					{
						query: {
							match: assign(
								{
									subject_type: 'company'
								},
								mapKeys(
									omit(
										pick(values, identity),
										'remote'
									),
									( value, key ) => `subject.${key}`
								)
							)
						}
					}
			};
		},
		fields: [
			{
				name: 'coc_number',
				label: 'KVK nummer',
				template: 'text'
			},
			{
				name: 'company',
				label: 'Handelsnaam',
				template: 'text'
			},
			{
				name: 'address_residence.street',
				label: 'Straat',
				template: 'text'
			},
			{
				name: 'address_residence.zipcode',
				label: 'Postcode',
				template: 'text'
			},
			{
				name: 'address_residence.street_number',
				label: 'Huisnummer',
				template: 'text'
			},
			{
				name: 'address_residence.street_number_letter',
				label: 'Huisletter',
				template: 'text'
			},
			{
				name: 'address_residence.street_number_suffix',
				label: 'Huisnummer toevoeging',
				template: 'text'
			},
			{
				name: 'address_residence.city',
				label: 'Plaats',
				template: 'text'
			}
		],
		columns:
			[
				{
					id: 'coc_number',
					label: 'KVK-nummer'
				},
				{
					id: 'company',
					label: 'Handelsnaam'
				},
				{
					id: 'city',
					label: 'Plaats',
					resolve: 'address_residence.instance.city'
				},
				{
					id: 'zipcode',
					label: 'Postcode',
					resolve: 'address_residence.instance.zipcode'
				},
				{
					id: 'street',
					label: 'Straat',
					template:
						`<span>{{::item.address}}</span>`
				}
			]
	};
