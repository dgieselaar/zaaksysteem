import isEmpty from 'lodash/lang/isEmpty';
import pick from 'lodash/object/pick';
import identity from 'lodash/utility/identity';
import assign from 'lodash/object/assign';
import mapKeys from 'lodash/object/mapKeys';
import omit from 'lodash/object/omit';

let remoteFields =
	[ 'address_residence.zipcode', 'address_residence.street_number' ];

let isBsnRequired = ( vals ) => {
	return vals.remote && isEmpty(
		pick(
			pick(vals, ...remoteFields),
			identity
		)
	);
};

export default {
	request: ( values ) => {

		return {
			method: 'POST',
			url: !values.remote ?
				'/api/v1/subject'
				: `/api/v1/subject/remote_search/`,
			data:
				{
					query: {
						match: assign(
							{
								subject_type: 'person'
							},
							mapKeys(
								omit(
									pick(values, identity),
									'remote'
								),
								( value, key ) => `subject.${key}`
							)
						)
					}
				}
		};
	},
	fields:
		[
			{
				name: 'personal_number',
				label: 'BSN',
				template: 'text',
				required: [ '$values', isBsnRequired ]
			},
			{
				name: 'prefix',
				label: 'Voorvoegsel',
				template: 'text'
			},
			{
				name: 'family_name',
				label: 'Achternaam',
				template: 'text'
			},
			{
				name: 'gender',
				label: 'Geslacht',
				template: 'radio',
				data: {
					options: [
						{
							value: 'M',
							label: 'Man'
						},
						{
							value: 'V',
							label: 'Vrouw'
						}
					]
				}
			},
			{
				name: 'date_of_birth',
				label: 'Geboortedatum',
				template: 'date'
			},
			{
				name: 'address_residence.street',
				label: 'Straat',
				template: 'text'
			},
			{
				name: 'address_residence.zipcode',
				label: 'Postcode',
				template: 'text',
				required: [ '$values', ( vals ) => {

					return vals.remote && !vals.personal_number;

				}]
			},
			{
				name: 'address_residence.street_number',
				label: 'Huisnummer',
				template: 'text',
				required: [ '$values', ( vals ) => {

					return vals.remote && !vals.personal_number;

				}]
			},
			{
				name: 'address_residence.city',
				label: 'Woonplaats',
				template: 'text'
			},
			{
				name: 'remote',
				label: '',
				template: 'checkbox',
				data: {
					checkboxLabel: 'Zoeken in BRP'
				}
			}
		],
	columns:
		[
			{
				id: 'gender',
				template:
					`<zs-icon
						icon-type="alert-circle"
						ng-show="item.messages"
						zs-tooltip="{{::item.messages || ''}}"
					></zs-icon>
					<zs-icon icon-type="account"></zs-icon>`,
				label: ''
			},
			{
				id: 'personal_number',
				label: 'BSN'
			},
			{
				id: 'first_names',
				label: 'Voornamen'
			},
			{
				id: 'family_name',
				label: 'Achternaam'
			},
			{
				id: 'address',
				label: 'Adres',
				template:
					`<span>{{::item.address}}</span>`
			}
		]
};
