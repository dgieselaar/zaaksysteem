import matchCondition from './matchCondition';
import some from 'lodash/collection/some';
import every from 'lodash/collection/every';

export default
	( rule, values, hidden ) => {

		let conditions = rule.conditions.conditions,
			type = rule.conditions.type,
			func = type === 'and' ? every : some,
			matches = false;

		if (conditions.length === 0) {
			matches = true;
		} else {
			matches = func(conditions, condition => matchCondition(condition, values || {}, hidden || {}));
		}

		return matches;

	};
