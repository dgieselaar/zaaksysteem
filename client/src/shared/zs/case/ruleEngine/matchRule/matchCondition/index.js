import some from 'lodash/collection/some';
import isEqual from 'lodash/lang/isEqual';
import isArray from 'lodash/lang/isArray';
import includes from 'lodash/collection/includes';

export default
	( condition, values = {}, hidden = {}) => {

		let attrName = condition.attribute_name,
			attrValue = hidden[attrName] ? null : values[attrName],
			matches = false,
			isValArray = isArray(attrValue);

		if (condition.validates_true !== undefined) {
			matches = !!condition.validates_true;
		} else {
			matches = some(condition.values, val => {
				return isValArray ?
					includes(attrValue, val)
					: isEqual(val, attrValue);
			});
		}

		return matches;

	};
