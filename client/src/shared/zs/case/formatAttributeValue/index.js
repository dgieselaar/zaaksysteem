import indexBy from 'lodash/collection/indexBy';
import identity from 'lodash/utility/identity';
import mapValues from 'lodash/object/mapValues';
import isArray from 'lodash/lang/isArray';
import compact from 'lodash/array/compact';

export default ( attribute, source ) => {

	let value = source;

	switch (attribute.type) {
		case 'checkbox':
		if (!isArray(value)) {
			value = [ value ];
		}

		value = mapValues(
			indexBy(compact(value), identity),
			( ) => true
		);

		if (Object.keys(value).length === 0) {
			value = null;
		}
		break;

		case 'date':
		if (value) {
			value = new Date(value);
		}
		break;
	}

	return value;

};
