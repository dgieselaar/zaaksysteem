import angular from 'angular';
import assign from 'lodash/object/assign';
import sortBy from 'lodash/collection/sortBy';
import pick from 'lodash/object/pick';
import get from 'lodash/object/get';
import formatAttributeValue from './../../case/formatAttributeValue';
import identity from 'lodash/utility/identity';
import indexBy from 'lodash/collection/indexBy';
import mapValues from 'lodash/object/mapValues';
import uniq from 'lodash/array/uniq';

let getOptions = ( values ) => {


	return uniq(
		sortBy(values, 'sort_order')
			.map( ( value ) => {
				return {
					label:
						!value.active ?
							`${value.value} (Inactief)`
							: value.value,
					value: value.value,
					active: value.active,
					classes: {
						inactive: !value.active
					}
				};
			}),
		'value'
	);
};

export default ( attr ) => {

	let config =
		{
			id: attr.id,
			name: attr.type === 'object' ?
				`object.${attr.object_type_prefix.toLowerCase()}`
				: attr.magic_string,
			label: attr.label,
			required: attr.required,
			description: attr.help,
			limit: attr.limit_values,
			$attribute: attr,
			type: attr.type,
			addLabel: attr.label_multiple
		};

	switch (attr.type) {

		case 'option':
		assign(config, { template: 'radio', data: { options: getOptions(attr.values) }, limit: 1 });
		break;

		case 'checkbox':
		assign(
			config,
			{
				template: 'checkbox-list',
				data: {
					options: getOptions(attr.values)
				},
				limit: 1,
				formatters: [
					( val ) => {
						return formatAttributeValue(attr, val);
					}
				],
				parsers: [
					( val ) => {
						return Object.keys(pick(val, identity));
					}
				]
			}
		);
		break;

		case 'text':
		assign(config, { template: 'text' });
		break;

		case 'numeric':
		assign(config, { template: 'number' });
		break;

		case 'url':
		assign(config, { template: 'url' });
		break;

		case 'image_from_url':

		assign(config, {
			template: 'image_from_url'
		});
		break;

		case 'email':
		assign(config, { template: 'email' });
		break;

		case 'date':
		assign(config, {
			template: 'date',
			formatters: [ ( val ) => {
				let value = formatAttributeValue(attr, val);

				return value;
			}]
		});
		break;

		case 'select':
		assign(config, { template: 'select', data: { options: getOptions(attr.values) } });
		break;

		case 'textarea':
		assign(config, { template: 'textarea' });
		break;

		case 'text_uc':
		assign(config, { template: 'text', formatters: [ ( val ) => (val || '').toUpperCase() ] });
		break;

		case 'subject':
		assign(
			config,
			{
				template: 'object-suggest',
				data: {
					objectType: 'contact',
					format: ( source ) => source.data,
					display: ( source ) => get(source, 'decorated_name', get(source, 'handelsnaam'))
				}
			}
		);
		break;

		case 'bag_adres':
		case 'bag_adressen':
		case 'bag_straat_adres':
		case 'bag_straat_adressen':
		assign(
			config,
			{
				template: 'object-suggest',
				data: {
					display: ( source ) => {
						return source ?
							get(source, 'human_identifier')
							: 'Geen adres geselecteerd';
					},
					objectType: 'bag',
					icon: 'map-marker',
					format: ( source ) => {

						let formatted =
							{
								bag_id: source.data.id,
								human_identifier: `${source.data.city} - ${source.data.street} ${source.data.number}`,
								address_data:
									assign(
										{ straat: source.data.street },
										pick(
											source.data,
											'gps_lat_lon',
											'huisletter',
											'huisnummer',
											'huisnummertoevoeging',
											'postcode',
											'woonplaats'
										)
									)
							};

						return formatted;
					}
				},
				limit: (attr.type === 'bag_adres' || attr.type === 'bag_straat_adres') ? 1 : -1
			});
		break;

		case 'bag_openbareruimte':
		case 'bag_openbareruimtes':
		assign(
			config,
			{
				template: 'object-suggest',
				data: {
					display: ( source ) => {
						return source ?
							get(source, 'human_identifier')
							: 'Geen adres geselecteerd';
					},
					objectType: 'bag-street',
					icon: 'map-marker',
					format: ( source ) => {
						
						return {
							bag_id: source.data.id,
							human_identifier: source.label,
							address_data: {
								gps_lat_lon: source.data.gps_lat_lon,
								straat: source.data.streetname,
								woonplaats: source.data.city
							}
						};
					}
				},
				limit: attr.type === 'bag_openbareruimte' ? 1 : -1
			}
		);
		break;

		case 'bankaccount':
		assign(
			config,
			{
				template: 'text'
			}
		);
		break;

		case 'valuta':
		case 'valutaex':
		case 'valutaex21':
		case 'valutaex6':
		case 'valutain':
		case 'valutain21':
		case 'valutain6':
		let btwType = attr.type.replace('valuta', '');

		assign(
			config,
			{
				template: 'valuta',
				data: {
					btwType
				}
			}
		);
		break;

		case 'object':
		let defaultLabels = {
				create: 'Object aanmaken',
				relate: 'Bestaand object relateren',
				update: 'Bestaand object wijzigen',
				delete: 'Bestaand object verwijderen'
			};

		assign(
			config,
			{
				data: {
					capabilities: pick(
						mapValues(
							indexBy('create relate delete update'.split(' ')),
							( value ) => {

								return attr.object_metadata[`${value}_object`] ?
									{
										available: true,
										label: attr.object_metadata[`${value}_object_action_label`]
											|| defaultLabels[value]
									}
									: false;
							}
						),
						identity
					),
					objectId: attr.object_id
				}
			}
		);
		break;

		case 'file':
		assign(config, {
			template: 'case_file',
			limit: -1
		});
		break;

		case 'richtext':
		assign(config, { template: 'rich-text' });
		break;

		case 'googlemaps':
		assign(config, { template: 'map' });
		break;

		case 'geolatlon':
		assign(config, { template: 'map', data: { addressType: 'coordinate' } });
		break;

		case 'calendar':
		case 'calendar_supersaas':
		assign(
			config,
			{
				template: 'calendar',
				data: {
					provider: {
						type: attr.type === 'calendar' ? 'qmatic' : 'supersaas',
						attributeName: attr.magic_string
					}
				}
			}
		);
		break;

		default:
		assign(
			config,
			{
				template: {
					display: ( ) => {
						return angular.element(`<span>Het kenmerktype ${attr.type} wordt momenteel nog niet ondersteund.</span>`);
					},
					control: ( ) => {
						return angular.element(`<span ng-model>Het kenmerktype ${attr.type} wordt momenteel nog niet ondersteund.</span>`);
					}
				}
			}
		);
		break;
	}

	return config;


};
