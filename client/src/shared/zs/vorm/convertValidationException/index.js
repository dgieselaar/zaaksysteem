import mapValues from 'lodash/object/mapValues';
import pick from 'lodash/object/pick';

export default ( instance ) => {

	return {
		validations:
			mapValues(
				pick(instance, ( value ) => value.validity !== 'valid'),
				( value ) => {

					return [
						{
							[value.validity]: value.message
						}
					];
				}
			),
		valid: false
	};

};
