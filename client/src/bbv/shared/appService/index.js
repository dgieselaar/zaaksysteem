import angular from 'angular';
import seamlessImmutable from 'seamless-immutable';
import without from 'lodash/array/without';
import invoke from 'lodash/collection/invoke';
import propCheck from './../../../shared/util/propCheck';

export default
	angular.module('appService', [
	])
		.provider('appService', [ ( ) => {

			let reducers = [],
				state = seamlessImmutable({}),
				listeners = [],
				service = {
					reduce: ( actionName, preferredKey, preferredReducer ) => {

						let key = preferredKey,
							reducer = preferredReducer;

						if (typeof key === 'function') {
							reducer = key;
							key = '*';
						}

						propCheck.throw(propCheck.shape({
							actionName: propCheck.string,
							key: propCheck.string,
							reducer: propCheck.func
						}), { actionName, key, reducer });

						reducers = reducers.concat({ actionName, reducer, key });

						return service;
					},
					setDefaultState: ( defaultState) => {
						state = seamlessImmutable(defaultState);
						return service;
					},
					$get: [ ( ) => {

						return {
							state: ( ) => state,
							dispatch: ( actionName, data ) => {
								state = seamlessImmutable(reducers.filter(reducer => reducer.actionName === actionName)
									.reduce(( prevState, reducer ) => {

										let stateKey = reducer.key,
											part = stateKey !== '*' ? state[stateKey] : state,
											reduced = reducer.reducer(part, data);

										return stateKey !== '*' ? state.merge({ [stateKey] : reduced }) : state.merge(reduced);

									}, state));

								invoke(
									listeners.filter( listener => listener.key === actionName || listener.key === '*'),
									'fn'
								);

							},
							on: ( key, fn ) => {

								let listener = { key, fn };

								listeners = listeners.concat(listener);

								return ( ) => {
									listeners = without(listeners, listener);
								};

							}
						};

					}]
				};

			return service;

		}])
		.name;
