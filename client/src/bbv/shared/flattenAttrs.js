import first from 'lodash/array/first';
import mapValues from 'lodash/object/mapValues';

export default proposal => {
	let flattened = proposal.merge(
		{ instance:
			{ attributes:
				mapValues(proposal.instance.attributes, ( attrValue ) => {
					return first(attrValue);
				})
			}
		},
		{ deep: true }
	);

	return flattened;
};
