import angular from 'angular';
import resourceModule from '../../../shared/api/resource';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import first from 'lodash/array/first';
import get from 'lodash/object/get';
import partition from 'lodash/collection/partition';
import flatten from 'lodash/array/flatten';
import seamlessImmutable from 'seamless-immutable';
import url from 'url';
import appServiceModule from '../appService';
import filter from 'lodash/collection/filter';

export default
	angular.module('Zaaksysteem.bbv.configService', [
		resourceModule,
		composedReducerModule,
		appServiceModule
	])
		.factory('configService', [ '$document', 'resource', '$rootScope', 'composedReducer', 'appService', ( $document, resource, $rootScope, composedReducer, appService ) => {

			let configResource,
				attributeReducer;

			configResource = resource(`/api/v1/app/bbvapp`, {
				scope: $rootScope,
				cache: {
					every: 60 * 1000
				}
			})
				.reduce( ( requestOptions, data ) => {
					return first(data) || { instance: { interface_config: {} } };
				});

			configResource.onUpdate(( data ) => {

				let manifestEl = $document[0].querySelector('link[rel="manifest"]'),
					manifestUrl,
					config = get(data, 'instance.interface_config', {});

				if (!manifestEl) {
					manifestEl = $document[0].createElement('link');
					manifestEl.setAttribute('rel', 'manifest');

					$document.find('head').append(manifestEl);
				}

				manifestUrl = url.format({
					pathname: '/clientutil/proxy/manifest',
					query: {
						short_name: config.title_small,
						name: config.title_normal,
						start_url: config.app_uri,
						icons: [],
						background_color: config.header_bgcolor,
						theme_color: '#FFFFFF',
						display: 'standalone',
						orientation: 'portrait'
					}
				});

				manifestEl.setAttribute('href', manifestUrl);

				// When loading app, check for config setting and toggle view if applicable
				if ( config.defaultview === 'not_per_meeting' ) {
					appService.dispatch('disable_grouped');
				}

			});

			attributeReducer = composedReducer({ scope: $rootScope }, configResource)
				.reduce( config => {

					let split = partition(
							filter(get(config, 'instance.interface_config.attribute_mapping', []), (attribute) => attribute.hasOwnProperty('internal_name') && attribute.internal_name !== null ),
							( attribute ) => attribute.external_name.indexOf('voorstel') === 0
						),
						attributes = {
							voorstel: split[0],
							vergadering: split[1],
							all: flatten(split) || []
						};

					return seamlessImmutable(attributes);
				});

			return {
				getConfig: ( ) => get(configResource.data(), 'instance.interface_config'),
				getAttributes: ( ) => get(attributeReducer.data(), 'all'),
				getProposalAttributes: ( ) => get(attributeReducer.data(), 'voorstel'),
				getMeetingAttributes: ( ) => get(attributeReducer.data(), 'vergadering'),
				getProposalVoteAcceptAttributes: ( ) => get(configResource.data(), 'instance.interface_config.magic_strings_accept'),
				getProposalVoteCommentAttributes: ( ) => get(configResource.data(), 'instance.interface_config.magic_strings_comment'),
				getProposalAttachmentAttributes: ( ) => filter( get(attributeReducer.data(), 'voorstel'), ( attribute ) => attribute.external_name.indexOf('voorstelbijlage') !== -1 ),
				getResource: ( ) => configResource,
				getInterfaceId: ( ) => get(configResource.data(), 'instance.id')
			};

		}])
		.name;
