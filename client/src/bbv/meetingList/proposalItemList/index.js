import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import configServiceModule from '../../shared/configService';
import assign from 'lodash/object/assign';
import shortid from 'shortid';
import get from 'lodash/object/get';
import find from 'lodash/collection/find';
import sortByOrder from 'lodash/collection/sortByOrder';
import rwdServiceModule from '../../../shared/util/rwdService';
import includes from 'lodash/collection/includes';
import appServiceModule from '../../shared/appService';
import seamlessImmutable from 'seamless-immutable';
import angularUiRouterModule from 'angular-ui-router';
import auxiliaryRouteModule from './../../../shared/util/route/auxiliaryRoute';
import isArray from 'lodash/lang/isArray';
import isString from 'lodash/lang/isString';

import './styles.scss';

export default
		angular.module('Zaaksysteem.bbv.meetingListItem.proposalItemList', [
			composedReducerModule,
			configServiceModule,
			rwdServiceModule,
			appServiceModule,
			angularUiRouterModule,
			auxiliaryRouteModule
		])
		.directive('proposalItemList', [ '$sce', '$location', 'dateFilter', '$state', 'configService', 'composedReducer', 'rwdService', 'appService', 'auxiliaryRouteService', ( $sce, $location, dateFilter, $state, configService, composedReducer, rwdService, appService, auxiliaryRouteService ) => {

			return {

				restrict: 'E',
				template,
				scope: {
					proposals: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						attrReducer,
						columnReducer,
						sortReducer,
						proposalReducer,
						sortedProposalReducer,
						sort = {};

					let getDetailState = ( ) => {
						return auxiliaryRouteService.append($state.current, 'proposalDetail');
					};

					ctrl.getViewSize = ( ) => {
						if ( includes( rwdService.getActiveViews(), 'small-and-down') ) {
							return 'small';
						}
						return 'wide';
					};

					ctrl.isGrouped = ( ) => appService.state().grouped;

					attrReducer = composedReducer( { scope: $scope }, configService.getProposalAttributes, ( ) => appService.state().grouped)
						.reduce( ( proposalAttrs, grouped ) => {

							let attributes = proposalAttrs || [];

							let noteCol = {
											external_name: 'voorstelnotitie',
											internal_name: {
												searchable_object_id: 'proposalNote',
												searchable_object_label: 'Notitie'
											},
											checked: 1
										};

							return grouped ?
								[
									{
										external_name: 'voorstelnummer',
										internal_name: {
											searchable_object_id: '$index',
											searchable_object_label: ''
										},
										checked: 1
									}
								].concat(attributes)
								.concat(noteCol)
								: attributes.concat(noteCol);

						});

					sortReducer = composedReducer({ scope: $scope }, ( ) => sort, attrReducer)
						.reduce( ( userSort, attributes ) => {

							let finalSort,
								sortKey;

							if (userSort.key) {
								finalSort = userSort;
							} else {

								let firstCol = find(attributes, ( attr ) => attr.external_name.indexOf('voorstel') === 0),
									key = firstCol.internal_name.searchable_object_id;

								finalSort = seamlessImmutable({
									key,
									reversed: false
								});

							}

							switch ( finalSort.key ) {
								case 'zaaknummer':
								sortKey = `instance.number`;
								break;
								case 'zaaktype':
								sortKey = `instance.casetype.instance.name`;
								break;
								default:
								sortKey = `instance.attributes.${finalSort.key}`;
							}

							finalSort = finalSort.merge({
								resolve: sortKey
							});

							return finalSort;

						});
						
					columnReducer = composedReducer({ scope: $scope }, attrReducer, sortReducer)
						.reduce( ( attributes, sortOptions ) => {

							return seamlessImmutable(attributes)
								.filter( n => {
									return n.external_name.indexOf('voorstel') === 0 && n.checked === 1;
								})
								.map( attr => {

									let isSortedOn = sortOptions.key === get(attr, 'internal_name.searchable_object_id'),
										isSortReversed = isSortedOn && sortOptions.reversed,
										colName = attr.external_name,
										templateValue = attr.internal_name.searchable_object_label_public || attr.internal_name.searchable_object_label,
										wideCol = false,
										smallCol = false;

										if (colName === 'voorstelnummer') {
											templateValue = '';
											smallCol = true;
										}

										if (colName === 'voorstelnotitie') {
											templateValue = '';
											smallCol = true;
										}

										if (colName === 'voorstelzaaknummer') {
											templateValue = '#';
											smallCol = true;
										}

										if (colName === 'voorstelonderwerp') {
											wideCol = true;
										}

									return attr.merge({
										icon: isSortReversed ?
											'chevron-up'
											: (isSortedOn ? 'chevron-down' : ''),
										classes: {
											'sorted-on': isSortedOn,
											'sort-reversed': isSortReversed
										},
										colClass: {
											'wide': wideCol,
											// 'medium': mediumCol,
											'small': smallCol
										},
										templateValue
									});

								});
						});
					
					ctrl.getConfigColumns = columnReducer.data;

					ctrl.sortByColumn = ( column ) => {

						let sortOptions = sortReducer.data(),
							key = sortOptions.key,
							reversed = sortOptions.reversed,
							colId = get(column, 'internal_name.searchable_object_id');

						if (sortOptions.key === colId) {
							reversed = !reversed;
						} else {
							reversed = false;
							key = colId;
						}

						sort = seamlessImmutable({ key, reversed });
					};

					ctrl.handleProposalClick = ( proposal ) => {
						$state.go(getDetailState(), { zaakID: proposal.casenumber });
					};

					sortedProposalReducer = composedReducer( { scope: $scope }, ctrl.proposals, ctrl.getConfigColumns, sortReducer)
						.reduce( ( proposals, columns, sortOptions ) => {

							let sortedProposals = proposals;

							if (sortOptions && sortOptions.key) {
								
								sortedProposals = seamlessImmutable(sortByOrder(sortedProposals,
									( proposal ) => get(proposal, sortOptions.resolve),
									sortOptions.reversed ? 'desc' : 'asc'
								));
							}

							return sortedProposals;

						});

					// For bigger viewports
					proposalReducer = composedReducer({ scope: $scope }, sortedProposalReducer, ctrl.getConfigColumns)
						
						.reduce( ( sortedProposals, columns ) => {

							let mutableCols = columns.asMutable(),
								formattedProposals;

							formattedProposals =
								sortedProposals.asMutable().map( ( proposal ) => {

									return assign(
										{
											$id: shortid(),
											id: proposal.reference,
											casenumber: proposal.instance.number,
											casetype: proposal.instance.casetype.instance.name
										},
										{
											values: mutableCols.map( ( column ) => {

												let columnName = column.internal_name.searchable_object_id,
													value = proposal.instance.attributes[columnName],
													tpl;

												if ( isArray(value) ) {

													if (value.length > 1) {
														tpl = `<ul>`;

														value.map( ( el ) => {
															tpl += `<li>${el}</li>`;
														});

														tpl += `</ul>`;
													} else {
														tpl = `${value.join()}`;
													}

												} else if ( isString(value) && value.split(' ').length > 20 ) {

													let resultArray = value.split(' ');
													
													if (resultArray.length > 20) {
														resultArray = resultArray.slice(0, 20);
														tpl = `${resultArray.join(' ')}...`;
													}

												} else if (columnName === '$index' || isString(value) ) {
													tpl = value;
												} else {
													tpl = '-';
												}


												if (column.attribute_type === 'file') {
													tpl = `<a href="/download/${value.id}">Download</a>`;
												}

												if (column.external_name === 'voorstelnummer') {
													tpl = `<span class="proposal__number">${tpl}</span>`;
												}

												if (column.external_name === 'voorstelnotitie') {
													if ( proposal.notes ) {
														tpl = `<i class="mdi mdi-note-plus"></i>`;
													} else {
														tpl = '';
													}
												}

												if (columnName === 'zaaknummer') {
													tpl = String(proposal.instance.number || '');
												}

												if (column.external_name.includes('datum') ) {
													tpl = dateFilter( value, 'dd MMM yyyy');
												}

												if (column.external_name === 'voorstelzaaktype') {
													tpl = String(proposal.instance.casetype.instance.name || '-');
												}

												if ( column.internal_name.searchable_object_id === 'resultaat') {
													tpl = String(proposal.instance.result || '-');
												}

												return {
													id: shortid(),
													name: columnName,
													template: $sce.trustAsHtml(tpl),
													value
												};

											})
										}
									);
								});

							return formattedProposals;
						});

					ctrl.getProposals = proposalReducer.data;

					// For smaller viewports
					ctrl.getSmallProposals = composedReducer({ scope: $scope }, sortedProposalReducer, ctrl.getConfigColumns)
						.reduce( ( proposals, columns ) => {

							return proposals.map(( proposal, index ) => {

								let descriptionCol = find(columns, ( col ) => col.external_name === 'voorstelonderwerp' ),
									value = proposal.instance.attributes[descriptionCol.internal_name.searchable_object_id] || '-';

								if (value.split(' ').length > 20) {
									
									let resultArray = value.split(' ');
									
									if (resultArray.length > 20) {
										resultArray = resultArray.slice(0, 20);

										value = `${resultArray.join(' ')}...`;

									}
								}

								return {
									$id: shortid(),
									id: proposal.reference,
									casenumber: proposal.instance.number,
									subject: value,
									agendapunt_nummer: index + 1,
									notes: proposal.notes || false,
									link: $state.href(getDetailState(), { zaakID:  proposal.instance.number })
								};

							});

						})
						.data;

				}],
				controllerAs: 'proposalItemList'

			};
		}
		])
		.name;
