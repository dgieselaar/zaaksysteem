import angular from 'angular';
import template from './template.html';
import resourceModule from '../../../shared/api/resource';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import proposalItemListModule from '../proposalItemList';
import rwdServiceModule from '../../../shared/util/rwdService';
import includes from 'lodash/collection/includes';
import configServiceModule from '../../shared/configService';
import first from 'lodash/array/first';
import isArray from 'lodash/lang/isArray';

import './styles.scss';

export default
		angular.module('Zaaksysteem.bbv.meetingListItem', [
			resourceModule,
			composedReducerModule,
			proposalItemListModule,
			rwdServiceModule,
			configServiceModule
		])
		.directive('meetingListItem', [ 'resource', 'rwdService', 'dateFilter', 'configService', 'composedReducer', ( resource, rwdService, dateFilter, configService, composedReducer ) => {

			return {

				restrict: 'E',
				template,
				scope: {
					onToggleExpand: '&',
					isExpanded: '&',
					isGrouped: '&',
					meeting: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						styleReducer;

					ctrl.getDate = ( ) => {
						return dateFilter( ctrl.meeting().date, 'dd MMM yyyy');
					};

					ctrl.getTime = ( ) => {
						return ctrl.meeting().time;
					};

					ctrl.getTitle = ( ) => {
						return isArray(ctrl.meeting().label) ? first(ctrl.meeting().label) : ctrl.meeting().label || ctrl.meeting().instance.casetype.instance.name;
					};

					ctrl.getLocation = ( ) => {
						if (isArray(ctrl.meeting().location )) {
							return first(ctrl.meeting().location);
						}
						return ctrl.meeting().location;
					};

					ctrl.getChairman = ( ) => {
						return ctrl.meeting().chairman;
					};

					ctrl.getProposals = ( ) => {
						return !ctrl.isExpanded() ? [ ] : ctrl.meeting().children;
					};

					ctrl.toggleExpand = ( ) => {
						ctrl.onToggleExpand();
					};

					styleReducer = composedReducer( { scope: $scope }, configService.getConfig)
						.reduce( config => {

							return { 'color': config.accent_color };

						});

					ctrl.getTitleStyle = ( ) => styleReducer.data();

					ctrl.getViewSize = ( ) => {
						if ( includes( rwdService.getActiveViews(), 'small-and-down') ) {
							return 'small';
						}
						return 'wide';
					};

				}],
				controllerAs: 'meetingListItem'

			};
		}
		])
		.name;
