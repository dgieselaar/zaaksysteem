import angular from 'angular';
import 'angular-mocks';
import meetingListItemModule from '.';
import configData from '../../shared/mock/config.json';
import meetingData from '../../shared/mock/meeting.json';
import cacherModule from '../../../shared/api/cacher';
import appServiceModule from '../../shared/appService';

describe('meetingListItem', ( ) => {

	let meetingListItem,
		$httpBackend,
		apiCacher,
		dateFilter,
		el;


	beforeEach(angular.mock.module(meetingListItemModule, cacherModule, appServiceModule, [ 'appServiceProvider', ( appServiceProvider ) => {

		appServiceProvider.setDefaultState({ expanded: true, grouped: true })
			.reduce('toggle_grouped', 'grouped', ( isGrouped ) => !isGrouped)
			.reduce('toggle_expand', 'expanded', ( isExpanded ) => !isExpanded);

	}]));

	beforeEach(angular.mock.inject([ '$httpBackend', 'apiCacher', ( ...rest ) => {

		[ $httpBackend, apiCacher ] = rest;

		$httpBackend.expectGET(/\/api\/v1\/app\/bbvapp/)
			.respond([ configData ]);

		apiCacher.clear();

	}]));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$document', 'dateFilter', 'configService', 'resource', ( ...rest ) => {

		let $rootScope, $compile, $document;

		[ $rootScope, $compile, $document, dateFilter ] = rest;

		el = angular.element(`<meeting-list-item
			data-meeting=meeting
			is-grouped="isGrouped"
		/>`);

		$document.find('body').append(el);

		let scope = $rootScope.$new();

		scope.isGrouped = false;

		scope.meeting = meetingData;

		$compile(el)(scope);

		meetingListItem = el.controller('meetingListItem');

		$httpBackend.flush();

	}]));


	it('should have a controller', ( ) => {

		expect(meetingListItem).toBeDefined();

	});


	it('should have a function that returns the date of a meeting', ( ) => {

		expect( meetingListItem.getDate ).toBeDefined();

	});

	it('should return the date of a meeting in the form dd MMMM yyyy', ( ) => {

		expect( meetingListItem.getDate() ).toEqual(dateFilter(meetingData.date, 'dd MMM yyyy'));

	});

	it('should have a function that returns the time of a meeting', ( ) => {

		expect( meetingListItem.getTime ).toBeDefined();

	});


	it('should return the time of a meeting in the form hh:mm', ( ) => {

		expect( meetingListItem.getTime() ).toEqual('19:00');

	});

	it('should have a function that returns the title of the meeting', ( ) => {

		expect( meetingListItem.getTitle ).toBeDefined();

	});


	it('should return a title if it is present in the data', ( ) => {

		expect( meetingListItem.getTitle() ).toEqual('Testvergadering');

	});

	it('should have a function that returns the location of the meeting', ( ) => {

		expect( meetingListItem.getLocation ).toBeDefined();

	});


	it('should return a location if it is present in the data', ( ) => {

		expect( meetingListItem.getLocation() ).toEqual('Testlocatie');

	});

	it('should have a function that returns the chairman of the meeting', ( ) => {

		expect( meetingListItem.getChairman ).toBeDefined();

	});

	it('should return a chairman if it is present in the data', ( ) => {

		expect( meetingListItem.getChairman() ).toEqual('Testvoorzitter');

	});

	it('should have a function that returns the proposals of the meeting', ( ) => {

		expect( meetingListItem.getProposals ).toBeDefined();

	});

	it('should return proposals if they are present in the data', ( ) => {

		expect( meetingListItem.getProposals().length ).not.toBeGreaterThan(0);

	});

	it('should have a function that toggles if proposals for that meeting are expanded or not', ( ) => {

		expect( meetingListItem.toggleExpand ).toBeDefined();

	});

	it('should have a function that return if meetings are grouped or not', ( ) => {

		expect( meetingListItem.isGrouped ).toBeDefined();

		expect( typeof meetingListItem.isGrouped() ).toEqual('boolean');

	});

	it('should have a function that returns the viewport size', ( ) => {

		expect( meetingListItem.getViewSize ).toBeDefined();

	});

	it('should have a function that returns the style of the meeting title', ( ) => {

		expect( meetingListItem.getTitleStyle ).toBeDefined();

	});

	it('should return style object of the form { `color` : `#ffffff`', ( ) => {

		expect( meetingListItem.getTitleStyle() ).toEqual(jasmine.objectContaining({
			'color': configData.instance.interface_config.accent_color
		}));

	});

});
