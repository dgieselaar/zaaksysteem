import angular from 'angular';
import 'angular-mocks';
import meetingListViewModule from '.';
import configData from '../../shared/mock/config.json';
import meetingData from '../../shared/mock/meeting.json';
import proposalData from '../../shared/mock/proposal.json';
import cacherModule from '../../../shared/api/cacher';
import composedReducerModule from '../../../shared/api/resource/composedReducer';

describe('meetingListView', ( ) => {

	let meetingListView,
		$httpBackend,
		apiCacher,
		el;

	beforeEach(angular.mock.module(meetingListViewModule, cacherModule, composedReducerModule, [ ( ) => {}]));

	beforeEach(angular.mock.inject([ '$httpBackend', 'apiCacher', ( ...rest ) => {

		[ $httpBackend, apiCacher ] = rest;

		$httpBackend.expectGET(/.*?/)
			.respond([ configData ]);

		apiCacher.clear();

	}]));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$document', 'composedReducer', ( ...rest ) => {

		let $rootScope, $compile, $document, composedReducer;

		[ $rootScope, $compile, $document, composedReducer ] = rest;

		el = angular.element(`<meeting-list-view
			proposal-resource="proposalResource"
			meeting-resource="meetingResource"
		/>`);

		$document.find('body').append(el);

		let scope = $rootScope.$new();

		scope.proposalResource = composedReducer({ scope }, proposalData);

		scope.meetingResource = meetingData;

		$compile(el)(scope);

		meetingListView = el.controller('meetingListView');

		$httpBackend.flush();

	}]));


	it('should have a controller', ( ) => {

		expect(meetingListView).toBeDefined();

	});

	it('should have a function that returns meetings', ( ) => {

		expect( meetingListView.getMeetings ).toBeDefined();

	});

	it('should return meetings', ( ) => {

		expect( meetingListView.getMeetings().length ).not.toEqual(0);

	});

	it('should have a function that handles the toggle', ( ) => {

		expect( meetingListView.handleToggle ).toBeDefined();

	});

	it('should have a function that handles the toggle', ( ) => {

		expect( meetingListView.isExpanded ).toBeDefined();

	});

	it('should have a function that handles the toggle', ( ) => {

		expect( meetingListView.isLoading ).toBeDefined();

	});


});
