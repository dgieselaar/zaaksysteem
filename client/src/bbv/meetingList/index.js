import angular from 'angular';
import template from './index.html';
import resourceModule from '../../shared/api/resource';
import meetingListViewModule from './meetingListView';
import meetingListItemModule from './meetingListItem';
import configServiceModule from './../shared/configService';
import snackbarServiceModule from './../../shared/ui/zsSnackbar/snackbarService';
import seamlessImmutable from 'seamless-immutable';
import uniq from 'lodash/array/uniq';
import flattenDeep from 'lodash/array/flattenDeep';
import pluck from 'lodash/collection/pluck';
import flattenAttrs from '../shared/flattenAttrs';
import first from 'lodash/array/first';

export default {

	moduleName:
		angular.module('Zaaksysteem.bbv.meetingList', [
			resourceModule,
			meetingListItemModule,
			meetingListViewModule,
			configServiceModule,
			snackbarServiceModule
		])
		.controller('meetingListController', [ '$stateParams', '$scope', 'proposalResource', 'meetingResource', ( $stateParams, $scope, proposalResource, meetingResource ) => {

			$scope.proposalResource = proposalResource;
			$scope.meetingResource = meetingResource;

			$scope.meetingType = $stateParams.meetingType;

			$scope.$on('$destroy', ( ) => {
				
				[ proposalResource, meetingResource ].forEach( resource => {
					resource.destroy();
				});
			});

		}])
		.name,
	config: [
		{
			route: {
				url: '/vergaderingen/:meetingType',
				template,
				resolve: {
					proposalResource: [ '$rootScope', '$q', 'config', 'resource', 'configService', 'snackbarService', 'meetingResource', ( $rootScope, $q, config, resource, configService, snackbarService, meetingResource ) => {

						let proposalResource = resource(( ) => {

							let ids =
								uniq(
									flattenDeep(meetingResource.data().map(meeting => pluck(meeting.instance.relations.instance.rows, 'reference')))
								);
								
							if (!ids.length) {
								return null;
							}

							return {
								url: '/api/v1/case',
								params: {
									zql: `SELECT {} FROM case WHERE (case.casetype.id IN (${configService.getConfig().casetype_voorstel_item.join()}))`
								}
							};

						}, { scope: $rootScope })

							// grab first value of array for every attribute to reduce
							// boilerplate
							
							.reduce( ( requestOptions, data ) => {
								return (data || seamlessImmutable([])).map(flattenAttrs);
							});


						return proposalResource.asPromise()
							.then(( ) => proposalResource)
							.catch( err => {
								snackbarService.error('Er ging iets fout bij het ophalen van de voorstellen. Neem contact op met uw beheerder voor meer informatie.');
								return $q.reject(err);
							});

					}],
					meetingResource: [ '$rootScope', '$q', '$stateParams', 'config', 'resource', 'configService', 'snackbarService', ( $rootScope, $q, $stateParams, config, resource, configService, snackbarService ) => {

						let meetingResource = resource( ( ) => {

							let statuses =
								$stateParams.meetingType === 'open' ?
									[ 'open', 'new' ]
									: [ 'resolved' ],
								zql;
								
							zql = `SELECT {} FROM case WHERE (case.casetype.id = ${first(configService.getConfig().casetype_meeting)})  AND case.status IN ("${statuses.join('","')}")`;

							return {
								url: '/api/v1/case',
								params: {
									zql
								}
							};

						}, { scope: $rootScope })
							.reduce( ( requestOptions, data ) => {
								return (data || seamlessImmutable([])).map(flattenAttrs);
							});

						return meetingResource.asPromise()
							.then( ( ) => meetingResource)
							.catch( err => {
								snackbarService.error('Er ging iets fout bij het ophalen van de vergaderingen. Neem contact op met uw beheerder voor meer informatie.');
								return $q.reject(err);
							});

					}]
				},
				controller: 'meetingListController'
			},
			state: 'meetingList'
		}
	]
};
