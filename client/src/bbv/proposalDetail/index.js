import angular from 'angular';
import resourceModule from '../../shared/api/resource';
import proposalDetailViewModule from './proposalDetailView';
import first from 'lodash/array/first';
import snackbarServiceModule from './../../shared/ui/zsSnackbar/snackbarService';
import auxiliaryRouteModule from './../../shared/util/route/auxiliaryRoute';
import onRouteActivateModule from './../../shared/util/route/onRouteActivate';
import zsModalModule from './../../shared/ui/zsModal';
import viewTitleModule from './../../shared/util/route/viewTitle';
import template from './index.html';
import seamlessImmutable from 'seamless-immutable';
import flattenAttrs from '../shared/flattenAttrs';
import configServiceModule from '../shared/configService';
import find from 'lodash/collection/find';

import './styles.scss';

export default {

	moduleName:
		angular.module('proposalDetail', [
			resourceModule,
			proposalDetailViewModule,
			snackbarServiceModule,
			auxiliaryRouteModule,
			onRouteActivateModule,
			zsModalModule,
			viewTitleModule,
			configServiceModule
		])
		.name,
	config: [
		{
			route: {
				url: '/voorstel/:zaakID/',
				title: [ 'proposal', 'configService', ( proposal, configService ) => {

					let findDescription = ( ) => find(configService.getProposalAttributes(), ( attribute ) => attribute.external_name === 'voorstelonderwerp');

					return `${proposal.data().instance.number}: ${proposal.data().instance.attributes[findDescription().internal_name.searchable_object_id] || proposal.data().instance.casetype.name}`;

				}],
				resolve: {
					proposal: [ '$rootScope', '$stateParams', '$q', 'resource', 'snackbarService', ( $rootScope, $stateParams, $q, resource, snackbarService ) => {

						let proposalResource = resource(
							{ url: '/api/v1/case', params: { zql: `SELECT {} FROM case WHERE case.number = ${$stateParams.zaakID}` } },
							{ scope: $rootScope }
						)
							.reduce( ( requestOptions, data ) => {

								return first( (data || seamlessImmutable([])).map(flattenAttrs) );

							});

						return proposalResource
							.asPromise()
							.then( ( ) => proposalResource)
							.catch( ( err ) => {

								snackbarService.error('Het voorstel kon niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);
							});
					}],
					documents: [ '$rootScope', '$stateParams', '$q', 'resource', 'snackbarService', 'proposal', ( $rootScope, $stateParams, $q, resource, snackbarService, proposal ) => {

						return resource(
							{ url: `/api/v1/case/${proposal.data().reference}/document` },
							{ scope: $rootScope }
						)
							.asPromise()
							.then( ( data ) => data )
							.catch( ( err ) => {

								snackbarService.error('De voorsteldocumenten konden niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);

							});

					}],
					casetype: [ '$rootScope', '$q', 'resource', 'snackbarService', 'configService', ( $rootScope, $q, resource, snackbarService, configService ) => {

						return resource(
								configService.getConfig().access === 'rw' ?
									`/api/v1/casetype/?zql=SELECT {} FROM casetype WHERE object.uuid IN ("${configService.getConfig().casetype_voorstel_item_uuids.join('","')}")`
									: null
							,
							{ scope: $rootScope }
						)
							.asPromise()
							.catch( ( err ) => {

								snackbarService.error('De zaaktypes konden niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);

							});

					}]
				},
				auxiliary: true,
				onActivate: [ '$timeout', '$rootScope', '$window', '$document', '$compile', 'viewTitle', '$stateParams', 'zsModal', 'proposal', 'documents', 'casetype', ( $timeout, $rootScope, $window, $document, $compile, viewTitle, $stateParams, zsModal, proposal, documents, casetype ) => {

					let scrollEl = angular.element($document[0].querySelector('.body-scroll-container'));

					scrollEl.css({ overflow: 'hidden' });

					let openModal = ( ) => {

						let modal,
							unregister,
							scope = $rootScope.$new(true);

						scope.proposal = proposal;
						scope.documents = documents;
						scope.casetype = casetype;

						modal = zsModal({
							el: $compile(angular.element(template))(scope),
							title: viewTitle.get(),
							classes: 'detail-modal'
						});

						modal.open();

						modal.onClose( ( ) => {
							$window.history.back();
							return true;
						});

						unregister = $rootScope.$on('$stateChangeStart', ( ) => {

							$window.requestAnimationFrame( ( ) => {

								$rootScope.$evalAsync(( ) => {
									modal.close()
										.then(( ) => {

											scope.$destroy();

											scrollEl.css({
												overflow: '',
												height: ''
											});

										});

									unregister();

								});

							});
							
						});

					};


					$window.requestAnimationFrame( ( ) => {
						$rootScope.$evalAsync(openModal);
					});

				}]

			},
			state: 'proposalDetail'
		}
	]
};
