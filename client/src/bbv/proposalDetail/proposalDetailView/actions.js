import angular from 'angular';
import mutationServiceModule from './../../../shared/api/resource/mutationService';
import propCheck from '../../../shared/util/propCheck';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import configServiceModule from '../../shared/configService';

export default
	angular.module('Zaaksysteem.bbv.voteActions', [
		mutationServiceModule,
		snackbarServiceModule,
		configServiceModule
	])
	.run([ 'snackbarService', 'mutationService', 'configService', ( snackbarService, mutationService, configService ) => {

		mutationService.register( {
			type: 'VOTE_FOR_PROPOSAL',
			request: ( mutationData ) => {

				propCheck.throw(
					propCheck.shape({
						proposalId: propCheck.string,
						voteData: propCheck.object
					}),
					mutationData
				);

				return {
					url: `/api/v1/app/bbvapp/case/${mutationData.proposalId}/update_attributes`,
					data: mutationData.voteData,
					headers: {
						'API-Interface-Id': configService.getInterfaceId()
					}
				};
			},
			reduce: ( data, mutationData ) => {

				return data.map(
					proposal => {

						if (proposal.reference === mutationData.proposalId) {

							return proposal.merge({
								instance: {
									attributes: mutationData.voteData.values
								}
							}, { deep: true });
						}

						return proposal;

					}
				);

			},
			error: ( /*data*/ ) => {

				snackbarService.error('Er ging iets mis bij het opslaan van uw stem. Neem contact op met uw beheerder voor meer informatie.');

			}

		} );

	}])
	.name;
