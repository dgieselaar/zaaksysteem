export default ( parent, query, filter ) => {

	parent
		.$('.suggestion-input')
		.click()
		.sendKeys(query);

	return parent.all(by.css('.suggestion-list-item'))
		.then(( elements ) => {

			return protractor.promise.all(
				elements.map(
					el => {

						return protractor.promise.all([
							el.getAttribute('data-id'),
							el.$('.suggestion-text').getText()
						])
							.then( ( [ id, label ] ) => {

								return { id, label };
							});

					}
				)
			)
				.then(( options ) => {

					let toSelect = filter(options);

					return parent
						.$(`.suggestion-list-item[data-id="${toSelect.id}"]`)
						.click();

				});

		});
		
};
