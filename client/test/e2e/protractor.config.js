require('babel-core/register');

var login = require('./actions/auth/login'), //eslint-disable-line
	baseUrl = 'https://vagrant.zaaksysteem.nl';

exports.config = {
	allScriptsTimeout: 11000,
	specs: [
		'tests/**/*.js'
	],
	capabilities: {
		'browserName': 'chrome'
	},
	chromeOnly: true,
	framework: 'jasmine2',
	directConnect: true,
	baseUrl: baseUrl, //eslint-disable-line
	seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
	jasmineNodeOpts: {
		defaultTimeoutInterval: 30000
	},
	onPrepare: function ( ) { //eslint-disable-line

		browser.driver.get(baseUrl + '/auth/login'); //eslint-disable-line

		login();
		
	}
};
