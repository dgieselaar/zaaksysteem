import searchFor from './../../actions/spotEnlighter/searchFor';
import getResultElements from './../../actions/spotEnlighter/getResultElements';
import closeSpotEnlighter from './../../actions/spotEnlighter/closeSpotEnlighter';

describe('when searching for cases', ( ) => {

	it(`and query is 'zaak'`, ( ) => {

		searchFor('melding');

		expect(getResultElements().count()).toBeGreaterThan(0);

	});

	afterEach(( ) => {

		closeSpotEnlighter();
		
	});

});
