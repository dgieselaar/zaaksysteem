import attemptLogin from './../../actions/auth/attemptLogin';
import login from './../../actions/auth/login';
import logout from './../../actions/auth/logout';

describe('when logging in', ( ) => {

	beforeAll(( ) => {

		logout();

	});

	it('should have a form', ( ) => {

		expect(
			element(by.css('#loginwrap form')).isPresent()
		).toBe(true);

	});

	it('should have a username input field', ( ) => {

		expect(
			element(by.css('input[type=text]#id_username')).isPresent()
		).toBe(true);

	});

	it('should have a password input field', ( ) => {

		expect(
			element(by.css('input[type=password]#id_password')).isPresent()
		).toBe(true);

	});

	it('should have a submit button', ( ) => {

		expect(
			element(by.css('#loginwrap input[type=submit]')).isPresent()
		).toBe(true);

	});

	describe('when logging in with valid credentials', ( ) => {

		it('should redirect to the application', ( ) => {

			attemptLogin('admin', 'admin');

			expect(browser.getCurrentUrl()).toMatch(/intern/);

			logout();

		});

	});

	describe('when logging in with invalid credentials', ( ) => {

		it('should redirect to the application', ( ) => {

			attemptLogin('admin', 'foo');

			expect(browser.getCurrentUrl()).not.toMatch(/intern/);

		});

	});

	afterAll(( ) => {

		login();

	});

});
