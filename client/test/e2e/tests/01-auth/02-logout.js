import login from './../../actions/auth/login';
import logout from './../../actions/auth/logout';

describe('when logged in', ( ) => {

	describe('and user logs out', ( ) => {

		beforeEach(( ) => {

			logout();

		});

		it('should redirect to the login page', ( ) => {

			expect(browser.getCurrentUrl()).toMatch(/auth\/page/);

		});

		afterEach(( ) => {

			login();

		});

	});

});
