import 'phantomjs-polyfill';
import angular from 'angular';
import 'angular-mocks';
import remove from 'lodash/array/remove';
import countBy from 'lodash/collection/countBy';
import map from 'lodash/collection/map';
import appUnloadModule from './../../src/shared/util/appUnload';
import apiCacherModule from './../../src/shared/api/cacher';
import mutationServiceModule from './../../src/shared/api/resource/mutationService';

let testsContext = require.context('../../src/', true, /(^([.\/]*)([a-z0-9\-]+)\/index\.js$)|(test\.js$)/);

testsContext.keys()
	.forEach(testsContext);

// the window object is reused and can sometimes cause hard-to-debug
// test failures because of sticky window listeners. here's a horrible
// hacky way to prevent it.

let listeners = [];

beforeEach(angular.mock.module(appUnloadModule, apiCacherModule, mutationServiceModule));

// injecting $window here gives an error, not sure why
beforeAll(( ) => {
	
	let $window = angular.injector([ 'ng' ]).get('$window'),
		fn = $window.addEventListener;

	$window.addEventListener = ( ...rest ) => {
		listeners.push(rest);
		return fn.apply($window, rest);
	};

	$window.removeEventListener = ( ...rest ) => {
		remove(listeners, rest);
		return fn.apply($window, rest);
	};


});

afterEach(angular.mock.inject([ '$httpBackend', '$window', 'appUnload', 'apiCacher', 'mutationService', ( $httpBackend, $window, appUnload, apiCacher, mutationService ) => {

	appUnload.triggerUnload();

	$httpBackend.verifyNoOutstandingExpectation();
	$httpBackend.verifyNoOutstandingRequest();

	apiCacher.clear();

	mutationService.clear();

	if (listeners.length) {

		let types =
			map(
				countBy(listeners, ( val ) => val[0]),
				( value, key ) => `${key} (${value})`
			).join(', ');

		while (listeners.length) {
			
			let listener = listeners.shift();

			$window.removeEventListener(...listener);
		}

		throw new Error(`Listeners found on $window. Please make sure to remove all listeners to prevent test failures.
		Found: ${types}`);

	}

}]));
