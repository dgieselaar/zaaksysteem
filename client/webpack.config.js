var webpack = require('webpack'),
	path = require('path'),
	ExtractTextPlugin = require('extract-text-webpack-plugin'),
	ServiceWorkerCachePlugin = require('./ServiceWorkerCachePlugin'),
	HtmlWebpackPlugin = require('html-webpack-plugin'),
	HtmlWebpackAlwaysWritePlugin = require('./HtmlWebpackAlwaysWritePlugin'),
	ForceCaseSensitivityPlugin = require('force-case-sensitivity-webpack-plugin'),
	NamedModulesPlugin = require('webpack/lib/NamedModulesPlugin'),
	merge = require('lodash/object/merge'),
	apps = require('./apps'),
	ip = require('ip'),
	find = require('lodash/collection/find');

var env = process.env.NODE_ENV || 'development',
	root = path.join('..', 'root'),
	publicPath = '/assets/',
	devServerHost = ip.address(),
	devServerPort = '9090',
	isDevServer = !!find(process.argv, arg => arg.indexOf('webpack-dev-server') !== -1),
	devServerPrefix = isDevServer ? `https://${devServerHost}:${devServerPort}` : '',
	exports;

exports = {
	devtool: env === 'production' ? 'source-map' : 'eval',
	entry: [],
	output: {
		path: path.join(__dirname, '/../root/assets/'),
		publicPath: devServerPrefix + publicPath,
		filename: '[name].[hash].js'
	},
	module: {
		loaders: [
			{
				test: /.*\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					optional: [ 'runtime' ]
				}
			},
			{
				test: /.*\.html$/,
				// removing optional tags breaks HtmlWebpackPlugin script injection
				loader: 'html?removeOptionalTags=false&removeDefaultAttributes=false',
				exclude: /node_modules/
			},
			{
				test: /.*\.json$/,
				loader: 'json-loader',
				exclude: /node_modules/
			},
			{
				test: /\.scss$/,
				loader: env === 'production' ? ExtractTextPlugin.extract('style-loader', 'css-loader!autoprefixer-loader!sass-loader') : 'style-loader!css-loader!autoprefixer-loader!sass-loader',
				exclude: /node_modules/
			},
			{
				test: /\.css$/,
				loader: env === 'production' ? ExtractTextPlugin.extract('style-loader', 'css-loader') : 'style-loader!css-loader',
				exclude: /node_modules/
			},
			{
				test: /\.(woff(2)?|eot|ttf|svg)(\?v=\d+\.\d+\.\d+)?$/,
				loader: 'url-loader?name=[name].[ext]&limit=10000'
			}
		],
		noParse: [
			/[\/\\]node_modules[\/\\]quill/,
			/[\/\\]node_modules[\/\\]proj4/,
			/[\/\\]node_modules[\/\\]openlayers/,
			/[\/\\]node_modules[\/\\](angular-mocks|angular-ui-router)/,
			/[\/\\]node_modules[\/\\]openlayers/,
			/[\/\\]node_modules[\/\\]api-check/,
			/[\/\\]node_modules[\/\\]email-regex/,
			/[\/\\]node_modules[\/\\]api-check/,
			/[\/\\]node_modules[\/\\]define/,
			/[\/\\]node_modules[\/\\]fuzzy/,
			/[\/\\]node_modules[\/\\]iban/,
			/[\/\\]node_modules[\/\\]is-promise/,
			/[\/\\]node_modules[\/\\]match-media/,
			/[\/\\]node_modules[\/\\]soma-events/,
			/[\/\\]node_modules[\/\\]word-ngrams/
		]
	},
	resolve: {
		alias: {
			quill: 'quill/dist/quill.js'
		}
	},
	devServer: {
		https: true,
		port: devServerPort,
		host: devServerHost
	}
};

module.exports = apps.map(
	app => {

		var plugins = [
			new webpack.DefinePlugin({
				ENV: env,
				DEV: env === 'development',
				PROD: env === 'production',
				'process.env': {
					NODE_ENV: JSON.stringify(env)
				}
			}),
			new NamedModulesPlugin(),
			new HtmlWebpackPlugin({
				filename: path.join(app, 'index.html'),
				template: `${path.join('.', 'src', app, 'index.html')}`,
				inject: 'body',
				chunks: [ app ],
				minify: false
			}),
			new HtmlWebpackAlwaysWritePlugin( { stripPrefix: devServerPrefix, root, entries: [ app ] }),
			new ForceCaseSensitivityPlugin()
		];

		if (env === 'production') {
			plugins = plugins.concat(
				new ServiceWorkerCachePlugin({
					root,
					publicPath,
					entries: [ app ],
					globs: [ path.join(app, 'index.html') ],
					options: {
						navigateFallback: publicPath + app + '/index.html', //eslint-disable-line
						navigateFallbackWhitelist: [ new RegExp('^\/' + app ) ] //eslint-disable-line
					}
				}),
				new ExtractTextPlugin('[name].[hash].css', {
					allChunks: false
				}),
				new webpack.optimize.UglifyJsPlugin({
					comments: false
				}),
				new webpack.optimize.DedupePlugin(),
				new webpack.optimize.OccurrenceOrderPlugin(true)
			);
		}
		
		return merge({}, exports, {
			plugins,
			entry: {
				[app]: [ `./src/${app}/index.js` ]
			}
		});
	}
);
