/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.core')
		.factory('safeApply', [ function ( ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply');
			
			return safeApply;
			
		}]);
	
})();
