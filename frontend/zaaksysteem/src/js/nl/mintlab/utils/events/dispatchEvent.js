/*global define,document*/
(function ( ) {
	
	window.define('nl.mintlab.utils.events.dispatchEvent', function ( ) {
		var doc = document;
		
		if(doc.dispatchEvent) {
			return function ( element, event ) {
				return element.dispatchEvent(event);
			};
		} else if(doc.fireEvent) {
			return function ( element, event ) {
				return element.fireEvent(event);	
			};
		} else {
			console.log('dispatching events not supported in this browser');
		}
		
	});
	
})();
