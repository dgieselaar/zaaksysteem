/*global IBAN,angular,_*/
(function () {
    "use strict";
    angular.module('Zaaksysteem').directive('zsIbanValidate', [ '$parse', function ( $parse ) {

        function checkIban(value) {
            return IBAN.isValid(value);
        }

        function elfProef(value) {
            var total = 0,
                factor = value.length;

            _.each(value.split(''), function (char) {
                total += char * factor;
                factor -= 1;
            });

            return !(total % 11);
        }

        function checkNumber(value) {
            return checkIban(value) || value.match(/^\d{9,}$/) ? elfProef(value) : value.match(/^\d{7}$/);
        }

        function checkBanknumber(value, enforce) {
            return enforce ? checkIban(value) : checkNumber(value);
        }

        return {
            restrict: 'A',
            require: [ 'ngModel', '?^zsCaseWebformIbanField' ],
            scope: false,
            link: function (scope, element, attrs, controllers ) {
                
                var ngModel = controllers[0],
                    zsCaseWebformIbanField = controllers[1];
                
                if(zsCaseWebformIbanField) {
                    zsCaseWebformIbanField.setIbanValidate({
                        setValue: function ( value ) {
                            $parse(attrs.ngModel).assign(scope, value);
                        }
                    });
                }

                var validator = function (value) {
                    // For now required fields checking has to be done by the backend
                    // therefore empty is considered valid by this validator.
                    // $isEmpty considers NaN, undefined and null empty as well,
                    // though only empty string should count. Chose $isEmpty to
                    // follow common angularjs pattern.
                    var valid = ngModel.$isEmpty(value) || checkBanknumber(value, scope.enforce);
                    ngModel.$setValidity('zsIbanValidate', valid);
                    return valid ? value : undefined;
                };

                ngModel.$parsers.unshift(validator);
                ngModel.$formatters.unshift(function (value) {
                    // on page load if the field validates as normal banknumber, uncheck IBAN checkbox.
                    scope.enforce = !value || !!checkBanknumber(value, true);
                    return validator(value);
                });

                scope.$watch('enforce', function () {
                    validator(element.val());
                });
                
                if(zsCaseWebformIbanField) {
                    scope.$watch('$destroy', function ( ) {
                        zsCaseWebformIbanField.unsetIbanValidate();
                    });
                }
            }
        };
    }]);
}());
