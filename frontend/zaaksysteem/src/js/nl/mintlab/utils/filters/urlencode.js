/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.filter('urlencode', [ function ( ) {
			
			return function ( str ) {
				return encodeURIComponent(str);
			};
			
		}]);
	
})();