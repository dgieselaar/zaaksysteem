/*global angular,_,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseWebformObjectTypeMutation', [ 'translationService', function ( translationService ) {
			
			var cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent');
			
			return {
				require: [ 'zsCaseWebformObjectTypeMutation', '^zsCaseWebformObjectTypeField' ],
				controller: [ '$scope', '$attrs', function ( $scope, $attrs ) {
					
					var ctrl = this,
						zsCaseWebformObjectTypeField,
						open = false,
						mutation = $scope.$eval($attrs.mutation),
						loading = false,
						form;
					
					ctrl.openForm = function ( ) {
						open = true;
					};
					
					ctrl.closeForm = function ( ) {
						open = false;
					};
					
					ctrl.isOpen = function ( ) {
						return open;
					};
					
					ctrl.toggleForm = function ( ) {
						open = !open;
					};
					
					ctrl.getLabel = function ( ) {
						return mutation.label;
					};
					
					ctrl.getTypeLabel = function ( ) {
						var type = mutation.type,
							label,
							pastTense = mutation.state === 'executed' || mutation.state === 'failed';
							
						switch(type) {
							case 'relate':
							label = translationService.get(pastTense ? 'Gerelateerd' : 'Te relateren');
							break;
							
							case 'delete':
							label = translationService.get(pastTense ? 'Verwijderd' : 'Te verwijderen');
							break;
							
							case 'update':
							label = translationService.get(pastTense ? 'Gewijzigd' : 'Te wijzigen');
							break;
							
							case 'create':
							label = translationService.get(pastTense ? 'Aangemaakt' : 'Aan te maken');
							break;
						}
						
						if(mutation.state === 'failed') {
							label = translationService.get('Niet') + ' ' + label.toLowerCase();
						}
						
						return label;
					};
					
					ctrl.hasError = function ( ) {
						return mutation.state === 'failed';
					};
					
					ctrl.isInvalid = function ( ) {
						return !mutation.valid;
					};
					
					ctrl.getStateMessage = function ( ) {
						var msg;
						
						switch(mutation.state) {
							case 'pending':
							msg = translationService.get('Deze actie wordt pas uitgevoerd bij de fase overgang');
							break;
							
							case 'failed':
							msg = translationService.get('Deze actie kon niet worden uitgevoerd. Vraag uw beheerder om meer informatie.');
							break;
							
							case 'executed':
							msg = '';
							break;
						}
						
						return msg;	
					};
					
					ctrl.setControls = function ( ) {
						zsCaseWebformObjectTypeField = arguments[0];	
						
						zsCaseWebformObjectTypeField.addControl(ctrl);
						$scope.$on('$destroy', function ( ) {
							zsCaseWebformObjectTypeField.removeControl(ctrl);
						});
					};
					
					ctrl.setValues = function ( values ) {
						_.each(values, function ( value, key ) {
							mutation.values[key] = value;
						});
					};
					
					ctrl.handleButtonClick = function ( /*$event*/ ) {
						open = !open;
					};
					
					ctrl.getMutation = function ( ) {
						return mutation;	
					};
					
					ctrl.isValid = function ( ) {
						return form && form.isValid();	
					};
					
					ctrl.confirm = function ( $event ) {
						var values = _.pick(form.getValues(), _.identity);
						
						cancelEvent($event);
						
						loading = true;
						
						zsCaseWebformObjectTypeField.updateMutation(mutation, values)
							.then(function ( ) {
								ctrl.closeForm();
							})
							['finally'](function ( ) {
								loading = false;
							});
					};
					
					ctrl.cancel = function ( ) {
						loading = true;
						zsCaseWebformObjectTypeField.removeMutation(mutation)
							['finally'](function ( ) {
								loading = false;
							});
					};
					
					ctrl.setForm = function ( f ) {
						form = f;
					};
					
					ctrl.isLoading = function ( ) {
						return loading || form && form.isLoading();
					};
					
					ctrl.canChange = function ( ) {
						return zsCaseWebformObjectTypeField.canChange() && !mutation.read_only;	
					};
					
					ctrl.isEditable = function ( ) {
						var mutationType = mutation.type,
							isEditable;
							
						isEditable = ctrl.canChange() && (mutationType === 'create' || mutationType === 'update');
						
						return isEditable;
					};
					
					return ctrl;
					
				}],
				controllerAs: 'caseWebformObjectTypeMutation',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].setControls.apply(controllers[0], controllers.slice(1));
					
				}
			};
			
		}]);
	
})();
