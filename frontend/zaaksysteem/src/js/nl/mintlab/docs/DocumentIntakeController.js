/*global angular,fetch,$*/
(function ( ) {
	
	angular.module('Zaaksysteem.docs')
		.controller('nl.mintlab.docs.DocumentIntakeController', [ '$scope', 'smartHttp', function ( $scope, smartHttp ) {
			
			var File = fetch('nl.mintlab.docs.File'),
				Folder = fetch('nl.mintlab.docs.Folder');
				
			$scope.visibilities = [];
			$scope.visibility = 'all';
			
			$scope.readOnly = false;
			
			$scope.loading = false;
			
			$scope.initialize = function ( ) {
				
				$scope.docGlobal = new Folder( { name: 'global' } );
				$scope.loading = true;
					
				smartHttp.connect({
					url: '/zaak/intake/get_visibility_for_user'
				})
					.success(function ( response ) {
						$scope.visibilities = response.result;
						$scope.visibility = $scope.visibilities[0] ? $scope.visibilities[0].value : null;
					})
					.error(function ( ) {
						$scope.loading = false;
					});
			};
			
			$scope.reloadData = function ( ) {
				
				$scope.loading = true;
				
				if(!$scope.visibility) {
					// $scope.docGlobal = new Folder( { name: 'global' } );
					return;
				}
				
				smartHttp.connect({
					url: 'file/search_queue',
					method: 'GET',
					params: {
						visibility: $scope.visibility
					}
				})
					.success(function ( data ) {
						var docs,
							file,
							i,
							l;
							
						docs = data.result || [];
						
						$scope.docGlobal.empty();
						
						
						for(i = 0, l = docs.length; i < l; ++i) {
							file = new File();
							file.updateWith(docs[i]);
							$scope.docGlobal.add(file);
						}
						
					})
					.error(function ( ) {
						
					})
					.then(function ( ) {
						$scope.loading = false;
					}, function ( ) {
						$scope.loading = false;
					});
			};
			
			$scope.registerCase = function ( file ) {
				
				$('#ezra_nieuwe_zaak_tooltip').trigger({
					type: 'nieuweZaakTooltip',
					show: 1,
					popup: 1,
					action: '/zaak/create/?actie=doc_intake&amp;actie_value=' + file.id + '&amp;actie_description=Document%20aan%20zaak%20toevoegen'
				});
				
			};
			
			$scope.$watch('visibility', function ( ) {
				$scope.reloadData();
			});
			
			
		}]);
})();