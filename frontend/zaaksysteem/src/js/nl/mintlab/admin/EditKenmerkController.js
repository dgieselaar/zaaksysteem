/*global angular,_,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin')
		.controller('nl.mintlab.admin.EditKenmerkController', [ '$scope', 'translationService', function ( $scope, translationService ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply'),
				arrayMove = fetch('nl.mintlab.utils.collection.arrayMove');
			
			$scope.showInactiveOptions = false;
			
			function resetOrder ( ) {
				_.each($scope.options, function ( opt, index ) {
					opt.sort_order = index;
				});
			}
			
			$scope.deleteOption = function ( option ) {
				var index = _.indexOf($scope.options, option);
				
				$scope.options.splice(index, 1);
				
				resetOrder();
			};
			
			$scope.addOption = function ( txt ) {
				var hasTxt = !!_.find($scope.options, function ( opt ) {
					return opt.value === txt;
				});
				
				if(hasTxt) {
					$scope.$emit('systemMessage', {
						type: 'error',
						content: translationService.get('U kunt een mogelijkheid slechts één keer toevoegen')
					});
				} else {
					$scope.options.push({
						active: true,
						value: txt,
						sort_order: $scope.options.length
					});
					$scope.addopt.$setPristine();
					$scope.newOption = '';
				}
			};
			
			$scope.isOptionVisible = function ( item ) {
				return item.active || item.id === undefined || $scope.showInactiveOptions;
			};
			
			$scope.toggleInactiveOptionsVisibility = function ( ) {
				$scope.showInactiveOptions = !$scope.showInactiveOptions;
			};
			
			$scope.hasInactiveOptions = function ( ) {
				return _.find($scope.options, function ( opt ) {
					return opt.id !== undefined && !opt.active;
				});
			};
			
			$scope.getJson = function ( ) {
				return JSON.stringify($scope.options);
			};
			
			$scope.$watch('options', function ( ) {
				if($scope.options) {
					_.each($scope.options, function ( opt ) {
						opt.active = !!opt.active;
					});
					resetOrder();
				}
			});
			
			$scope.$on('zs.sort.update', function ( event, data, before ) {
				safeApply($scope, function ( ) {
					var option,
						to = before ? _.findIndex($scope.options, { 'id': before.id }) : $scope.options.length-1;
						
					option = _.find($scope.options, { 'id': data.id });
					
					arrayMove($scope.options, option, to);
				});
			});
			
			$scope.$on('zs.sort.commit', function ( /*event, data*/ ) {
				safeApply($scope, function ( ) {
					resetOrder();
				});
			});
			
		}]);
	
})();