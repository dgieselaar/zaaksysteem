/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsCrudInstancesDataProvider', [ function ( ) {
			
			return {
				require: [ 'zsCrudTemplateParser', '^zsInstancesCrud' ],
				link: function ( scope, element, attrs, controllers ) {
					
					var zsCrudTemplateParser = controllers[0],
						zsInstancesCrud = controllers[1];
						
					function filter ( item ) {
						return zsInstancesCrud.options.showDeleted ? item : !zsInstancesCrud.isDeleted(item);
					}
					
					zsCrudTemplateParser.addFilter(filter);
					
					scope.$on('$destroy', function ( ) {
						zsCrudTemplateParser.removeFilter(filter);
					});
					
				}
			};
			
		}]);
	
})();