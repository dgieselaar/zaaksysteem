BEGIN;

    DROP TABLE IF EXISTS object_type_bibliotheek_entry;
    ALTER SEQUENCE object_type_bibliotheek_entry_id_seq RENAME TO object_bibliotheek_entry_id_seq;

    DROP TABLE IF EXISTS seen;
    DROP TABLE IF EXISTS kennisbank_relaties;
    DROP TABLE IF EXISTS kennisbank_producten;
    DROP TABLE IF EXISTS kennisbank_vragen;

    ALTER TABLE zaaktype_kenmerken ADD object_id UUID;
    ALTER TABLE zaaktype_kenmerken ADD FOREIGN KEY(object_id) REFERENCES object_data(uuid) ON DELETE RESTRICT;
    ALTER TABLE zaaktype_kenmerken ADD object_metadata text NOT NULL DEFAULT '{}';

    ALTER TABLE zaaktype_regel ADD COLUMN active BOOLEAN DEFAULT true;
    UPDATE zaaktype_regel SET active=true;

    ALTER TABLE object_relationships ADD COLUMN blocks_deletion BOOLEAN DEFAULT FALSE NOT NULL;

    DROP TABLE IF EXISTS object_mutation;

    CREATE TABLE object_mutation (
        id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
        object_uuid UUID UNIQUE REFERENCES object_data(uuid),
        object_type TEXT DEFAULT 'object',
        lock_object_uuid UUID REFERENCES object_data(uuid),
        type TEXT NOT NULL CHECK ( type in ('create', 'update', 'delete', 'relate') ),
        values TEXT NOT NULL DEFAULT '{}',
        date_created timestamp without time zone DEFAULT NOW(),
        subject_id INTEGER NOT NULL REFERENCES subject(id)
    );

    ALTER TABLE zaaktype_regel ADD COLUMN is_group BOOLEAN DEFAULT false;

    -- personen

    ALTER TABLE natuurlijk_persoon DROP COLUMN adres_buitenland1;
    ALTER TABLE natuurlijk_persoon DROP COLUMN adres_buitenland2;
    ALTER TABLE natuurlijk_persoon DROP COLUMN adres_buitenland3;
    ALTER TABLE gm_natuurlijk_persoon DROP COLUMN adres_buitenland1;
    ALTER TABLE gm_natuurlijk_persoon DROP COLUMN adres_buitenland2;
    ALTER TABLE gm_natuurlijk_persoon DROP COLUMN adres_buitenland3;

    ALTER TABLE adres ADD COLUMN adres_buitenland1 TEXT;
    ALTER TABLE adres ADD COLUMN adres_buitenland2 TEXT;
    ALTER TABLE adres ADD COLUMN adres_buitenland3 TEXT;
    ALTER TABLE adres ADD COLUMN landcode INTEGER default 6030;

    ALTER TABLE gm_adres ADD COLUMN adres_buitenland1 TEXT;
    ALTER TABLE gm_adres ADD COLUMN adres_buitenland2 TEXT;
    ALTER TABLE gm_adres ADD COLUMN adres_buitenland3 TEXT;
    ALTER TABLE gm_adres ADD COLUMN landcode INTEGER default 6030;

    -- Bedrijven

    ALTER TABLE bedrijf ADD COLUMN vestiging_adres_buitenland1 TEXT;
    ALTER TABLE bedrijf ADD COLUMN vestiging_adres_buitenland2 TEXT;
    ALTER TABLE bedrijf ADD COLUMN vestiging_adres_buitenland3 TEXT;
    ALTER TABLE bedrijf ADD COLUMN vestiging_landcode INTEGER default 6030;

    ALTER TABLE bedrijf ADD COLUMN correspondentie_adres_buitenland1 TEXT;
    ALTER TABLE bedrijf ADD COLUMN correspondentie_adres_buitenland2 TEXT;
    ALTER TABLE bedrijf ADD COLUMN correspondentie_adres_buitenland3 TEXT;
    ALTER TABLE bedrijf ADD COLUMN correspondentie_landcode INTEGER default 6030;


    ALTER TABLE gm_bedrijf ADD COLUMN vestiging_adres_buitenland1 TEXT;
    ALTER TABLE gm_bedrijf ADD COLUMN vestiging_adres_buitenland2 TEXT;
    ALTER TABLE gm_bedrijf ADD COLUMN vestiging_adres_buitenland3 TEXT;
    ALTER TABLE gm_bedrijf ADD COLUMN vestiging_landcode INTEGER default 6030;

    ALTER TABLE gm_bedrijf ADD COLUMN correspondentie_adres_buitenland1 TEXT;
    ALTER TABLE gm_bedrijf ADD COLUMN correspondentie_adres_buitenland2 TEXT;
    ALTER TABLE gm_bedrijf ADD COLUMN correspondentie_adres_buitenland3 TEXT;
    ALTER TABLE gm_bedrijf ADD COLUMN correspondentie_landcode INTEGER default 6030;

    ALTER TABLE natuurlijk_persoon ADD COLUMN landcode INTEGER default 6030 NOT NULL;
    ALTER TABLE gm_natuurlijk_persoon ADD COLUMN landcode INTEGER default 6030 NOT NULL;

    alter table bibliotheek_notificaties add sender text;
    alter table bibliotheek_notificaties add sender_address text;

    ALTER TABLE zaaktype_notificatie ADD cc text;
    ALTER TABLE zaaktype_notificatie ADD bcc text;

    ALTER TABLE logging DROP COLUMN IF EXISTS object_uuid;
    ALTER TABLE logging ADD COLUMN object_uuid UUID REFERENCES object_data (uuid) ON DELETE SET NULL;

    ALTER TABLE subject ADD last_modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

    ALTER TABLE bibliotheek_kenmerken DROP CONSTRAINT bibliotheek_kenmerken_value_type_check;
    ALTER TABLE bibliotheek_kenmerken ADD CONSTRAINT bibliotheek_kenmerken_value_type_check CHECK (value_type IN ('text_uc', 'checkbox', 'richtext', 'date', 'file', 'bag_straat_adres', 'email', 'valutaex', 'bag_openbareruimte', 'text', 'bag_openbareruimtes', 'url', 'valuta', 'option', 'bag_adres', 'select', 'valutain6', 'valutaex6', 'valutaex21', 'image_from_url', 'bag_adressen', 'valutain', 'calendar', 'bag_straat_adressen', 'googlemaps', 'numeric', 'valutain21', 'textarea', 'bankaccount', 'subject'));

    ALTER TABLE object_relationships ADD COLUMN title1 TEXT;
    ALTER TABLE object_relationships ADD COLUMN title2 TEXT;

    ALTER TABLE object_mutation DROP CONSTRAINT object_mutation_object_uuid_fkey;
    ALTER TABLE object_mutation ADD CONSTRAINT object_mutation_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES object_data (uuid) ON DELETE CASCADE;

    ALTER TABLE file ADD COLUMN queue BOOLEAN DEFAULT TRUE NOT NULL;

    INSERT INTO config(parameter, value, advanced) VALUES('custom_objects_enabled', '0', TRUE);

COMMIT;
