BEGIN;

    ALTER TABLE subject ADD COLUMN nobody BOOLEAN NOT NULL DEFAULT false;
    ALTER TABLE subject ADD COLUMN system BOOLEAN NOT NULL DEFAULT false;

    UPDATE subject SET system = true WHERE username IN ('admin', 'beheerder');

COMMIT;
