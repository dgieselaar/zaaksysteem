#!/usr/bin/perl -w
use strict;

use Moose;
use Data::Dumper;
use JSON;
use Time::HiRes qw(gettimeofday tv_interval);

use File::Copy;
use File::Temp qw/mktemp/;

use Cwd 'realpath';

use FindBin;
use lib "$FindBin::Bin/../lib";

use Catalyst qw[ConfigLoader];

use Zaaksysteem::Log::CallingLogger;
use Zaaksysteem::Model::DB;
use Zaaksysteem::Model::LDAP;

use Zaaksysteem::Geo;
use Text::CSV;

#use Geo::Proj4;
use LWP::UserAgent;

use HTTP::Request;
use LWP::UserAgent;

use encoding 'utf8';

use Config::Any;

my $log = Zaaksysteem::Log::CallingLogger->new();

error("USAGE: $0 [hostname] [configfile] [customer.d-dir] [dbname] [INTERFACE_ID] [START_ID] [stuf_url]") unless @ARGV && scalar(@ARGV) >= 6;
my ($hostname, $config_file, $config_dir, $dbname, $interface_id, $start_id, $stuf_url)    = @ARGV;

my $mig_config                              = {};

my $current_hostname;
sub load_customer_d {
    my $config_root = $config_dir;

    # Fetch configuration files
    opendir CONFIG, $config_root or die "Error reading config root: $!";
    my @confs = grep {/\.conf$/} readdir CONFIG;

    info("Found configuration files: @confs");

    for my $f (@confs) {
        my $config      = _process_config("$config_root/$f");
        my @customers   = $config;

        for my $customer (@customers) {
            for my $config_hostname (keys %$customer) {
                my $config_data     = $customer->{$config_hostname};

                if ($hostname eq $config_hostname ) {
                    info('Upgrading hostname: ' . $hostname);
                    $current_hostname = $hostname;
                    $mig_config->{database_dsn}         = $config_data->{'Model::DB'}->{connect_info}->{dsn};
                    $mig_config->{database_password}    = $config_data->{'Model::DB'}->{connect_info}->{password};
                    $mig_config->{ldap_config_basedn}   = $config_data->{LDAP}->{basedn};
                    $mig_config->{customer_info}        = { gemeente => $config_data->{customer_info} };

                    if ($dbname) {
                        $mig_config->{database_dsn} =~ s/dbname=([0-9a-zA-Z_-]+)/dbname=$dbname/;
                    }
                }
            }
        }
    }
}

sub load_zaaksysteem_conf {
    my $config_root = 'etc/customer.d/';
    my $config      = _process_config($config_file);

    $mig_config->{ldap_config_hostname}         = $config->{LDAP}->{hostname};
    $mig_config->{ldap_config_password}         = $config->{LDAP}->{password};
    $mig_config->{ldap_config_user}             = $config->{LDAP}->{admin};
    $mig_config->{ldap_config}                  = $config->{LDAP};
}

sub _process_config {
    my ($config) = @_;
    return Config::Any->load_files({
        files       => [$config],
        use_ext     => 1,
        driver_args => {}
    })->[0]->{$config};
}

load_customer_d();
load_zaaksysteem_conf();

error('Cannot find requested hostname: ' . $hostname) unless $current_hostname;

error('Missing one of required config params: ' . Data::Dumper::Dumper($mig_config))
    unless (
        $mig_config->{database_dsn}
    );

my $dbic = database($mig_config->{database_dsn}, $mig_config->{user}, $mig_config->{database_password});

$dbic->txn_do(sub {
    rerun_stuf($dbic);
});

$log->info('All done.');
$log->_flush;

sub database {
    my ($dsn, $user, $password) = @_;

    my %connect_info = (
        dsn             => $dsn,
        pg_enable_utf8  => 1,
    );

    $connect_info{password}     = $password if $password;
    $connect_info{user}         = $user if $user;

    Zaaksysteem::Model::DB->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => \%connect_info
    );

    my $schema = Zaaksysteem::Model::DB->new->schema;

    $schema->log($log);

    $schema->betrokkene_model->log($log);
    $schema->betrokkene_model->config($mig_config->{customer_info});

    info('Connected to: ' . $dsn);

    return $schema;
}

sub error {
    my $error = shift;

    $log->error($error);
    $log->_flush;

    die("\n");
}

sub info {
    $log->info(sprintf shift, @_);
    $log->_flush;
}


sub rerun_stuf {
    my $dbic        = shift;
    my $external_id = shift;

    my $transactions = $dbic->resultset('Transaction')->search(
        {
            -and    => [
                { 'me.interface_id' => $interface_id},
                { 'me.id' => { '>=' => $start_id }},
                { 'success_count' => { '>' => 0 }},
            ]
        },
        {
            order_by => 'me.id',
            prefetch => 'interface_id',
        }
    );

    my $ua              = LWP::UserAgent->new;
    $ua->ssl_opts(verify_hostname => 0);

    my $header          = new HTTP::Headers (
        'User-Agent'     => 'ZS SOAP 0.1',
        'Content-Type'   => 'text/xml; charset=utf-8',
        'SOAPAction'     =>  '',
    );

    info("Sending XML for interface_id: " . $interface_id . ' since transaction id: ' . $start_id);
    info("XML Count: " . $transactions->count);

    while (my $transaction = $transactions->next) {
        my $xml = $transaction->input_data;

        my $req = HTTP::Request->new('POST',$stuf_url,$header,Encode::encode_utf8($xml));

        my $res = $ua->request($req);

        info("\n\n==== SENDING ====\n\n" . $xml);
        info("\n\n===== RESPONSE ====\n\n" . $res->content);

        #die('Once: ' . $transaction->id);

        #die('Failed running transaction for: ' . $external_id) unless $new_transaction->success_count > 0;
    }

}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
