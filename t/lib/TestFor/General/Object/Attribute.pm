package TestFor::General::Object::Attribute;

# ./zs_prove -v t/lib/TestFor/General/Object/Attribute.pm
use base 'ZSTest';

use TestSetup;
use JSON;

use DateTime;
use Zaaksysteem::Object::Attribute;

use Zaaksysteem::Object::Types::ObjectAttributeTraits;
use Zaaksysteem::Object::Types::ObjectAttributeFilestore;
use Zaaksysteem::Object::Types::ObjectRelationSet;
use Zaaksysteem::Object::Types::ObjectRelationAttrs;

sub zs_oa : Tests {
    my $now = DateTime->now();

    my $oa1 = Zaaksysteem::Object::Attribute->new(
        name           => 'foobar',
        attribute_type => 'timestamp',
        value          => $now,
    );

    is_deeply(
        $oa1->human_value,
        $now->strftime('%d-%m-%Y'),
        "Single-value timestamp works OK"
    );

    my $oa2 = Zaaksysteem::Object::Attribute->new(
        name           => 'foobar',
        attribute_type => 'timestamp',
        value          => undef,
    );

    is_deeply(
        $oa2->human_value,
        undef,
        "Single-value timestamp 'undef' works OK"
    );

    my $oa3 = Zaaksysteem::Object::Attribute->new(
        name           => 'foobar',
        attribute_type => 'timestamp',
        value          => [undef, $now],
    );

    my $ok = is_deeply(
        $oa3->human_value,
        [ undef, $now->strftime('%d-%m-%Y') ],
        "Multivalue timestamps work OK"
    );
    if (!$ok) {
        diag explain $oa3->human_value;
    }
}

sub zs_oa_uniq_trait : Tests {
    my $obj = Zaaksysteem::Object::Types::ObjectAttributeTraits->new();

    my $outcome = { solo => 1, duo => 0, no_unique => 0, standaard => 0 };

    my $res;

    $res = {
         map { $_->name => $_->unique ? 1 : 0 }
        grep { $_->name ne 'date_created' && $_->name ne 'date_modified' }
        grep { $_->can('has_unique') }
             $obj->meta->get_all_attributes
    };

    is_deeply($res, $outcome, "Moose: has_unique trait");

    $res = {
         map { $_->name => $_->unique ? 1 : 0 }
        grep { $_->name ne 'date_created' && $_->name ne 'date_modified' }
             $obj->attribute_instances
    };

    is_deeply($res, $outcome, "ZS::Object: has_unique trait");
}

sub zs_oa_defined_attr : Tests {
    my $thing = Zaaksysteem::TestUtils::generate_random_string();
    my %opts = (solo => $thing);
    my $obj = Zaaksysteem::Object::Types::ObjectAttributeTraits->new(%opts);

    my %attributes;
    my @attr = $obj->attribute_instances;

    foreach (@attr) {
        next if $_->name eq 'date_modified' || $_->name eq 'date_created';
        $attributes{ $_->name } = $_->TO_JSON;

        delete $attributes{$_->name}{human_label};
    }

    is_deeply(
        \%attributes,
        {
            duo => {
                'attribute_type' => 'text',
                'name'           => 'duo'
            },
            no_unique => {
                'attribute_type' => 'text',
                'name'           => 'no_unique'
            },
            solo => {
                'attribute_type' => 'text',
                'human_value'    => $thing,
                'name'           => 'solo',
                'value'          => $thing,
            },
            standaard => {
                'attribute_type' => 'text',
                'human_value'    => "Dope shit ouwe",
                'name'           => 'standaard',
                'value'          => "Dope shit ouwe",
            }
        },
        "Attributes to JSON are correct"
    );

    my $obj_data = $obj->TO_JSON->{ values };

    delete $obj_data->{ date_created };
    delete $obj_data->{ date_modified };

    is_deeply($obj_data, { %opts, standaard => "Dope shit ouwe" }, "Options which are not required won't be shown as values");

    use Zaaksysteem::Object::Model;
    {
        no warnings qw(redefine once);
        local *Zaaksysteem::Object::Model::load_object_package = sub { bless{}, 'Zaaksysteem::Object::Types::ObjectAttributeTraits' } ;
        my $model = Zaaksysteem::Object::Model->new(schema => $zs->schema);
        my $saved = $model->save_object(object => $obj);
        isnt($saved->{id}, undef, "Object has an ID");
        my $saved_two = $model->save_object(object => $saved);
        is_deeply($saved, $saved_two, "The same");
    }
}

sub zs_oa_filestore_attr : Tests {
    my $filestore       = $zs->create_filestore_ok();

    # Two, one object, one not, to test coercion
    my $filestore_type  = {
        uuid            => $filestore->uuid,
        thumbnail_uuid  => $filestore->thumbnail_uuid,
        filename        => $filestore->original_name,
        size            => $filestore->size,
        mimetype        => $filestore->mimetype,
        md5             => $filestore->md5,
    };

    my $obj             = Zaaksysteem::Object::Types::ObjectAttributeFilestore->new(
        profile_picture => [$filestore_type],
    );

    my $model   = Zaaksysteem::Object::Model->new(
        schema => $zs->schema
    );

    $model->package_type_mapping->{filestore} = 'Zaaksysteem::Object::Types::ObjectAttributeFilestore';

    $zs->zs_transaction_ok(sub {
        my $back_and_forth = $model->save(object => $obj);

        my ($save) = $model->search('object_attribute_filestore');

        ### Validate we got a Filestore enty back from db
        isa_ok($save->profile_picture->[0], 'HASH');
        is_deeply(
            [sort keys %{ $save->profile_picture->[0] }],
            [qw(filename md5 mimetype size thumbnail_uuid uuid)],
            "Database contents look like a file store entry"
        );

        ### Stringified it is a human name
        is(
            $save->profile_picture->[0]->{filename},
            'FilestoreTest.txt',
            'Got correct filename from db'
        );
    }, 'Check inflate / deflate from database');
}

sub zs_or_basic : Tests {
    my $self = shift;

    my $object = Zaaksysteem::Object::Types::ObjectRelationAttrs->new(
        id => 'de305d54-75b4-431b-adb2-eb6b9e546014'
    );

    lives_ok { $object->any_object($object) } 'Unconstrained object relation';
    lives_ok { $object->any_object($object->_ref) } 'Unconstrained object->_ref relation';
    lives_ok { $object->self_object($object) } 'Constrained object relation';
    lives_ok { $object->self_object($object->_ref) } 'Constrained object->_ref relation';
    dies_ok { $object->derp_object($object) } 'Invalid constrained object relation';
    dies_ok { $object->derp_object($object->_ref) } 'Invalid constrained object->_ref relation';
    lives_ok { $object->override_isa_object($object) } 'isa-overridden object relation';
    dies_ok { $object->override_isa_object($object->_ref) } 'Invalid isa-overridden object relation';
}

sub zs_or_set : Tests {
    my $self = shift;

    lives_ok {
        Zaaksysteem::Object::Types::ObjectRelationSet->new(
            object_set => []
        );
    } 'instantiate with empty set';

    lives_ok {
        Zaaksysteem::Object::Types::ObjectRelationSet->new(
            object_set => [
                Zaaksysteem::Object::Types::ObjectRelationSet->new
            ]
        );
    } 'instantiates with set (N = 1)';

    lives_ok {
        Zaaksysteem::Object::Types::ObjectRelationSet->new(
            object_set => [
                Zaaksysteem::Object::Types::ObjectRelationSet->new,
                Zaaksysteem::Object::Types::ObjectRelationSet->new,
                Zaaksysteem::Object::Types::ObjectRelationSet->new
            ]
        );
    } 'instantiates with set (N = 3)';

    dies_ok {
        Zaaksysteem::Object::Types::ObjectRelationSet->new(
            object_set => [1]
        );
    } 'excepts with scalar value';

    dies_ok {
        Zaaksysteem::Object::Types::ObjectRelationSet->new(
            object_set => [
                Zaaksysteem::Object::Types::ObjectRelationSet->new,
                1
            ]
        );
    } 'excepts with valid object, scalar value';

    dies_ok {
        Zaaksysteem::Object::Types::ObjectRelationSet->new(
            object_set => [
                1,
                Zaaksysteem::Object::Types::ObjectRelationSet->new
            ]
        );
    } 'excepts with scalar value, valid object';

    dies_ok {
        Zaaksysteem::Object::Types::ObjectRelationSet->new(
            object_set => [
                Zaaksysteem::Object::Types::ObjectRelationAttrs->new,
                Zaaksysteem::Object::Types::ObjectRelationSet->new
            ]
        );
    } 'excepts with invalid object, valid object';

    dies_ok {
        Zaaksysteem::Object::Types::ObjectRelationSet->new(
            object_set => [
                Zaaksysteem::Object::Types::ObjectRelationSet->new,
                Zaaksysteem::Object::Types::ObjectRelationAttrs->new,
            ]
        );
    } 'excepts with valid object, invalid object';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
