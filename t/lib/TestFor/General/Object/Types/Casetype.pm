package TestFor::General::Object::Types::Casetype;

use base 'ZSTest';

use TestSetup;

use Zaaksysteem::Object;
use Zaaksysteem::Object::Types::Casetype;
use Zaaksysteem::Object::Types::CasetypePhase;
use Zaaksysteem::Object::Types::CasetypeResult;

sub zs_object_types_casetype_general : Tests {
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(sub {
        my $zaaktype_node = $zs->create_zaaktype_node_ok;
        my $zaaktype = $zs->create_zaaktype_ok(node => $zaaktype_node);

        my $registratie_status = $zs->create_zaaktype_status_ok(
            status => 1,
            fase => 'registratiefase',
            node => $zaaktype_node
        );

        my $afhandel_status = $zs->create_zaaktype_status_ok(
            status => 2,
            fase => 'afhandelfase',
            node => $zaaktype_node
        );

        my $simpel_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
            naam => 'simpel kenmerk',
            magic_string => 'simpel_kenmerk',
            value_type => 'text'
        );

        my $valuta_kenmerk = $zs->create_bibliotheek_kenmerk_ok(
            naam => 'valuta kenmerk',
            magic_string => 'valuta_kenmerk',
            value_type => 'valuta'
        );

        my $simpel_zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
            status => $registratie_status,
            bibliotheek_kenmerk => $simpel_kenmerk
        );

        my $valuta_zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
            status => $afhandel_status,
            bibliotheek_kenmerk => $valuta_kenmerk
        );

        my $resultaat = $zs->create_zaaktype_resultaat_ok(
            status => $afhandel_status,
            resultaat => 'verwerkt',
            archiefnominatie => 'Bewaren (B)',
            dossiertype => 'digitaal',
            ingang => 'verleend',
            label => 'my label',
            selectielijst => '12.34',
            comments => 'rfc'
        );

        my $casetype = $model->inflate_from_row(
            $zaaktype->_sync_object($zs->object_model)->discard_changes
        );

        isa_ok $casetype, 'Zaaksysteem::Object::Types::Casetype',
            'casetype inflated';

        my $instance_phases = $casetype->instance_phases;

        is ref $instance_phases, 'ARRAY', 'instance_phases isa array';
        is scalar @{ $instance_phases }, 2, 'two phases';

        for (@{ $instance_phases }) {
            isa_ok $_, 'Zaaksysteem::Object::Types::CasetypePhase',
                'casetype_phase sub-object'
        }

        my ($reg, $afh) = sort { $a->sequence <=> $b->sequence } @{ $instance_phases };

        is $reg->sequence, 1, 'registration phase sequence number';
        is $reg->label, 'registratiefase', 'registration phase label';
        is ref $reg->route, 'HASH', 'registration phase default route';

        my $reg_attrs = $reg->attributes;

        is ref $reg_attrs, 'ARRAY', 'registration phase has attribute array';
        is scalar @{ $reg_attrs }, 1, 'registration phse has one attribute';

        my $reg_attr = $reg_attrs->[0];

        is ref $reg_attr, 'HASH', 'registration phase attribute';
        is $reg_attr->{ type }, 'text', 'registration phase attribute type';

        is $reg_attr->{ original_label }, 'simpel kenmerk',
            'registration phase attribute label';

        is $reg_attr->{ magic_string }, 'simpel_kenmerk',
            'registration phase attribute magic string';

        is $afh->sequence, 2, 'resolution phase sequence number';
        is $afh->label, 'afhandelfase', 'resolution phase label';

        my $afh_attrs = $afh->attributes;

        is ref $afh_attrs, 'ARRAY', 'resolution phase has attribute array';
        is scalar @{ $afh_attrs }, 1, 'resolution phase has one attribute';

        my $afh_attr = $afh_attrs->[0];

        is ref $afh_attr, 'HASH', 'resolution phase attribute';

        is $afh_attr->{ type }, 'valuta', 'resolution phase attribute type';

        is $afh_attr->{ original_label }, 'valuta kenmerk',
            'resolution phase attribute label';

        is $afh_attr->{ magic_string }, 'valuta_kenmerk',
            'resolution phase attribute magic string';

        my $results = $casetype->instance_results;
        
        is ref $results, 'ARRAY', 'casetype has instance results array';
        is scalar @{ $results }, 1, 'casetype has one instance result';

        for (@{ $results }) {
        isa_ok $_, 'Zaaksysteem::Object::Types::CasetypeResult',
                'casetype_result sub-object';
        }

        my $result = $results->[0];

        is $result->retention_period, 365, 'casetype result retention period';
        is $result->dossier_type, 'digitaal', 'casetype result dossier type';
        is $result->label, 'my label', 'casetype result label';
        is $result->selection_list, '12.34', 'casetype result selection list';
        is $result->explanation, 'rfc', 'casetype result explanation';

        is $result->archival_type, 'Bewaren (B)',
            'casetype result archival type (yuk)';

        is $result->retention_period_source_date, 'verleend',
            'casetype result retention period source date';
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
