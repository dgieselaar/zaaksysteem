package Zaaksysteem::API::v1::Serializer::Reader::ZaaktypeKenmerk;
use Moose;

use Zaaksysteem::Tools;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::ZaaktypeKenmerk - A Zaaktype kenmerk reader

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 METHODS

=head2 class

=cut

sub class { 'Zaaksysteem::DB::Component::ZaaktypeKenmerken' }

=head2 read

=cut

sig read => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read {
    my ($class, $serializer, $kenmerk) = @_;

    my $rv      = {
        is_group            => $class->to_bool($kenmerk->is_group),
        help                => $kenmerk->help,
        label               => $kenmerk->label,
        required            => $class->to_bool($kenmerk->value_mandatory),
        type                => undef,
        id                  => $kenmerk->id,
        label_multiple      => $kenmerk->label_multiple,
        multiple_values     => undef,
        values              => undef,
    };

    if (my $attr = $kenmerk->bibliotheek_kenmerken_id) {
        $rv = {
            %$rv,
            type                => $attr->value_type,
            catalogue_id        => $attr->id,
            magic_string        => $attr->magic_string,
            limit_values        => ($attr->type_multiple ? -1 : 1),
            values              => $attr->options,
            permissions         => $kenmerk->format_required_permissions($serializer),
            label               => ($rv->{label} || $attr->naam),
            original_label      => $attr->naam,
            is_system           => $class->to_bool($kenmerk->is_systeemkenmerk),
            referential         => $class->to_bool($kenmerk->referential),
        };
    }
    elsif(my $object_type_id = $kenmerk->object_id) {
        my $prefix = $object_type_id->get_object_attribute('prefix');

        $rv = {
            %$rv,
            type               => 'object',
            object_id          => $kenmerk->get_column('object_id'),
            object_type_prefix => $prefix->value,
            object_metadata    => $kenmerk->object_metadata,
        };
    }

    return $rv;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
