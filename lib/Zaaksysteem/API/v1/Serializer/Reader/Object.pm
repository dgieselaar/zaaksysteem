package Zaaksysteem::API::v1::Serializer::Reader::Object;

use Moose;

use Zaaksysteem::Tools;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Object - Read L<Zaaksysteem::Object>
instances.

=head1 DESCRIPTION

=head1 METHODS

=head2 class

Returns C<Zaaksysteem::Object>. Required by
L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Object' }

=head2 read

Returns a hashref data structure of instance data for serialization.
Required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

    {
        type => 'my_object',
        reference => '1123fg...',
        instance => {
            some_attr => 'some value',
            some_relation => { type => 'some_related_object', ... }
        }
    }

=cut

sub read {
    my ($class, $serializer, $object, $opts) = @_;

    if ($object->type eq 'casetype') {
        return $class->read_casetype($serializer, $object, $opts);
    }

    my @attrs = grep { $_->does('Zaaksysteem::Metarole::ObjectAttribute') }
                     $object->meta->get_all_attributes;

    my @rels = grep { $_->does('Zaaksysteem::Metarole::ObjectRelation') }
                    $object->meta->get_all_attributes;

    my %instance_attributes = map { my $value = $_->get_value($object); $_->name => (blessed $value ? $serializer->read($value) : $value) } @attrs;
    my %instance_relations  = map { $_->name => ($_->get_value($object) ? $serializer->read($_->get_value($object)) : undef) } @rels;

    return {
        type => $object->type,
        reference => $object->id,
        instance => { %instance_attributes, %instance_relations }
    };
}

=head2 read_casetype

Type-specific reader for casetype object instances.

=cut

# Casetype reader is explicitly non-generic, because v1 already has a casetype
# serialization we must adhere to for now
sub read_casetype {
    my ($class, $serializer, $object, $opts) = @_;

    my @phases = map {
        $class->_read_phase($_, {
            lock_registration_phase => $object->lock_registration_phase
        });
    } @{ $object->instance_phases };

    my @results = map {
        $class->_read_result($_);
    } @{ $object->instance_results };

    my $preset_client;
    if ($object->has_preset_client) {
        $preset_client = {
            instance  => $object->preset_client,
            type      => 'subject',
            reference => undef,
        };
    }

    return {
        type => 'casetype',
        reference => $object->id,
        instance => {
            id => $object->id,
            title => $object->name,
            trigger => $object->trigger,
            subject_types => $object->subject_types,
            preset_client => $preset_client,
            sources => $object->sources,
            results => \@results,
            phases => \@phases,
            queue_coworker_changes => ($object->queue_coworker_changes ? JSON::true : JSON::false),
            legacy => {
                zaaktype_id => $object->casetype_id,
                zaaktype_node_id => $object->casetype_node_id,
            },
            properties => {
                verlenging_mogelijk => $object->extendable ? 'Ja' : 'Nee',
                lex_silencio_positivo => $object->lex_silencio_positivo ? 'Ja' : 'Nee',
                lokale_grondslag => $object->local_basis,
                verantwoordingsrelatie => $object->supervisor_relation,
                publicatietekst => $object->publication_text,
                opschorten_mogelijk => $object->suspendable ? 'Ja' : 'Nee',
                beroep_mogelijk => $object->appealable ? 'Ja' : 'Nee',
                publicatie => $object->publishable ? 'Ja' : 'Nee',
                vertrouwelijkheidsaanduiding => $object->confidentiality,
                wkpb => $object->wkpb ? 'Ja' : 'Nee',
                doel => $object->purpose,
                archiefclassificatie => $object->records_classification,
                e_formulier => $object->eform,
                verantwoordelijke => $object->supervisor,
                aanleiding => $object->motivation,
                wet_dwangsom => $object->penalty
            }
        }
    };
}

sub _read_result {
    my $self = shift;
    my $result = shift;

    return {
        resultaat_id => $result->casetype_result_id,
        type => $result->generic_result_type,
        archive_procedure => $result->retention_period_source_date,
        period_of_preservation => $result->retention_period,
        type_of_dossier => $result->dossier_type,
        label => $result->label,
        selection_list => $result->selection_list,
        type_of_archiving => $result->archival_type,
        comments => $result->explanation,
        external_reference => $result->external_reference,
        trigger_archival => $result->trigger_archival
    };
}

sub _read_phase {
    my $self = shift;
    my $phase = shift;
    my $opts = shift;

    return {
        name => $phase->label,
        seq => $phase->sequence,
        id => $phase->casetype_phase_id,
        fields => $phase->attributes,
        locked => $opts->{ lock_registration_phase } && $phase->sequence == 1 ? \1 : \0
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
