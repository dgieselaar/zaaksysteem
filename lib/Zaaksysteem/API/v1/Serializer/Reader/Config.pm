package Zaaksysteem::API::v1::Serializer::Reader::Config;
use Moose;

with 'Zaaksysteem::API::v1::Serializer::ReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Config - Reader for "Config" database rows

=head1 SYNOPSIS

=head1 METHODS

=head2 class

Returns the supported class ("Zaaksysteem::Schema::Config") for serialization.

=cut

sub class { 'Zaaksysteem::Schema::Config' }

=head2 read

Turn a config row into an APIv1 result

=cut

sub read {
    my ($class, $serializer, $config) = @_;

    return {
        reference => $config->parameter,
        instance => {
            parameter => $config->parameter,
            value     => $config->value,
        },
        type      => 'config',
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
