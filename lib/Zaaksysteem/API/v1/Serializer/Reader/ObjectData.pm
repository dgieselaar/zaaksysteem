package Zaaksysteem::API::v1::Serializer::Reader::ObjectData;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Constants qw[ZAAKSYSTEEM_CONSTANTS];

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::ObjectData - Read ObjectData rows

=head1 SYNOPSIS

    my $reader = Zaaksysteem::API::v1::Serializer::Reader::ObjectData->grok($object);

    my $data = $reader->($serializer, $object);

=head1 DESCRIPTION

This class implements a serializer reader for
L<Zaaksysteem::Backend::Object::Data::Component> objects.

This class should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=head1 METHODS

=head2 grok

Implements sub required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub grok {
    my ($class, $object) = @_;

    return unless blessed $object && $object->isa($class->class);

    if ($object->object_class eq 'case') {
        return sub { $class->read_case(@_) };
    }

    if ($object->object_class eq 'casetype') {
        return sub { $class->read_casetype(@_) };
    }

    return;
}

=head2 class

Implements sub required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Backend::Object::Data::Component' }

=head2 read_case

Implements sub required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sig read_case => 'Zaaksysteem::API::v1::Serializer, Object, ?HashRef => HashRef';

sub read_case {
    my $class = shift;
    my ($serializer, $object, $opts) = @_;

    my $documents;
    my $with_documents;

    my $attribute_config = $class->_get_case_attributes(@_);
    my $attributes       = $attribute_config->{attributes};

    if($attribute_config->{with_documents}) {
        # Yeah, we *should* just dump a Zaaksysteem::Object::Iterator here,
        # however, that package relies on Backend::Object::Data::ResultSet to
        # do some of it's magic, the File RS is not such an object.
        $documents = {
            type => 'set',
            instance => {
                rows => [ map { $serializer->read($_) } $object->documents->all ]
            }
        };
    }

    my @base_attrs = map { $object->get_object_attribute(sprintf('case.%s', $_)) } qw[
        number
        status
        subject_external
        phase
        result
        date_of_registration
        date_target
    ];

    my %kvp = (
        casetype => {
            type => 'casetype',
            reference => $object->get_column('class_uuid'),
            instance => {}
        }
    );

    for my $attr (@base_attrs) {
        my (undef, $name) = split m[\.], $attr->name;
        my $value = $attr->value;

        $kvp{ $name } = blessed $value ? $serializer->read($value) : $value;
    }

    my $casetype_name = $object->get_object_attribute('case.casetype.name')->value;
    $kvp{'casetype'}->{instance}->{name} = blessed $casetype_name ? $serializer->read($casetype_name) : $casetype_name;

    ### Relationships
    my $relations = $class->_load_relations(@_);

    return {
        type => 'case',
        reference => $object->id,
        instance => {
            id          => $object->id,
            %kvp,
            %$relations,
            attributes => $attributes,
        }
    };
}

sub _get_case_attributes {
    my $class = shift;
    my ($serializer, $object, $opts) = @_;
    my $rv = {
        'return_documents'  => 0,
        'attributes'        => {}
    };

    return $rv unless (
        $serializer->has_full_access ||
        ($opts && exists $opts->{ fields } && scalar @{ $opts->{ fields } })
    );

    my @fields;
    if ($serializer->has_full_access) {
        $rv->{return_documents} = 1;
    } else {
        $rv->{return_documents} = 1 if grep { $_ eq 'case.documents' } @{ $opts->{ fields } };
        @fields      = map { m/^attribute\.(.*)/ } @{ $opts->{ fields } };
    }

    my %attributes;

    # Retrieve attributes the hard way to prevent (expensive) case inflations
    my $attrs = $object->result_source->schema->resultset('ZaaktypeKenmerken')->search(
        {
            zaaktype_node_id => $object->get_object_attribute('case.casetype.node.id')->value,
            bibliotheek_kenmerken_id => { '!=' => undef },
            $serializer->has_full_access ? () : (
                publish_public => 1,
                'bibliotheek_kenmerken_id.magic_string' => { -in => \@fields }
            ),
        },
        {
            prefetch => 'bibliotheek_kenmerken_id'
        }
    );

    for my $attr ($attrs->all) {
        my $magic_string = $attr->bibliotheek_kenmerken_id->magic_string;
        my $attrname     = "attribute." . $magic_string;

        next unless $object->has_object_attribute($attrname);

        my $value        = $object->get_object_attribute($attrname)->value;
        if ($attr->bibliotheek_kenmerken_id->value_type eq 'file') {
            $attributes{ $magic_string } = @$value ? [ map({ blessed $_ ? $_->uuid : $_->{uuid}} @$value) ] : [];
        } else {
            ### Make sure we get the plain text value for values, to prevent DateTime objects to expose to the outside
            my $plain_value = $class->_process_case_attribute_value($serializer, $value);
            $attributes{ $magic_string } = [ $plain_value ];
        }
    }

    $rv->{attributes} = \%attributes;

    return $rv;
}

=head2 _load_relations

    my $relations_serialized = $self->_load_relations

=cut

sub _load_relations {
    my $class   = shift;
    my ($serializer, $object, $opts) = @_;
    my %rv;

    my %mapping = (
        'relations'        => 'case.related_uuids',
    );

    for my $reltype (keys %mapping) {
        $rv{$reltype} = {
            type        => 'set',
            instance    => {
                rows    => [],
            }
        };

        next unless ($object->has_object_attribute($mapping{$reltype}));

        my $value = $object->get_object_attribute($mapping{$reltype})->value;
        $value    = blessed $value ? $serializer->read($value) : $value;

        my @ids = split(/,/, $value);

        for my $id (@ids) {
            push (
                @{ $rv{$reltype}->{instance}->{rows} },
                {
                    reference   => $id,
                    type        => 'case',
                }
            );
        }
    }

    my $rs = $object->result_source->schema->resultset('ObjectRelationships')->search(
        {
            object1_type => 'case',
            object2_type => 'case',
            '-or' => [
                { object1_uuid => $object->id, type1 => 'parent' },
                { object2_uuid => $object->id, type2 => 'parent' },
            ]
        }
    );

    my @rows;
    while (my $rel = $rs->next) {
        push(@rows,
            {
                reference => $rel->get_column('object1_uuid') eq $object->id
                ? $rel->get_column('object2_uuid')
                : $rel->get_column('object1_uuid'),
                type => 'case',
            }
        );
    };

    $rv{child_relations} = {
        type        => 'set',
        instance    => {
            rows    => \@rows,
        }
    };

    $rv{parent_relation} = {};
    if ($object->has_object_attribute('case.parent_uuid') && (my $value = $object->get_object_attribute('case.parent_uuid')->value)) {
        $rv{parent_relation} = {
            reference   => $value,
            type        => 'case'
        }
    }

    my $rel = $class->_get_relationships_per_type($object);

    $rv{case_relationships} = {
        parent => keys %{$rv{parent_relation}} || undef,
        child  => @rows || undef,
        %$rel,
    };

    return \%rv;
}

sub _get_relationships_per_type {
    my ($class, $object) = @_;

    my $rs = $object->result_source->schema->resultset('ObjectRelationships')->search(
        {
            object1_type => 'case',
            object2_type => 'case',
            '-or' => [
                { object1_uuid => $object->id, type1 => { '!=' => [ 'parent', 'child'] }},
                { object2_uuid => $object->id, type1 => { '!=' => [ 'parent', 'child'] }},
            ]
        }
    );

    my $relationships = {};
    while (my $rel = $rs->next) {
        my $relationship_type;
        my $relationship_id;

        if ($rel->get_column('object1_uuid') eq $object->id) {
            $relationship_type = $rel->get_column('type2');
            $relationship_id   = $rel->get_column('object2_uuid');
        }
        else {
            $relationship_type = $rel->get_column('type1');
            $relationship_id   = $rel->get_column('object1_uuid');
        }

        $relationships->{$relationship_type} //= {
            type     => 'set',
            instance => {
                rows => [],
            },
        };

        push(@{$relationships->{$relationship_type}{instance}{rows}}, {
            reference => $relationship_id,
            type      => 'case',
        });

    }
    return $relationships;
}

sub _process_case_attribute_value {
    my $self        = shift;
    my $serializer  = shift;
    my $value       = shift;

    my $processor   = sub {
        my $val         = shift;

        if (blessed($val)) {
            $val = $serializer->read($val);
        }

        return $val;
    };

    if (ref($value) eq 'ARRAY') {
        return [ map { $processor->($_) } @$value];
    }

    return $processor->($value);
}

sig read_casetype => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read_casetype {
    my ($class, $serializer, $object) = @_;

    my $zaaktype = $object->get_source_object;

    my $node;

    ### ZDHACK
    ### First try a version, when no node found, fall back to current version below
    if ($object->{_zaaktype_node_id}) {
        $node   = $zaaktype->zaaktype_nodes->search({ id => $object->{_zaaktype_node_id} })->first;
    }
    ### END ZDHACK

    ## Fallback
    $node   = $zaaktype->zaaktype_node_id unless $node;


    my $statussen = $node->zaaktype_statussen->search(undef, {
        order_by => { -asc => 'status' },
    });

    my @subject_types = $node->zaaktype_betrokkenen->search->get_column('betrokkene_type')->all;

    my @phases = map {
        $serializer->read($_, { node => $node })
    } $statussen->all;

    my @results = map {
        $serializer->read($_)
    } $node->zaaktype_resultaten->search({}, {order_by => 'me.id'})->all;

    my $id = $node->zaaktype_definitie_id->preset_client;
    my $preset_client;
    if ($id) {
        $preset_client = {
            instance => {
                betrokkene_id => $id,
                name          => $node->zaaktype_definitie_id->preset_client_name // undef,
            },
            type      => 'subject',
            reference => undef,
        };
    }

    return {
        type => 'casetype',
        reference => $object->id,
        instance => {
            id                  => $object->id,
            title               => $node->titel,
            trigger             => $node->trigger,
            subject_types       => \@subject_types,
            preset_client       => $preset_client,
            sources             => ZAAKSYSTEEM_CONSTANTS->{ contactkanalen },
            results             => \@results,
            phases              => \@phases,
            properties          => $node->properties,
            legacy              => {
                zaaktype_node_id    => $node->id,
                zaaktype_id         => $zaaktype->id,
            }
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
