package Zaaksysteem::XML::Zorginstituut::Reader;
use Moose;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::XML::Zorginstituut::Reader - An Zorginstituut reader for ZS

=head1 SYNOPSIS

    #!perl
    package Zaaksysteem::XML::Zorginstituut::WMO30X;
    use Moose;

    with 'Zaaksysteem::XML::Zorginstituut::Reader';

    # more code here

In some other module

    #!perl

    use Zaaksysteem::XML::Zorginstituut::WMO30X;
    my $reader = Zaaksysteem::XML::Zorginstituut::WMO30x->new(xml => $xml);
    $reader->is_valid();
    my $datastructure = $reader->show_invalid();

=head1 DESCRIPTION

This module will read Zorginstituut answer messages and tell you if the message says
"All ok" or "Something is wrong"

You don't want to instantiate this module directly but use it to extend it for a particular message type.

=cut

use XML::LibXML;
use XML::LibXML::XPathContext;

use Zaaksysteem::Types qw(NonEmptyStr);
use Zaaksysteem::Tools;

has xml => (
    is       => 'ro',
    isa      => NonEmptyStr,
    required => 1,
);

has libxml => (
    is      => 'ro',
    isa     => 'XML::LibXML::Document',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return XML::LibXML->load_xml(string => $self->xml);
    }
);

has xpath => (
    is      => 'ro',
    isa     => 'XML::LibXML::XPathContext',
    lazy    => 1,
    builder => '_xpath_builder',
);

has message_mapping => (
    is     => 'ro',
    isa    => 'HashRef',
    lazy   => 1,
    builder => '_build_message_mapping',
);

=head2 ns

Name space for the retourcode

=cut

has ns => (
    is       => 'ro',
    isa      => NonEmptyStr,
    required => 1,
);

=head1 BUILDERS

=cut

sub _xpath_builder {
    my $self = shift;
    my $xp   = XML::LibXML::XPathContext->new($self->libxml);
    return $xp;
}

sub _build_message_mapping { return {} }

=head2 find

    $self->find(xpath expression);

Find all the nodes for the given XPath.

=cut

sub find {
    my $self  = shift;
    return scalar $self->xpath->findnodes(@_);
}

=head2 get

    $self->get(xpath expression);

Similar to C<find>, but dies when nothing is found.

=cut

sub get {
    my $self = shift;
    my $n = $self->find(@_);

    return $n if $n->size;

    throw('zorginstituut/get/nodelist/no_nodes', "No nodes found in nodelist, expected one!" . dump_terse(\@_));
}

=head2 get_one_node_from_nodelist

    $self->one_node_from_nodelist(xpath expression);

Get a node from a nodelist, dies when the size of the nodelist is greater than 1.

=cut

sub get_one_node_from_nodelist {
    my ($self, $nl) = @_;
    if ($nl->size > 1) {
        throw('iwmo/nodelist/assert_one', 'More than one node in the node where one was expected');
    }
    return $nl->get_node(1);
}

=head2 is_valid

    my $ok = $self->is_valid();

Checks if the message is valid.

=cut

sub is_valid {
    my $self = shift;

    my $sections = $self->message_mapping;

    if ($self->_header_and_client_valid) {
        return 1;
    }

    foreach (keys %$sections) {
        return 0 unless $self->_is_section_valid($_);
    }

    return 1;
}

sub _header_and_client_valid {
    my $self = shift;

    my $sections = $self->message_mapping;

    my $clienten = $sections->{clienten};

    if ($self->_is_section_valid('header')) {
        if (!$self->find($clienten->{xpath})) {
                return 1;
        }
    }
    return 0;
}

=head2 show_errors

    my $data = $self->show_errors();

Returns a datastructure which tells you what the faulty errorcodes are for each section of the message.

=cut

sub show_errors {
    my $self = shift;

    my $sections = $self->message_mapping;
    my %rv;

    if ($self->_header_and_client_valid) {
        return \%rv;
    }

    foreach (keys %$sections) {
        unless ($self->_is_section_valid($_)) {
             $rv{$_} = $self->_get_rc_from_section($_);
        }
    }
    return \%rv;
}

=head2 assert_section

Asserts whether or not a section is present in the mapping so you can retreive it.

=cut

sub assert_section {
    my ($self, $section) = @_;

    if (exists $self->message_mapping->{$section}) {
        return $self->message_mapping->{$section};
    }
    throw('assert_section', "No such mapping available: $section");

}

=head1 PRIVATE METHODS

=head2 _is_retourcode_correct

    $self->_is_retourcode_correct

For more information see the L<LDT Retourcodes documentation|https://modellen.istandaarden.nl/preview/wmo/index.php/LDT/RetourCode>

=cut

sig _is_retourcode_correct => 'Str';

sub _is_retourcode_correct {
    my ($self, $rc) = @_;
    return int($rc // 0) == '200' ? 1 : 0;
}

sub _is_section_valid {
    my ($self, $section) = @_;
    $self->assert_section($section);

    my $s = $self->_get_rc_from_section($section);
    foreach (keys %$s) {
        next unless defined $s->{$_};
        if (ref $s->{$_}) {
            foreach my $rc (@{$s->{$_}}) {
                return 0 unless $self->_is_retourcode_correct($rc);
            }
        }
        else {
            return 0 unless $self->_is_retourcode_correct($s->{$_});
        }
    }
    return 1;
}

sub _get_rc_from_section {
    my ($self, $section) = @_;
    my $xpath = $self->assert_section($section);

    my $ns = $self->ns;

    my (%rv, @optional);
    if (!exists $xpath->{optional} || !$xpath->{optional}) {
        if (!exists $xpath->{multiple} || !$xpath->{multiple}) {
            my $nl  = $self->get(join('/', $xpath->{xpath}, "$ns:RetourCode01"));
            my $node = $self->get_one_node_from_nodelist($nl);
            %rv = (RetourCode01 => $node->textContent);
        }
        else {
            my $nl     = $self->get($xpath->{xpath});
            my $nl_max = $nl->size;
            $nl = $self->get(join('/', $xpath->{xpath}, "$ns:RetourCode01"));
            my $max = $nl->size;
            if ($max == $nl_max) {
                my @rc;
                foreach (my $i = 1; $i<= $max; $i++) {
                    my $node = $nl->get_node($i);
                    push(@rc, $node->textContent);
                }
                %rv = (RetourCode01 => \@rc);
            }
            else {
                throw("zs/zorginstituut/nodes/missing", "RetourCode01 missing");
            }
        }
        @optional = qw(RetourCode02 RetourCode03);
    }
    else {
        @optional = qw(RetourCode01 RetourCode02 RetourCode03);
    }

    foreach (@optional) {
        my $nl   = $self->find(join('/', $xpath->{xpath}, "$ns:$_"));
        if ($nl) {
            if (!exists $xpath->{multiple} || !$xpath->{multiple}) {
                $rv{$_} = $self->get_one_node_from_nodelist($nl)->textContent;
            }
            else {
                my $max = $nl->size;
                my @rc;
                foreach (my $i = 1; $i<= $max; $i++) {
                    my $node = $nl->get_node($i);
                    push(@rc, $node->textContent);
                }
                $rv{$_} = \@rc;
            }
        }
        else {
            $rv{$_} = undef;
        }
    }
    return \%rv;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
