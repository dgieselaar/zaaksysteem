package Zaaksysteem::XML::Zorginstituut::JW302;
use Moose;

extends 'Zaaksysteem::XML::Zorginstituut::Reader';

=head1 NAME

Zaaksysteem::XML::Zorginstituut::JW302 - An Zorginstituut JW302 reader for ZS

=head1 SYNOPSIS

    use Zaaksysteem::XML::Zorginistituut::JW302;

    my $reader = Zaaksysteem::XML::Zorginistituut::JW302->new(
        xml => $xml_string,
    );

    $reader->is_valid();
    my $datastructure = $reader->show_invalid();

=head1 DESCRIPTION

This module will read a JW 302 messages and tell you if the message says
"All ok" or "Something is wrong". If the message is incorrect, you will
need to resend the JW301 with the correct data.

=cut

has '+ns' => (default => 'jw302');

around _xpath_builder => sub {
    my $orig = shift;
    my $self = shift;

    my $xp = $self->$orig(@_);

    $xp->registerNs('jw',
        'http://www.istandaarden.nl/ijw/2_0/basisschema/schema/2_0');
    $xp->registerNs('jw302',
        'http://www.istandaarden.nl/ijw/2_0/jw302/schema/2_0');

    return $xp;
};


sub _build_message_mapping {

    return {
        header => { xpath => '/jw302:Bericht/jw302:Header', optional => 1},
        clienten => { xpath => '/jw302:Bericht/jw302:Clienten', optional => 1 },
        client => {
            xpath    => '/jw302:Bericht/jw302:Clienten/jw302:Client',
            multiple => 1,
        },
        relatie => {
            xpath    => '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Relaties/jw302:Relatie',
            optional => 1,
            multiple => 1,
        },
        relatie_adres => {
            xpath    => '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Relaties/jw302:Relatie/jw302:Adres',
            optional => 1,
            multiple => 1,
        },
        adres_adres => {
            xpath    => '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Adressen/jw302:Adres',
        },
        beschikking => {
            xpath    => '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Beschikking',
        },
        beperking => {
            xpath    => '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Beschikking/jw302:Beperkingen/jw302:Beperking',
            optional => 1,
            multiple => 1,
        },
        beperkingscore => {
            xpath =>
                '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Beschikking/jw302:Beperkingen/jw302:Beperking/jw302:BeperkingScores/jw302:BeperkingScore',
            optional => 1,
            multiple => 1,
        },
        beperkingscore_commentaarregels => {
            xpath =>
                '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Beschikking/jw302:Beperkingen/jw302:Beperking/jw302:BeperkingScores/jw302:BeperkingScore/jw302:Commentaarregels',
            optional => 1,
            multiple => 1,
        },
        beperking_commentaarregels => {
            xpath    => '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Beschikking/jw302:Beperkingen/jw302:Beperking/jw302:Commentaarregels',
            optional => 1,
            multiple => 1,
        },
        beschiktproduct => {
            xpath    => '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Beschikking/jw302:BeschikteProducten/jw302:BeschiktProduct',
            multiple => 1,
        },
        beschiktproduct_commentaarregels => {
            xpath => '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Beschikking/jw302:BeschikteProducten/jw302:BeschiktProduct/jw302:Commentaarregels',
            optional => 1,
            multiple => 1,
        },
        toegewezenproduct => {
            xpath    => '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Beschikking/jw302:ToegewezenProducten/jw302:ToegewezenProduct',
            multiple => 1,
        },
        beschikking_commentaarregels => {
            xpath    => '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Beschikking/jw302:Commentaarregels',
            optional => 1,
            multiple => 1,
        },
        client_commentaarregels => {
            xpath    => '/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Commentaarregels',
            optional => 1,
            multiple => 1,
        },
    };
}

=head2 beschikkingsnummer

Returns value for the C<beschikkingsnummer> node of the embedded message.

=cut

sub beschikkingsnummer {
    my $self = shift;
    return $self->xpath->findvalue('/jw302:Bericht/jw302:Clienten/jw302:Client/jw302:Beschikking/jw302:Beschikkingnummer');
}

=head2 berichtidentificatienummer

Returns value for the C<berichtidentificatienummer> node of the embedded message.

=cut

sub berichtidentificatienummer {
    my $self = shift;
    return $self->xpath->findvalue('/jw302:Bericht/jw302:Header/jw302:Berichtidentificatie/jw:Identificatie');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
