package Zaaksysteem::Object::TypeMetaClass;

use Moose;

extends qw/Zaaksysteem::Object/;

with qw/
    Zaaksysteem::Object::Roles::Type
    Zaaksysteem::Object::Roles::Relation
    Zaaksysteem::Object::Roles::Security
    Zaaksysteem::Object::Roles::Log
/;

=head1 NAME

Zaaksysteem::Object::TypeMetaClass - Base class for instance_meta_classes

=head1 SYNOPSIS

    # Do not use directly, it gets instantiated via Zaaksysteem::Object::Types::Type

=head1 DESCRIPTION

Base class for instance meta classes. By using this class we gain a performance boost
of 50%

=cut

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
