package Zaaksysteem::Object::Queue::Model::Email;

use Moose::Role;

use Zaaksysteem::Tools;
use Zaaksysteem::Backend::Mailer;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Email - Email queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 send_email

=cut

sig send_email => 'Zaaksysteem::Backend::Object::Queue::Component, ?Zaaksysteem::Backend::Mailer';

sub send_email {
    my $self = shift;
    my $item = shift;
    my $mailer = shift // Zaaksysteem::Backend::Mailer->new;

    my $case = $item->object_data->get_source_object;

    $case->mail_action({
        case => $case,
        action_data => $item->data,
        email_sender => $mailer
    });

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
