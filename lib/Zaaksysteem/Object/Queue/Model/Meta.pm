package Zaaksysteem::Object::Queue::Model::Meta;

use Moose::Role;

use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Meta - Meta queue item handlers

=head1 DESCRIPTION

=head1 METHODS

=head2 run_ordered_item_set

=cut

sig run_ordered_item_set => 'Zaaksysteem::Backend::Object::Queue::Component';

sub run_ordered_item_set {
    my $self = shift;
    my $item = shift;

    try {
        for my $item_id (@{ $item->data->{ item_ids } }) {
            my $sub_item = $self->run_item($item_id);

            if ($sub_item->is_failed) {
                throw('queue/run/failed_item_in_ordered_set', sprintf(
                    'Sub-item failed to run, ordered set run incomplete'
                ));
            }
        }
    } catch {
        for my $item_id (@{ $item->data->{ item_ids } }) {
            $self->mark_item($item_id, 'failed');
        }

        $_->throw if blessed $_;
        die $_;
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
