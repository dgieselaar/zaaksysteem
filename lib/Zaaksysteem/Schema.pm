package Zaaksysteem::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_classes;


# Created by DBIx::Class::Schema::Loader v0.04006 @ 2013-01-10 07:42:00
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:R4RHNOmRFCrCmyTFSe08tA

use Moose;
use Zaaksysteem::Tools;
use File::Spec::Functions qw(catdir);

=head1 NAME

Zaaksysteem::Schema - Schema definition for Zaaksysteem datamodel

=head1 DESCRIPTION

=head1 CONSTANTS

=head2 CLEAR_PER_REQUEST

Definition list for the L</ATTRIBUTES> that should be cleared each request
cycle.

=cut

use constant CLEAR_PER_REQUEST => [qw/
    users
    current_user
    cache
    betrokkene_pager
    catalyst_config
    customer_instance
    current_user_ou_ancestry
    customer_config
    output_path
    storage_path
    tmp_path
/];

=head1 ATTRIBUTES

=head2 current_user_ou_ancestry

Stores a list of groups the L</current_user> is (implicitly via inheritance)
member of.

=cut

has 'current_user_ou_ancestry'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'default'   => sub {
        my $self                = shift;

        return [ map {
            {
                ou_id => $_->id,
                name  => $_->name,
            }
        } @{ $self->current_user->inherited_groups } ];
    }
);

=head2 users

=head2 current_user

Stores a weak reference to the currently logged in user.

=cut

has [qw/users current_user/] => (
    'is'        => 'rw',
    'weak_ref'  => 1,
);

=head2 customer_config

Stores a weak reference to the cutomer config for the current request.

=cut

has customer_config => (
    is       => 'rw',
    lazy     => 1,
    isa      => 'Maybe[HashRef]',
    weak_ref => 1,
    default  => sub {
        my $self = shift;
        if ($self->customer_instance && $self->customer_instance->{start_config}) {
            return $self->customer_instance->{start_config};
        }
        return {};
    },
);

=head2 output_path

=cut

has output_path => (
    is      => 'rw',
    isa     => 'Maybe[Str]',
    lazy    => 1,
    default => sub {
        my $self = shift;
        if ($self->customer_config && $self->customer_config->{basedir}) {
            return $self->customer_config->{basedir};
        }
        return '/var/tmp/zs';
    },
);

=head2 storage_path

=cut

has storage_path => (
    is      => 'rw',
    isa     => 'Maybe[Str]',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return catdir($self->output_path, 'storage');
    },
);

=head2 tmp_patch

=cut

has tmp_path => (
    is      => 'rw',
    isa     => 'Maybe[Str]',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return catdir($self->output_path, 'tmp');
    },
);

=head2 cache

=cut

has [qw/cache/] => (
    'is'        => 'rw',
);

=head2 log

Stores a weak reference to a L<Catalyst::Log> instance.

=cut

has 'log'  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'weak_ref'  => 1,
    'default'   => sub { return Catalyst::Log->new }
);

=head2 betrokkene_pager

=head2 catalyst_config

=head2 customer_instance

=cut

has [qw/betrokkene_pager catalyst_config customer_instance/]  => (
    'is'        => 'rw',
    'lazy'      => 1,
    'weak_ref'  => 1,
    'default'   => sub { return {} }
);

=head1 METHODS

=head2 betrokkene_model

Factory method that instantiates fresh L<Zaaksysteem::Betrokkene> instances.

=cut

sub betrokkene_model {
    my $self            = shift;

    my $dbic            = $self->clone;

    my $stash           = {};

    if ($self->betrokkene_pager) {
        for my $key (keys %{ $self->betrokkene_pager }) {
            $stash->{ $key } = $self->betrokkene_pager->{ $key };
        }
    }

    $dbic->cache($self->cache);

    return Zaaksysteem::Betrokkene->new(
        dbic            => $dbic,
        stash           => $stash,
        config          => $self->catalyst_config,
        customer        => $self->customer_instance,
    );
}

=head2 set_current_user

Installs the provided subject as current_user

=cut

sub set_current_user {
    my $self = shift;
    my $subject = shift;

    $self->default_resultset_attributes->{ current_user } = $subject;
    $self->current_user($subject);

    return $self;
}

=head2 _clear_schema

Cleanup method, see L</CLEAR_PER_REQUEST> constant for a definition of
attributes cleared.

=cut

sub _clear_schema {
    my $self            = shift;

    for my $attr (@{ CLEAR_PER_REQUEST() }) {
        $self->$attr(undef);
    }
}


# Install our own exception action, so when DBIx throws one it's an
# Zaaksysteem::Exceptio::Base instance
__PACKAGE__->exception_action(sub {
    my $err = shift;

    if(blessed $err) {
        $err->throw if $err->can('throw');
        throw('db', $err->as_string, $err) if $err->can('as_string');
        throw('db', '' . $err, $err);
    }
    throw('db', $err);
});

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
