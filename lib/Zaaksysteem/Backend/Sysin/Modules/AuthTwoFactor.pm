package Zaaksysteem::Backend::Sysin::Modules::AuthTwoFactor;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::AuthTwoFactor - Non-government authentication for PIP/Form

=head1 DESCRIPTION

This interface module calls itself C<auth_twofactor>, and handles creation of
accounts, and authentication by means of username, password and an SMS token.

=cut

use Authen::Passphrase;
use Crypt::OpenSSL::Random qw(random_pseudo_bytes);
use DateTime;
use LWP::UserAgent;
use Math::Base36 qw(encode_base36);
use XML::LibXML;
use Zaaksysteem::Tools;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

my $PASSWORD_RESET_MINIMUM_INTERVAL = 300; # 300 seconds = 5 minutes

my $INTERFACE_ID = 'auth_twofactor';

my @INTERFACE_CONFIG_FIELDS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_allow_new_accounts',
        type        => 'checkbox',
        required    => 0,
        label       => 'Account aanmaken toestaan',
        description => '',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_twofactor_type',
        type        => 'select',
        required    => 1,
        label       => 'Tweede authenticatie-factor',
        data => {
            options => [
                { label => "Code via SMS",  value => "sms" },
            ],
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_enable_for',
        type     => 'select',
        required => 1,
        label => 'Gebruik deze authenticatiemethode voor',
        data => {
            options => [
                { label => "Geen",                     value => "" },
                { label => "Personen",                 value => "persons" },
                { label => "Organisaties",             value => "companies" },
                { label => "Personen en organisaties", value => "both" },
            ],
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_login_message',
        type        => 'textarea',
        required    => 0,
        label       => 'Introductietekst bij inloggen',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_registration_message',
        type        => 'textarea',
        required    => 0,
        label       => 'Registratietoelichting',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_password_lost_message',
        type        => 'textarea',
        required    => 0,
        label       => 'Toelichting vergeten wachtwoord',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_password_reset_template',
        type        => 'spot-enlighter',
        required    => 1,
        description => 'Mogelijke magicstrings: [[username]], [[email]], [[mobile]]',
        label       => 'Sjabloon voor wachtwoord-reset-email',
        data => {
            restrict   => 'notification',
            placeholer => 'Type uw zoekterm',
            label      => 'label',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_account_confirmation_template',
        type        => 'spot-enlighter',
        required    => 1,
        description => 'Mogelijke magicstrings: [[username]], [[email]], [[mobile]]',
        label       => 'Sjabloon voor bevestigingsemail',
        data => {
            restrict    => 'notification',
            placeholder => 'Type uw zoekterm',
            label       => 'label',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sms_sender',
        type        => 'text',
        required    => 1,
        label       => 'SMS Afzender',
        description => 'Afzender voor SMS-berichten (maximaal 11 tekens)',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sms_product_token',
        type        => 'text',
        required    => 1,
        label       => 'SMS Product token',
        description => 'Product-token (zoals opgegeven door CM)',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sms_endpoint',
        type        => 'text',
        required    => 1,
        label       => 'SMS Resource URL',
        description => 'URL (zoals opgegeven door CM) die gebruikt wordt om SMS te sturen',
        default     => 'https://sgw01.cm.nl/gateway.ashx',
    ),
);

my %MODULE_SETTINGS = (
    name             => $INTERFACE_ID,
    label            => 'Alternatieve gebruikersauthenticatie voor PIP en form',
    essential        => 0,
    interface_config => \@INTERFACE_CONFIG_FIELDS,
    direction        => 'incoming',
    manual_type      => [],
    is_multiple      => 0,
    is_manual        => 0,
    retry_on_error   => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    trigger_definition => {
        "check_password" => {
            method => "check_password",
            update => 1,
        },
        "change_password" => {
            method => "change_password",
            update => 1,
        },
        "verify_second_factor" => {
            method => "verify_second_factor",
            update => 1,
        },
        "register_account" => {
            method => "register_account",
            update => 1,
        },
        "activate_account" => {
            method => "activate_account",
            update => 1,
        },
        "link_betrokkene_id" => {
            method => "link_betrokkene_id",
            update => 1,
        },
        "lost_password_initialize" => {
            method => "lost_password_initialize",
            update => 1,
        },
        "send_sms" => {
            method => "send_sms",
            update => 1,
        },
    },
);

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig(%MODULE_SETTINGS);
};

=head1 METHODS

=head2 check_password

Check whether the supplied username/password combination is valid.

=cut

sub check_password {
    my ($self, $params, $interface) = @_;
    my $schema = $interface->result_source->schema;

    my $ue = eval {
        $self->_find_userentity($interface->id, $schema, $params->{username});
    };

    if (!$ue || !$ue->active) {
        $self->log->trace("User entity not found or not active");
        return { success => 0, reason => 'twofactor/user_not_found' };
    }

    my $ppr = Authen::Passphrase->from_rfc2307($ue->password);

    if (!$ppr->match($params->{password})) {
        $self->log->trace("Password did not match");
        return { success => 0, reason => 'twofactor/password_incorrect' };
    }
    $self->log->trace("Password matched.");

    my $factor = $self->_generate_second_factor();

    my $betrokkene = $self->_get_betrokkene($ue);

    return {
        success        => 1,
        twofactor_code => $factor,
        betrokkene     => $betrokkene,
        subject        => $ue->subject_id,
    };
}

=head2 verify_second_factor

Verify if the second factor entered by the user is the same as the one stored
in the session.

=cut

sub verify_second_factor {
    my ($self, $params, $interface) = @_;

    # Do a case-insensitive comparison.
    if (uc($params->{stored}) eq uc($params->{entered})) {
        return { success => 1 };
    }

    return { success => 0 };
}

=head2 register_account

Create a new account (subject + userentity) based on the supplied data.

=cut

sub register_account {
    my ($self, $params, $interface) = @_;
    my $schema  = $interface->result_source->schema;

    {
        my $ue = eval {
            $self->_find_userentity($interface->id, $schema, $params->{username});
        };

        if ($ue) {
            $self->log->trace("Username already exists.");
            return {
                success => 0,
                reason  => "twofactor/subject_exists",
            };
        }
    }

    if ($params->{password} ne $params->{password_check}) {
        $self->log->trace("Password incorrect.");
        return {
            success => 0,
            reason  => "twofactor/password_mismatch",
        };
    }

    my $subject = $schema->resultset('Subject')->create(
        {
            subject_type => $params->{subject_type},
            username     => $params->{username},
            properties   => {
                initial_phone_number  => $params->{phone},
                initial_email_address => $params->{email},

                ($params->{subject_type} eq 'person')
                    ? (bsn => $params->{bsn})
                    : (
                        kvknummer        => $params->{kvknummer},
                        vestigingsnummer => $params->{vestigingsnummer},
                    ),
            },
        }
    );
    my $ue = $subject->create_related(
        'user_entities',
        {
            source_interface_id => $interface->id,
            source_identifier   => $params->{username},

            # User entities are created with a "date deleted". When SMS
            # verification is done, the "date deleted" will be removed.
            date_deleted        => DateTime->now(),
        }
    );

    $subject->update_password(
        $ue,
        { password => $params->{password} }
    );

    my $factor = $self->_generate_second_factor();

    $self->log->trace("Account created successfully.");
    return {
        success        => 1,
        twofactor_code => $factor,
        subject        => $subject,
    };
}

=head2 activate_account

Activates a new not yet active account when a correct code (second factor) is
received.

=cut

sub activate_account {
    my ($self, $params, $interface) = @_;

    my $transaction = $interface->process(
        {
            external_transaction_id => 'n/a',
            input_data => "Account activation for $params->{username}",

            processor_params => {
                %$params,
                processor => '_activate_account',
            }
        }
    );

    return $transaction;
}

sub _activate_account {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;
    my $schema      = $interface->result_source->schema;

    my $ue = $self->_find_userentity($interface->id, $schema, $params->{username});

    $ue->update({
        date_deleted => undef
    });

    my $subject = $ue->subject_id;

    my $email_id = $interface->jpath('$.account_confirmation_template.id');
    my $template = $schema->resultset('BibliotheekNotificaties')->find($email_id);

    my $email = $template->send_mail(
        {
            to => $subject->properties->{initial_email_address},
            ztt_context => {
                username => $params->{username},
                email    => $subject->properties->{initial_email_address},
                mobile   => $subject->properties->{initial_phone_number},
            },
        }
    );

    $record->preview_string("Nieuw account '$params->{username}' geactiveerd");
    $record->output($email->as_string);

    return;
}

=head2 lost_password_initialize

Initialize the "lost password" process for a given user.

=cut

sub lost_password_initialize {
    my ($self, $params, $interface) = @_;
    my $schema = $interface->result_source->schema;

    my $ue = eval {
        $self->_find_userentity($interface->id, $schema, $params->{username});
    };
    if (!$ue) {
        $self->log->trace("User entity not found while starting password-reset");
        return {
            success => 0,
            reason  => 'twofactor/user_not_found',
        };
    }

    my $properties = $ue->properties;
    my $last_reset = $properties->{password_reset};
    if (   defined($last_reset)
        && (time - $last_reset < $PASSWORD_RESET_MINIMUM_INTERVAL)
    ) {
        $self->log->trace("Password reset not triggered, minimum time interval not passed");
        return {
            success => 0,
            reason  => 'twofactor/password_reset_interval'
        };
    }

    $properties->{password_reset} = time;
    $ue->properties($properties);
    $ue->update();

    my $factor = $self->_generate_second_factor();

    $self->log->trace("Password reset triggered");
    return {
        success        => 1,
        twofactor_code => $factor,
        betrokkene     => $self->_get_betrokkene($ue),
    };
}

=head2 change_password

Change a user's password. Also does the "are these passwords identical" check
beforehand.

=cut

sub change_password {
    my ($self, $params, $interface) = @_;

    my $transaction = $interface->process(
        {
            external_transaction_id => 'n/a',
            input_data => "Password change for $params->{username}",

            processor_params => {
                %$params,
                processor => '_change_password',
            }
        }
    );

    return $transaction;
}

sub _change_password {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;
    my $schema      = $interface->result_source->schema;

    my $ue = $self->_find_userentity($interface->id, $schema, $params->{username});

    if (   (not exists $params->{password})
        || (not exists $params->{password_check})
        || ($params->{password} ne $params->{password_check})
    ) {
        $self->log->trace("Passwords don't match.");
        throw(
            "twofactor/password_mismatch",
            "Passwords don't match",
            { fatal => 1 },
        );
    }

    my $subject = $ue->subject_id;

    my $email_id = $interface->jpath('$.password_reset_template.id');
    my $template = $schema->resultset('BibliotheekNotificaties')->find($email_id);

    my $email = $template->send_mail(
        {
            to => $subject->properties->{initial_email_address},
            ztt_context => {
                username => $params->{username},
                email    => $subject->properties->{initial_email_address},
                mobile   => $subject->properties->{initial_phone_number},
            },
        }
    );

    $record->preview_string("Wachtwoordwijziging voor $params->{username}");
    $record->output($email->as_string);

    $self->log->trace("Passwords match. Updating to '$params->{password}'");
    $subject->update_password(
        $ue,
        { password => $params->{password} }
    );

    # Remove clear-text password
    delete $params->{password};
    delete $params->{password_check};
    $transaction->processor_params($params);

    return {
        success => 1,
    };
}

sub _get_betrokkene {
    my ($self, $ue) = @_;
    my $subject = $ue->subject_id;

    my $schema = $ue->result_source->schema;

    my %search_options;
    if ($subject->subject_type eq 'person') {
        $search_options{burgerservicenummer} = $subject->properties->{bsn};
    }
    else {
        $search_options{dossiernummer}    = $subject->properties->{kvknummer};
        $search_options{vestigingsnummer} = $subject->properties->{vestigingsnummer};
    }

    my $betrokkene_rs = $schema->betrokkene_model->search(
        {
            type => ($subject->subject_type eq 'person')
                ? 'natuurlijk_persoon'
                : 'bedrijf',
            intern => 0,
        },
        \%search_options,
    );

    if ($betrokkene_rs) {
        return $betrokkene_rs->next;
    }

    return;
}

sub _find_userentity {
    my ($self, $interface_id, $schema, $username) = @_;

    my $ue = $schema->resultset('UserEntity')->search(
        {
            source_identifier   => $username,
            source_interface_id => $interface_id,
        }
    )->first;

    if (!$ue) {
        $self->log->trace("User entity for '$username' not found");
        throw(
            'twofactor/no_such_user',
            "User not found.",
            { fatal => 1 },
        );
    }

    return $ue;
}

sub _generate_second_factor {
    my $self = shift;

    my $prand = random_pseudo_bytes(4);

    my $code = unpack("N", $prand);

    my $factor = encode_base36($code);

    $self->log->trace("Generated second factor: " . $factor);
    return $factor;
}

=head2 send_sms

Send a text message

=cut

# This should be split out to its own interface
sub send_sms {
    my ($self, $params, $interface) = @_;

    my $transaction = $interface->process(
        {
            external_transaction_id => 'n/a',
            input_data => "SMS to $params->{phonenumber}",

            processor_params => {
                %$params,
                processor => '_send_sms',
            }
        }
    );

    return $transaction;
}

sub _send_sms {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    my $phonenr;
    if ($params->{phonenumber} =~ /^06(.*)/) {
        $phonenr = "00316$1";
        $self->log->trace("Sending text message to '$phonenr'.");
    }
    else {
        $self->log->trace("Phone number '$params->{phonenumber}' is not a mobile phone number.");
        throw(
            "twofactor/sms_number_not_supported",
            "Can't send SMS to non-mobile phone number '$params->{phonenumber}'",
            { fatal => 1 }
        );
    }
    my $from = $interface->jpath('$.sms_sender');
    my $token = $interface->jpath('$.sms_product_token');

    my $doc = XML::LibXML->createDocument;

    {
        my %nodes;

        $nodes{root} = $doc->createElement("MESSAGES");
        $doc->setDocumentElement($nodes{root});

        $nodes{authentication}  = $nodes{root}->addNewChild(undef, "AUTHENTICATION");
        $nodes{token}           = $nodes{authentication}->addNewChild(undef, "PRODUCTTOKEN");

        $nodes{msg} = $nodes{root}->addNewChild(undef, "MSG");
        $nodes{from}      = $nodes{msg}->addNewChild(undef, "FROM");
        $nodes{to}        = $nodes{msg}->addNewChild(undef, "TO");
        $nodes{body}      = $nodes{msg}->addNewChild(undef, "BODY");
        $nodes{reference} = $nodes{msg}->addNewChild(undef, "REFERENCE");

        $nodes{token}->appendTextNode($token);
        $nodes{from}->appendTextNode(substr($from, 0, 11));
        $nodes{to}->appendTextNode($phonenr);
        $nodes{body}->appendTextNode(substr($params->{message}, 0, 160));
        $nodes{reference}->appendTextNode($transaction->id);
    }

    $self->log->trace("SMS XML:\n" . $doc->toString(1));

    my $sms_endpoint = $interface->jpath('$.sms_endpoint');

    my $ua = LWP::UserAgent->new;
    $ua->timeout(10);
    $ua->ssl_opts(
        verify_hostname => 1,
    );

    my $req = HTTP::Request->new(
        'POST',
        $sms_endpoint,
        [ "Content-Type" => "application/xml" ],
        $doc->toString(1),
    );
    my $res = $ua->request($req);

    $record->input($req->dump(maxlength => 0));
    $record->output($res->dump(maxlength => 0));

    if (!$res->is_success) {
        throw(
            "twofactor/sms_sending_failed",
            $res->status_line,
            { fatal => 1 }, # Don't retry SMS sending. That's expensive.
        );
    }
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
