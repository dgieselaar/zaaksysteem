package Zaaksysteem::Backend::Sysin::Modules::EmailConfiguratie;
use Moose;

use Mail::Track;
use Try::Tiny;

use Zaaksysteem::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::Email;
use File::Slurp qw(read_file);

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Roles::Email
/;


=head1 INTERFACE CONSTANTS

=head2 INTERFACE_ID

=head2 INTERFACE_CONFIG_FIELDS

=head2 MODULE_SETTINGS

=cut

=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=cut

use constant INTERFACE_ID               => 'emailconfiguration';
use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_user',
        type        => 'text',
        label       => 'Verzendadres',
        required    => 1,
        description => 'E-mailadres waar de mail vanaf verstuurd moet worden',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sender_name',
        type        => 'text',
        label       => 'Verzendnaam',
        required    => 0,
        description => 'De naam van de verzender, dit wordt in combinatie met het e-mailadres de afzender: "Directie ZS <info@zaaksysteem.nl>"',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_subject',
        type        => 'text',
        label       => 'Onderwerpprefix',
        required    => 1,
        description => 'Een prefix voor het onderwerp welke aan mails wordt toegevoegd indien ze afkomstig zijn vanuit Zaaksysteem. Dit wordt gebruikt om een uniek identifieerbaar onderwerp te krijgen waardoor mailuitwisseling tussen o.a. aanvrager en behandelaar mogelijk is.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_max_size',
        type        => 'text',
        label       => 'Emailgrootte',
        required    => 1,
        default     => '10',
        description => 'De grootte van de e-mails in MB die verstuurd mogen worden. Mails die groter zijn dan gedefinieerd worden niet verstuurd.',
    ),
];

use constant MODULE_SETTINGS            => {
    name                            => INTERFACE_ID,
    label                           => 'Uitgaande mailconfiguratie',
    interface_config                => INTERFACE_CONFIG_FIELDS,
    direction                       => 'incoming',
    manual_type                     => ['text'],
    is_multiple                     => 0,
    is_manual                       => 0,
    allow_multiple_configurations   => 0,
    is_casetype_interface           => 0,
    has_attributes                  => 0,
    attribute_list                  => [],
    retry_on_error                  => 1,
    trigger_definition  => {
        process_mail   => {
            method  => 'process_mail',
        },
    },
};

=head2 BUILDARGS

Settings for module, see

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() } );
};

=head1 ATTRIBUTES

=head2 interface

=head2 exception

=head2 schema

=cut

has interface => (is => 'rw');
has exception => (is => 'rw');
has schema => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return if !$self->interface;
        return $self->interface->result_source->schema;
    },
    weak_ref => 1,
);

=head1 METHODS

=head2 process_mail

=cut

sub process_mail {
    my ($self, $params, $interface) = @_;

    $self->interface($interface);

    my $transaction = $interface->process({
            external_transaction_id => 'unknown',
            input_data              => 'mail',
            processor_params        => {
                processor => '_process_mail',
                %$params,
            },
        },
    );

    if ($self->exception) {
        if (eval { $self->exception->isa('Throwable::Error') } ) {
            $self->exception->throw();
        }
        else {
            throw('sysin/emailconfiguration/process_mail', $self->exception);
        }
    }
    return $transaction;
}

sub _process_mail {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    $self->interface($interface);

    try {

        my $config = $interface->get_interface_config;
        my $regexp = qr/(\d+-[a-z0-9]{6})/;

        my $mt = Mail::Track->new(
            subject_prefix_name  => $config->{subject},
            identifier_regex     => qr/$regexp/,
        );

        my $file = $self->assert_file_from_id($interface, $params->{message});
        my $path = $self->assert_path($file);
        my $mime = read_file($path);
        my $message = $mt->parse($mime);

        if (!$message->identifier) {
            throw(
                "sysin/emailconfiguration/case_id/not_found",
                "Email-onderwerp: Geen zaak-identifier gevonden",
            );
        }
        my ($mid, $uuid) = split(/-/, $message->identifier);
        my $case = $self->schema->resultset('Zaak')->find($mid);

        if (!$case) {
            throw(
                "sysin/emailconfiguration/case/not_found",
                "Email-onderwerp: Zaak met ID '$mid' niet gevonden",
            );
        }

        if($uuid ne substr($case->object_data->uuid, -6)) {
            # Eigenlijk moeten we een mail terugsturen
            throw(
                "sysin/emailconfiguration/uuid/invalid",
                sprintf(
                    "Email-onderwerp: Zaak ID '%s' en UUID '%s' horen niet bij elkaar",
                    $mid,
                    $uuid,
                ),
            );
        }

        my $mail = Zaaksysteem::Email->new(
            message => $message,
            schema  => $self->schema
        );

        $mail->add_to_case($case);

        $file->delete();
        $file->filestore->delete();

        my $msg = sprintf("Email van '%s' naar '%s' met onderwerp '%s' is verwerkt", $message->from, $message->to, $message->subject);
        $record->preview_string(substr($msg, 0, 200));
        $record->output($msg);
    }
    catch {
        if (eval {$_->isa('Throwable::Error')}) {
            my $err = $_->as_string;
            $record->output($err);
            $record->preview_string($err);
            $self->exception($_);
        }
        # ClamAv::Error::Client errors
        elsif (eval {$_->isa('Error::Simple')}) {
            my $err = $_->stringify;
            $record->output($err);
            $record->preview_string($err);
            $self->exception($err);
        }
        else {
            $record->output($_);
            $record->preview_string($_);
            $self->exception($_);
        }
        die $_;
    };
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
