package Zaaksysteem::Backend::Sysin::Modules::STUFZKNClient;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

use Encode qw(encode_utf8);
use LWP::UserAgent;
use XML::LibXML;
use XML::LibXML::XPathContext;
use Zaaksysteem::Tools;
use Zaaksysteem::Object::Model;
use Zaaksysteem::SOAP::Client;
use Zaaksysteem::StUF::0310::Processor;
use Zaaksysteem::XML::Compile;
use Zaaksysteem::ZAPI::Form::Field;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::STUFZKN - STUF DMS/STUF ZKN interface

=head1 DESCRIPTION

Interface module that implements the SOAP calls described in the STUF-DMS/STUF-ZKN.

This module implements the L<Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric>
role, making the STUF-ZKN calls available through a SOAP interface.

=cut

my $INTERFACE_ID = 'stuf_zkn_client';

my @INTERFACE_CONFIG_FIELDS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_client_cert',
        type        => 'file',
        label       => 'Client-certificaat + key',
        description => 'Upload hier het certificaat dat Zaaksysteem zal gebruiken voor StUF-ZKN-verbindingen.',
        required    => 0,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_ca_cert',
        type        => 'file',
        label       => 'Certificaat (CA certificaten)',
        description => 'Upload hier de CA-certificaat-keten die Zaaksysteem gebruikt om te controleren of de endpoint URL is wie hij zegt te zijn. Als hier niets wordt geupload, wordt de ingebouwde set CAs gebruikt.',
        required    => 0,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_endpoint',
        type        => 'text',
        label       => 'Endpoint URL',
        description => 'URL waarmee Zaaksysteem verbinding maakt om de StUF-ZKN SOAP call te doen',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_zender_applicatie',
        type        => 'text',
        label       => 'Applicatienaam van verzender (Zaaksysteem)',
        required    => 1,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_ontvanger_applicatie',
        type        => 'text',
        label       => 'Applicatienaam van ontvanger',
        required    => 1,
    ),
);

my %MODULE_SETTINGS = (
    name                          => $INTERFACE_ID,
    label                         => 'StUF-ZKN-Client',
    interface_config              => \@INTERFACE_CONFIG_FIELDS,
    direction                     => 'outgoing',
    manual_type                   => ['text'],
    is_multiple                   => 1,
    is_manual                     => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    attribute_list                => [],
    retry_on_error                => 0,
    trigger_definition            => {
        PostStatusUpdate => {
            method => 'send_case_create',
            update => 1,
        }
    },
    test_interface => 1,
    test_definition => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de verbinding
            van uw profiel.
        },
        tests => [
            {
                id          => 1,
                label       => 'Test verbinding',
                name        => 'connection_test',
                method      => 'test_stufzkn_client_connection',
                description => 'Test verbinding naar StUF-ZKN endpoint',
            }
        ],
    },
);

=head2 BUILDARGS

Configures this interface module (configuration form fields, etc.).

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%MODULE_SETTINGS);
};

=head2 _build_user_agent

Create an L<LWP::UserAgent> instance to use when making SOAP calls.

=cut

sub _build_user_agent {
    my $self = shift;
    my ($interface) = @_;

    my $agent = LWP::UserAgent->new(
        agent   => sprintf('Zaaksysteem/%s', $Zaaksysteem::VERSION),
        timeout => 30,
        ssl_opts => {
            verify_hostname => 1,
            keep_alive      => 1,
            $self->_get_certificates(),
        },
    );

    return $agent;
}

sub _get_certificates {
    my $self = shift;
    my ($interface) = @_;

    my $schema = $interface->result_source->schema;

    my ($ca_file, $client_file);
    if ($interface->jpath('$.ca_cert[0].id')) {
        $ca_file = $schema->resultset('Filestore')->find($interface->jpath('$.ca_cert[0].id'))->get_path;
    }

    if ($interface->jpath('$.client_cert[0].id')) {
        $client_file = $schema->resultset('Filestore')->find($interface->jpath('$.client_cert[0].id'))->get_path;
    }

    return (
        $ca_file ? (SSL_ca_file => $ca_file) : (SSL_ca_path => '/etc/ssl/certs'),
        $client_file ? (
            SSL_cert_file => $client_file,
            SSL_key_file  => $client_file,
        ) : ()
    );
}

=head2 send_case_create

Send a "zakLk01" message with mutation type "T" ("Toevoegen") on the configured
interface.

=cut

define_profile send_case_create => (
    required => [qw/
        kenmerken
        case_id
    /],
    optional => [qw/
        statusCode
        test_responses
        statusText
        toelichting
    /],
);

sub send_case_create {
    my ($self, $params, $interface) = @_;

    $interface->process({
        external_transaction_id => 'unknown',
        input_data => 'xml',
        processor_params => {
            %$params,
            processor => '_process_PostStatusUpdate',
        },
    });

    return;
}

sub _process_PostStatusUpdate {
    my $self = shift;
    my $record = shift;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    my %certs = $self->_get_certificates($interface);

    my $soap_client = Zaaksysteem::SOAP::Client->new(
        endpoint => $interface->jpath('$.endpoint'),
        ua => LWP::UserAgent->new(
            agent   => "Zaaksysteem/" . $Zaaksysteem::VERSION,
            timeout => 60,
            ssl_opts => {
                verify_hostname => 1,
                %certs,
            },
            protocols_allowed => [qw(https)],
        ),
    );

    my $schema = $interface->result_source->schema;
    my $case = $schema->resultset('Zaak')->search(
        {
            'me.id' => $params->{case_id},
        },
        {
            prefetch => ['zaaktype_node_id']
        }
    )->first;

    my %args;
    $args{stuurgegevens}{berichtcode}    = 'Lk01';
    $args{stuurgegevens}{entiteittype}   = 'ZAK';
    $args{stuurgegevens}{zender}{applicatie}    = $interface->jpath('$.zender_applicatie');
    $args{stuurgegevens}{ontvanger}{applicatie} = $interface->jpath('$.ontvanger_applicatie');
    $args{stuurgegevens}{referentienummer}      = $record->id,
    $args{stuurgegevens}{tijdstipBericht}       = DateTime->now()->strftime('%Y%m%d%H%M%S%03N');

    $args{parameters}{mutatiesoort}      = 'T';
    $args{parameters}{indicatorOvername} = 'V';

    my $processor = Zaaksysteem::StUF::0310::Processor->new(
        record => $record,
        schema => $schema,
        betrokkene_model => $schema->betrokkene_model,
        object_model => Zaaksysteem::Object::Model->new(
            schema => $schema,
        ),
    );

    $args{object} = [
        $processor->format_case($case)
    ];

    Zaaksysteem::XML::Compile->xml_compile->add_class('Zaaksysteem::XML::Generator::StUF0310');
    my $stuf0310 = Zaaksysteem::XML::Compile->xml_compile->stuf0310;
    my $xml = $stuf0310->write_case('reader', \%args);

    $record->preview_string("Zaak " . $params->{case_id});

    my ($request_data, $response_data);
    my $soap_data = try {
        $soap_client->call(
            'http://www.egem.nl/StUF/sector/zkn/0310/creeerZaak_Lk01',
            $xml,
        );
    }
    catch {
        $self->log->error("Exception during StUF-ZKN SOAP call: $_");

        $response_data = $_;
        $record->output( encode_utf8($_) );
        $transaction->error_fatal(1);
        $transaction->error_count(1);
        $transaction->update();
    };

    if ($soap_data) {
        $request_data  = $soap_data->{request}->decoded_content;
        $response_data = $soap_data->{response}->decoded_content;

        $transaction->input_data(
            $record->input(encode_utf8($request_data))
        );
        $record->output( encode_utf8($response_data) );

        if (!$soap_data->{response}->is_success) {
            $self->process_stash->{error_fatal} = 1;
            $record->is_error(1);
            $transaction->error_message("HTTP request was not successful (but " . $soap_data->{response}->status_line . ")");
            $transaction->update();
        }
        
        if (!$self->_is_bv03($response_data)) {
            $record->is_error(1);
            $transaction->error_message("Response XML is not a Bv03Bericht (or not XML at all)");
            $transaction->update();
        }
    }

    $case->trigger_logging(
        'case/send_external_system_message',
        {
            component => 'case',
            component_id  => $params->{case_id},
            zaak_id       => $params->{case_id},
            data          => {
                destination => 'StUF-ZKN',
                input  => $request_data,
                output => $response_data,
                error  => $record->is_error,
            }
        }
    );
}

sub _is_bv03 {
    my $self = shift;
    my ($data) = @_;

    my $failed;
    my $doc = try {
        XML::LibXML->load_xml(string => $data);
    } catch {
        $self->log->error("Error parsing XML: $_");
        $failed = 1;
    };

    return if $failed;

    my $xc = XML::LibXML::XPathContext->new($doc);
    $xc->registerNs('SOAP',  'http://schemas.xmlsoap.org/soap/envelope/');
    $xc->registerNs('StUF',  'http://www.egem.nl/StUF/StUF0301');
    $xc->registerNs('ZKN',   'http://www.egem.nl/StUF/sector/zkn/0310');
    $xc->registerNs('BG',    'http://www.egem.nl/StUF/sector/bg/0310');
    $xc->registerNs('xlink', 'http://www.w3.org/1999/xlink');

    # Get the children of the SOAP:Body tag
    my @nodes = $xc->findnodes('//SOAP:Body/*[1]');
    if (@nodes) {
        my $name = sprintf('{%s}%s', $nodes[0]->namespaceURI, $nodes[0]->localname);

        if ($name eq "{http://www.egem.nl/StUF/StUF0301}Bv03Bericht") {
            $self->log->debug("Received XML was a StUF Bv03 message");
            return 1;
        }
        $self->log->error("Received XML was not a StUF Bv03 message (but: '$name')");
    }
    else {
        $self->log->error(sprintf(
            "Received XML was not a SOAP message, but {%s}%s",
            $doc->documentElement->namespaceURI,
            $doc->documentElement->localname,
        ));
    }

    return;
}

=head2 test_stufzkn_client_connection

Perform a simple connection test to the configured endpoint, using the
configured certificates (if any).

=cut

sub test_stufzkn_client_connection {
    my $self = shift;
    my ($interface) = @_;

    my %certs = $self->_get_certificates($interface);

    $self->test_host_port_ssl(
        $interface->jpath('$.endpoint'),

        # If the CA file is undefined, the system store is used.
        $certs{SSL_ca_file} ? $certs{SSL_ca_file} : undef,

        # test_host_port wants a certificate and a key.
        # We put them in the same file.
        $certs{SSL_cert_file} ? ($certs{SSL_cert_file}, $certs{SSL_cert_file}) : (),
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
