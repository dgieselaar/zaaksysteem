package Zaaksysteem::Backend::Sysin::Roles::Zorginstituut;
use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Sysin::Roles::Zorginstituut - A role for Zorginstituut modules

=head1 DESCRIPTION

This role some, minimal, housekeeping for Zorginstituut like modules.

=cut

use Moose::Util::TypeConstraints;
class_type('Zaaksysteem::Backend::Sysin::iWMO::Model');
class_type('Zaaksysteem::Backend::Sysin::iJW::Model');

use Zaaksysteem::Tools;
use Zaaksysteem::Constants;
use Zaaksysteem::XML::Zorginstituut::WMO302;
use Zaaksysteem::XML::Zorginstituut::JW302;

=head1 ATTRIBUTES

=head2 instance

A required instance of a model from one of the following objects:

=over

=item L<Zaaksysteem::Backend::Sysin::iWMO::Model>

=item L<Zaaksysteem::Backend::Sysin::iJW::Model>

=back

=cut

has instance => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Backend::Sysin::iWMO::Model|Zaaksysteem::Backend::Sysin::iJW::Model',
    required => 1,
);

has converter => (
    is        => 'ro',
    isa       => 'Zaaksysteem::Backend::Sysin::Zorginstituut::Converter',
    predicate => 'has_converter',
);

has synchronous => (
    is        => 'ro',
    isa       => 'Bool',
    default   => 0,
);

has berichtcode => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has berichttype => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has berichtidentificatienummer => (
    is  => 'rw',
    isa => 'Int',
);

=head1 METHODS

=head2 send_301_message

Send a 301 message from the case

=head3 ARGUMENTS

=over

=item * case

A L<Zaaksysteem::Schema::Zaak> object, required.

=item * data

Override case data, should only be used for testing!

=back

=head3 RETURNS

This functions returns the following data structure

    {
        case     => '42',
        message  => 'json encoded message',
    };

=cut

define_profile send_301_message => (
    required => {
        case        => 'Zaaksysteem::Schema::Zaak',
        berichtcode => 'Int',
    },
    optional => { data => 'Str', },
);

sub send_301_message {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $data
        = $self->interface->get_mapped_attributes_from_case($opts->{case});

    my $res = {
        case => $opts->{case}->id,
        type => $self->berichttype,
        code => $self->berichtcode,
    };

    if ($data->{beschikkingsnummer} && $data->{beschikkingsnummer} ne $opts->{case}->id) {
        my $case = $self->_assert_case($data->{beschikkingsnummer});

        my $d2 = $self->interface->get_mapped_attributes_from_case($case);

        # copy all enddates
        $d2->{beschikkingseinddatum}              = $data->{stop_beschikkingseinddatum};
        $d2->{toegewezen_product_einddatum}       = $data->{stop_toegewezen_product_einddatum};
        $d2->{toegewezen_product_redenintrekking} = $data->{stop_toegewezen_product_redenintrekking};

        foreach (qw(1 2 3)) {
            # Assume that when there is no date set, we don't have a product
            next unless exists $data->{"toegewezen_product_ingangsdatum_$_"};
            $d2->{"toegewezen_product_einddatum_$_"}
                = $data->{stop_toegewezen_product_einddatum_1};
            $d2->{"toegewezen_product_redenintrekking_$_"}
                = $data->{stop_toegewezen_product_redenintrekking};
        }

        my $xml = $self->instance->encode_301_message(
                %$opts,
                data => $d2,
                case => $case
        );
        $res->{stop}{request}{xml} = $xml;

        $xml = $self->_convert_via_zorginstituut($xml);

        my $response = $self->_send_301_message(
            endpoint => $self->endpoints->{message},
            xml      => $xml,
        );

        $res->{stop}{response} = $self->validate_synchronous($opts->{case}, $response);
    }

    ## Filled in only when we have a start, otherwise it is stop only
    if ($data->{aanbieder}) {
        my $xml = $self->instance->encode_301_message(%$opts, data => $data);

        $res->{start}{request}{xml} = $xml;

        $xml = $self->_convert_via_zorginstituut($xml);

        my $response = $self->_send_301_message(
            endpoint => $self->endpoints->{message},
            xml      => $xml,
        );

        $res->{start}{response} = $self->validate_synchronous($opts->{case}, $response);
    }

    if (!$res->{start} && !$res->{stop}) {
        throw("zorginstituut/301/no_message", "No message could be sent, no beschikking or aanbieder found");
    }

    return $res;

}

=head2 validate_synchronous

Validate and log a message if the interface is synchronous.

=cut

sub validate_synchronous {
    my ($self, $case, $xml) = @_;
    if ($self->synchronous) {
        my $valid = $self->validate_302_message($xml);
        $valid->{msg} = $self->log_302_to_case($case, $valid);
        return $valid;
    }
    return { xml => $xml };
}

=head2 validate_302_message

Validate a message.

=cut

sub validate_302_message {
    my ($self, $xml) = @_;

    if ($self->has_converter && $self->converter) {
        $xml = $self->converter->to_xml($xml);
    }

    my $type = $self->berichttype;
    my $validator;

    if ($type eq 'wmo301') {
        $validator = Zaaksysteem::XML::Zorginstituut::WMO302->new(xml => $xml);
    }
    elsif ($type eq 'jw301' ) {
        $validator = Zaaksysteem::XML::Zorginstituut::JW302->new(xml => $xml);
    }

    my $valid = $validator->is_valid();

    $self->berichtidentificatienummer($validator->berichtidentificatienummer);

    return {
        valid  => $valid,
        errors => !$valid ? $validator->show_errors() : undef,
        xml    => $xml,
        type   => $type,
    };
}

=head2 log_302_to_case

Trigger logging and create a user notification for the 302 message.

=cut

sub log_302_to_case {
    my ($self, $case, $data) = @_;

    my $event = $case->trigger_logging('case/update/zorginstituut',
        { component => LOGGING_COMPONENT_ZAAK, data => $data });

    my $msg = sprintf("%s antwoordbericht is ontvangen voor zaak %d", uc($self->berichttype),  $case->id);

    $case->create_message_for_behandelaar(
        event_type => 'case/update/zorginstituut',
        message    => $msg,
        log        => $event,
    );
    return $msg;
}

sub _convert_via_zorginstituut {
    my ($self, $xml) = @_;
    if ($self->has_converter && $self->converter) {
        return $self->converter->to_ascii($xml);
    }
    return $xml;
}

=head2 _send_301_message

You want to override this function if the API is different.

=cut

define_profile _send_301_message => (
    required => {
        endpoint => 'Str',
        xml      => 'Str',
    },
);

sub _send_301_message {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    return $self->call(
        endpoint => $opts->{endpoint},
        message  => $opts->{xml},
    );
}

sub _assert_case {
    my ($self, $id) = @_;
    my $case = $self->schema->resultset('Zaak')->find($id);
    return $case if $case;

    throw("Zorginstituut/case/not_found", "Unable to find case by number $id");
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
