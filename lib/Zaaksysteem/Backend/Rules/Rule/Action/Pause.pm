package Zaaksysteem::Backend::Rules::Rule::Action::Pause;

use Moose;
use Zaaksysteem::Tools;

with 'Zaaksysteem::Backend::Rules::Rule::Action';

=head1 NAME

Z::B::Rules::Rule::Action::Pause - This rule will pause an application.

=head1 SYNOPSIS

=head1 DESCRIPTION

This specific action manage pause actions

=head1 ATTRIBUTES

=head2 copy_attributes

=cut

has 'copy_attributes'    => (
    is      => 'rw',
    isa     => 'Bool',
);


=head2 message

=cut

has 'message'             => (
    is          => 'rw',
    isa         => 'Str',
    required    => 1,
);

=head2 want_start_case

Whether the "startzaak" checkbox for this rule action is checked or not.

=cut

has 'want_start_case' => (
    is       => 'rw',
    isa      => 'Bool',
    required => 0,
    default  => 0,
);

=head2 start_case

=cut

has 'start_case'             => (
    is          => 'rw',
    isa         => 'Maybe[HashRef]',
    predicate   => 'has_start_case',
    lazy        => 1,
    default     => sub {
        my $self        = shift;

        return unless $self->want_start_case;

        unless ($self->casetype_id) {
            warn('Need at least a zaaktype to get start_case');
            return;
        }

        return {
            prefill     => {
                aanvrager_type  => $self->requestor_type,
                zaaktype        => {
                    titel           => $self->casetype_title,
                    zaaktype_id     => $self->casetype_id,
                }
            }
        };
    }
);

has 'casetype_id'       => (
    'is'        => 'rw',
    'isa'       => 'Int',
);


has 'casetype_title'       => (
    'is'        => 'rw',
    'isa'       => 'Str',
);

has 'requestor_type'       => (
    'is'        => 'rw',
    'isa'       => 'Str',
);


=head2 _data_attributes

isa: Array

List of attributes to show in data

=cut

has '_data_attributes' => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub {
        my $self            = shift;

        my @keys            = qw/copy_attributes message/;

        if ($self->casetype_id) {
            push(@keys, 'start_case');
        }

        return \@keys;
    }
);

sub integrity_verified { return 1; }

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 integrity_verified

TODO: Fix the POD

=cut

