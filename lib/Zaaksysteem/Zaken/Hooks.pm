package Zaaksysteem::Zaken::Hooks;

use Moose::Role;

with qw/
    Zaaksysteem::Zaken::Hooks::Sysin
/;

=head1 HOOKS

=head2 hook_case_register

Triggers on a case register

=cut

after '_bootstrap'  => sub {
    my $self            = shift;

    $self->hook_case_register();
};

sub hook_case_register {}

=head2 hook_case_advance

Triggers after a case advanced

=cut

sub hook_case_advance {}

after 'advance'     => sub {
    my $self            = shift;
};

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

