package Zaaksysteem::Zaken::ResultSetZaakKenmerk;
use Moose;

use Data::Dumper;
use Params::Profile;

use Zaaksysteem::Constants;
use Zaaksysteem::Tools;

use Clone qw/clone/;
use List::Util;
use Time::HiRes qw/tv_interval gettimeofday/;

use DateTime::Format::Strptime;
use DateTime::Format::DateParse;

extends 'DBIx::Class::ResultSet';

with qw(MooseX::Log::Log4perl);

{
    Params::Profile->register_profile(
        method  => 'create_kenmerk',
        profile => {
            required => [ qw/zaak_id bibliotheek_kenmerken_id values/],
        }
    );

    my @numeric_types = qw(numeric valuta valutain valutaex valutain6 valutaex6 valutain21 valutaex21);

    sub create_kenmerk {
        my ($self, $params) = @_;

        my $schema = $self->result_source->schema;
        my $bibliotheek_kenmerk = $schema->resultset('BibliotheekKenmerken')->find($params->{bibliotheek_kenmerken_id});
        my $value_type = $bibliotheek_kenmerk->value_type;

        my $values = $params->{values};

        # Pre-process the numeric types to ensure correct formatting of decimal
        # separator and thousands separator.
        if ( List::Util::first { $_ eq $value_type } @numeric_types ) {
            $values = $bibliotheek_kenmerk->filter( $params->{values} );
        }

        $values = UNIVERSAL::isa($values, 'ARRAY') ? $values : [$values];

        foreach my $value (@{ $values }) {
            next unless (length($value) && !ref($value));

            my $row = $self->create({
                zaak_id                     => $params->{zaak_id},
                bibliotheek_kenmerken_id    => $params->{bibliotheek_kenmerken_id},
                value                       => $value
            });
            $row->set_value($value);
        }
    }
}

sub log_field_update {
    my ($self, $params) = @_;

    my $values = $params->{values} || [];
    my $value_string = join(", ", grep { defined($_) } @$values);

    return unless defined($value_string);

    # Convert nummeraanduiding to human readable.
    if ($value_string =~ /^(nummeraanduiding|openbareruimte|woonplaats)-\d+.*$/) {
        $value_string = $self->get_bag_hr(@$values);
    }

    my $match_date = qr[\d{4}\-\d{2}\-\d{2}T\d{2}:\d{2}:\d{2}(?:Z|\+\d{4})];

    if ($value_string =~ m[^($match_date);($match_date);\d+$]) {
        my $start = DateTime::Format::DateParse->parse_datetime($1);
        my $end   = DateTime::Format::DateParse->parse_datetime($2);

        my $fmt = DateTime::Format::Strptime->new(
            pattern => '%F %R'
        );

        $value_string = sprintf(
            '%s - %s',
            $fmt->format_datetime($start),
            $fmt->format_datetime($end)
        );
    }

    my $data = {
        case_id         => $params->{zaak}->id,
        attribute_id    => $params->{bibliotheek_kenmerken_id},
        attribute_value => $value_string
    };

    $self->log->debug("Find recent");

    my $event = do {
        local $data->{attribute_value} = '%';

        my $logging = $self->result_source->schema->resultset('Logging');

        $logging->find_recent({
            data => $data,
            event_type => 'case/attribute/update'
        });
    };

    if ($event) {
        $self->log->debug("Found existing log event to update: " . $event->id);
        $event->data($data);
        $event->restricted(1) if ($params->{zaak}->confidentiality ne 'public');
        $event->update;
    }
    else {
        $self->log->debug("Did not find event");
        $event = $params->{zaak}->trigger_logging(
            'case/attribute/update',
            {
                component => 'kenmerk',
                data      => $data,
            }
        );
        $self->log->debug(
            sprintf("Created event '%s' of type '%s'", $event->id, ref($event))
        );

    }
    return $event;
}




{
    Params::Profile->register_profile(
        method  => 'replace_kenmerk',
        profile => 'Zaaksysteem::Zaken::ResultSetZaakKenmerk::create_kenmerk',
    );

    sub replace_kenmerk {
        my ($self, $params) = @_;

        eval {
            $self->result_source->schema->txn_do(sub {

                # Remove existing values for this bibliotheek_kenmerken_id
                $self->result_source->resultset->search({
                    zaak_id                     => $params->{zaak_id},
                    bibliotheek_kenmerken_id    => $params->{bibliotheek_kenmerken_id},
                })->delete;

                # Re-create this kenmerk with new values
                $self->create_kenmerk({
                    zaak_id                     => $params->{zaak_id},
                    bibliotheek_kenmerken_id    => $params->{bibliotheek_kenmerken_id},
                    values                      => $params->{values}
                });
            });
        };

        if ($@) {
            warn('Arguments: ' . Dumper($params));
            die('Replace kenmerk failed: ' . $@)
        }

        return 1;
    }
}




{
    Params::Profile->register_profile(
        method  => 'get',
        profile => {
            required => [ qw/bibliotheek_kenmerken_id/ ]
        }
    );


    sub get {
        my ($self, $params) = @_;

        my $dv = Params::Profile->check(params  => $params);
        die "invalid options for get" unless $dv->success;

        my $bibliotheek_kenmerken_id = $params->{bibliotheek_kenmerken_id};

        my $kenmerken   = $self->search(
            {
                bibliotheek_kenmerken_id => $bibliotheek_kenmerken_id
            },
            {
                prefetch        => [
                    'bibliotheek_kenmerken_id'
                ],
            }
        );

        return $kenmerken->first();
    }
}



Params::Profile->register_profile(
    method  => 'update_field',
    profile => {
        required => [ qw/bibliotheek_kenmerken_id zaak/ ],
        optional => [ qw/new_values/ ]
    }
);

sub update_field {
    my ($self, $opts) = @_;

    my $dv = Params::Profile->check(params  => $opts);
    die "invalid options for update_field" unless $dv->success;

    my $bibliotheek_kenmerken_id    = $dv->valid('bibliotheek_kenmerken_id');
    my $new_values                  = $dv->valid('new_values');
    my $zaak_id                     = $dv->valid('zaak')->id;

    my ($removed,$race, $raceprotector) = (0,1,0);
    my ($errormsg);

    ### Race condition security, prevent multiple updates;
    while ($race && $raceprotector < 5) {
        $raceprotector++;

        my $params = {
            bibliotheek_kenmerken_id    => $bibliotheek_kenmerken_id,
            zaak_id                     => $zaak_id,
        };

        eval {
            $self->result_source->schema->txn_do(sub {
                my $attributes = $self->result_source->resultset->search($params);
                $removed = $attributes->count;

                if($removed) {
                    $attributes->delete;
                };

                $params->{values} = $new_values;

                if(defined $new_values)  {
                    # Re-create this kenmerk with new values
                    $self->create_kenmerk($params);
                }

                my $log_params = {
                    %$params,
                };
                delete $log_params->{zaak_id};
                $log_params->{zaak} = $dv->valid('zaak');

                $self->log_field_update($log_params);
            });
        };

        ### RACE PROTECTION CODE
        ### The code below checks the amount of kenmerken which would be added
        ### as a result of the above code. Another request could in theory and,
        ### unfortunatly, already call another update_field which results
        ### in duplicated entries.
        ###
        ### See: https://github.com/mintlab/Zaaksysteem/issues/1833
        ### Only testable on more or less slow connections, like test-e
        ###
        ### Improvement hint: do not use count from $params->{values}, but from
        ### a counter of the real rows above.
        ###
        ### - michiel
        if ($@) {
            $errormsg = $@;
            if ($errormsg =~ /ERROR:  deadlock detected/) {
                ### Try again
                warn('Deadlock, try again');
                next;
            }
        } else {
            $errormsg = undef;
        }

        {
            my %check_params = %{ $params };
            delete($check_params{values});

            $params->{values} = [ $params->{values} ]
                unless UNIVERSAL::isa($params->{values}, 'ARRAY');

            my $kenmerk_count = $self->result_source->resultset->search(\%check_params);

            if ($kenmerk_count <= scalar @{ $params->{values}}) {
                $race=0;
            } else {
                warn('RACE CONDITION, TRY AGAIN');
            }
        }
    }

    if ($errormsg) {
        throw('rs/case.attribute/update_field', "Could not update attribute " . $errormsg);
    }


    return $removed;
}

=head2 update_fields_authorized

    my $success = $zaak->zaak_kenmerken->update_fields_authorized(
        {
            new_values  => {
                text_value     => 'New value',
                radio_beer     => 'Heineken'
            },
            zaak        => $schema->resultset('Zaak')->find(44),
        }
    );

Will only update values when user has proper rights to edit this case.

=cut

define_profile update_fields_authorized => (
    required => {
        new_values => 'HashRef',
        zaak => 'Zaaksysteem::Zaken::ComponentZaak',
    },
    optional => {
        user => 'Any',
        ignore_log_update => 'Bool'
    },
);

sub update_fields_authorized {
    my $self        = shift;
    my $params      = assert_profile(shift)->valid;
    my $new_values  = $params->{ new_values };
    my $zaak        = $params->{ zaak };
    my $user        = ($params->{ user } || $self->result_source->schema->current_user);

    my @groups      = @{ $user->primary_groups };
    my @roles       = @{ $user->roles };

    ### 1: Retrieve all kenmerken according to given magic strings
    my @kenmerken   = $zaak->zaaktype_node_id->zaaktype_kenmerken->search(
        {
            'bibliotheek_kenmerken_id.magic_string' => { -in => [ keys %$new_values ] },
        },
        {
            join    => 'bibliotheek_kenmerken_id',
        }
    )->all;

    ### 2: Loop over the database kenmerken, to check for valid permissions. Also, prepare
    ### the new_values hash.
    my %fields;
    for my $kenmerk (@kenmerken) {
        my $permissions     = $kenmerk->format_required_permissions;

        ### First check for propery permission on the attribute when attribute is set
        if (@$permissions) {
            my $ok;
            for my $permission (@$permissions) {
                ### Check for group
                if (!$permission->{group} || !grep { $permission->{group}->id == $_->id } @groups) {
                    next;
                }

                ### Now check for role
                if (!$permission->{role} || !grep { $permission->{role}->id == $_->id } @roles) {
                    next;
                }

                $ok = 1
            }

            if (!$ok) {
                throw(
                    'case/update_fields_authorized/field_forbidden',
                    "You do not have the proper rights to update kenmerk: " . $kenmerk->magic_string
                );
            }
        }

        ### Permissions check out, continue
        ### We do not support 'opplusbare velden' yet, deref first item.
        $fields{ $kenmerk->get_column('bibliotheek_kenmerken_id') } = $new_values->{ $kenmerk->magic_string }->[0];
    }

    return $self->update_fields(
        {
            %$params,
            new_values  => \%fields,
        }
    );
}

=head2 _process_rules_updated_fields

    $self->_process_rules_updated_fields({
        case    => $schema->resultset('Zaak')->find(24),
        values  => {
            '2280' => [
                        'Vul 3 in'
                      ],
            '2281' => [
                        'Onwijzigbaar 3'
                      ]
        },
    })

Will get all current fields in this case via C<field_values> and merges it with the given C<values>. It will
feed it to the "ruler", and make sure the "hidden fields" will be emptied in the C<$return_values>. It will
also fill values with "vul_waarde_in" when the rule engine requests it.

=cut

define_profile _process_rules_updated_fields => (
    required    => {
        case        => 'Any',
        values      => 'HashRef',
    },
    optional    => {
        set_values_except_for_attrs => 'Any',
    }
);

sub _process_rules_updated_fields {
    my $self    = shift;
    my $params  = assert_profile(shift || {})->valid;

    my $case    = $params->{case};
    my $values  = $params->{values};

    my $rules   = $case->rules;

    return $values unless $rules;

    my %merged_values = (%{ $case->field_values }, %$values);

    ### Validate the current case params with the given params
    my $val_obj = $rules->validate(
        {
            %{ $rules->rules_params },
            %merged_values
        }
    );

    ### Empty the "hidden" attributes, and set the "vul_waarde_in" attributes
    my $processed_values = $val_obj->process_params({
        engine      => $rules,
        values      => \%merged_values,
        keyformat   => 'bibliotheek_kenmerken_id',
        set_values_except_for_attrs => $params->{set_values_except_for_attrs},
    });

    ### Return the correct list of values to update_fields
    return $processed_values;
}

define_profile update_fields => (
    required => {
        new_values => 'HashRef',
        zaak => 'Zaaksysteem::Zaken::ComponentZaak',
    },
    optional => {
        ignore_log_update => 'Bool',
        set_values_except_for_attrs => 'Any',
    },
);

sub update_fields {
    my $self = shift;

    my $params = assert_profile(shift)->valid;

    my $new_values = $params->{ new_values };
    my $zaak = $params->{ zaak };
    my $zaak_id = $zaak->id;

    my $error = 0;
    my $something_changed = 0;

    $new_values = $self->_process_rules_updated_fields(
        {
            case    => $zaak,
            values  => $new_values,
            set_values_except_for_attrs => $params->{set_values_except_for_attrs},
        }
    );

    $self->result_source->schema->txn_do(sub {
        eval {
            ### Result is also some kind of attribute
            if (my $result = delete $new_values->{'case.result'}) {
                $zaak->set_resultaat((ref $result eq 'ARRAY' ? $result->[0] : $result));
                $zaak->update;
            }

            ### Confidentiality is also some kind of attribute
            if (my $result = delete $new_values->{'case.confidentiality'}) {
                $zaak->set_confidentiality((ref $result eq 'ARRAY' ? $result->[0] : $result));
                $zaak->update;
            }

            ### Delete every value which does not have an integer key (like case.price...)
            $new_values = { map({ $_ => $new_values->{$_} } grep(/^\d+$/, keys %$new_values)) };

            my $params = {
                bibliotheek_kenmerken_id    => { in => [ keys %$new_values ]},
                zaak_id                     => $zaak_id,
            };

            my $kenmerken = $self->search($params,
                {
                    prefetch => 'bibliotheek_kenmerken_id'
                }
            );

            ### Only create kenmerk when value is not equal 'new value'
            $self->strip_identical_fields($new_values,$kenmerken);

            ### Delete old params
            $self->search(
                {
                    bibliotheek_kenmerken_id    => { in => [ keys %$new_values ]},
                    zaak_id                     => $zaak_id,
                },
            )->delete;

            for my $bibid (keys %{ $new_values }) {
                $something_changed = 1;
                my $create_kenmerk_params = {
                    bibliotheek_kenmerken_id    => $bibid,
                    zaak_id                     => $zaak_id,
                    values                      => $new_values->{$bibid}
                };

                $self->replace_kenmerk($create_kenmerk_params);

                unless ($params->{ ignore_log_update }) {
                    my $values = $create_kenmerk_params->{values};

                    $create_kenmerk_params->{values} =
                        UNIVERSAL::isa($values, 'ARRAY') ? $values : [$values];

                    delete $create_kenmerk_params->{zaak_id};
                    $create_kenmerk_params->{zaak} = $zaak;

                    $self->log_field_update($create_kenmerk_params);
                }
            }
        };

        if ($@) {
            $error++;
            throw('rs/case.attribute/update_fields', "Could not update attribute " . $@);
        }

        if ($something_changed) {
            $zaak->discard_changes;
            $zaak->touch;
        }
    });

    return not $error;
}

sub strip_identical_fields {
    my ($self, $new_values, $kenmerken) = @_;

    my $db_values;
    while (my $kenmerk = $kenmerken->next) {
        my $bibid = $kenmerk->bibliotheek_kenmerken_id->id;

        if (
            $db_values->{$bibid}
        ) {
            unless (UNIVERSAL::isa($db_values, 'ARRAY')) {
                $db_values->{$bibid} = [
                    $db_values->{$bibid}
                ];
            }

            push(
                @{ $db_values->{$bibid} }, $kenmerk->value
            );
        }

        $db_values->{$bibid} = $kenmerk->value;
    }

    for my $bibid (keys %{ $new_values }) {
        if (
            !ref($db_values->{$bibid}) &&
            !ref($new_values->{$bibid}) &&
            !length($db_values->{$bibid}) && !length($new_values->{$bibid})
        ) {
            delete($new_values->{$bibid})
        }


        next unless $db_values->{$bibid};

        if (
            !ref($db_values->{$bibid}) &&
            !ref($new_values->{$bibid}) &&
            (
                defined $db_values->{$bibid} ? $db_values->{$bibid} : ''
            ) eq (
                defined $new_values->{$bibid} ? $new_values->{$bibid} : ''
            )
        ) {
            delete($new_values->{$bibid});
        }

        if (
            UNIVERSAL::isa($db_values->{$bibid}, 'ARRAY') &&
            UNIVERSAL::isa($new_values->{$bibid}, 'ARRAY') &&
            scalar(@{ $db_values->{$bibid} }) == scalar(@{ $new_values->{$bibid} })
        ) {
            my $match = 1;
            for my $value (@{ $new_values->{$bibid} }) {
                $match = 0 unless (grep { $value eq $_ } @{ $db_values->{$bibid} });
            }

            if ($match) {
                delete($new_values->{$bibid});
            }
        }
    }
}


=head2 get_bag_hr

Convert bag strings to something human readable.

=cut

sub get_bag_hr {
    my ($self) = shift;
    my @values = @_;

    my @hr;
    my $value_string;
    for my $bag_id (@values) {
        push @hr, $self->result_source->schema->resultset('BagNummeraanduiding')
            ->get_record_by_source_identifier($bag_id)->to_string;
    }
    if (@hr > 1) {
        $value_string = join(", ", grep { defined($_) } @hr);
    }
    else {
        $value_string = $hr[0];
    }

    return $value_string;
}


=head2 delete_fields

The use case for this method is fields that are being hidden by rules.
We receive a list of field_ids (bibliotheek_kenmerken) and we look
if any values are stored. If so, these are deleted, and the fields that
are actually affected are gathered and put in a log.

=cut

sub delete_fields {
    my ($self, $arguments) = @_;

    my $bibliotheek_kenmerken_ids = $arguments->{bibliotheek_kenmerken_ids} or die "need bibliotheek_kenmerken_ids";

    throw('delete_fields/missing_argument', 'ZaakKenmerk::delete_fields needs a "zaak" argument')
        unless $arguments->{zaak};

    my $zaak_id = $arguments->{zaak}->id;

    if ($self->log->is_trace) {
        $self->log->trace("Hiding fields in case $zaak_id " . join ", ", @$bibliotheek_kenmerken_ids);
    }

    my $current = $self->search({
        zaak_id => $zaak_id, # ensure only this case is affected
        bibliotheek_kenmerken_id => {
            '-in' => $bibliotheek_kenmerken_ids
        }
    });

    # if there are no values stored that need deletion, we're done. nothing to do here
    my @current = $current->all
        or return;

    # a list with removed attributes. because checkboxes are stored as separate values
    # abuse a hash to ensure uniqueness.
    # using get_column to avoid subqueries - performance tweak
    my $deleted = { map { $_->get_column('bibliotheek_kenmerken_id') => 1 } @current };

    $current->delete;

    my $schema = $self->result_source->schema;

    my $logging = $schema->resultset('Logging');
    my $bibliotheek_kenmerken = $schema->resultset('BibliotheekKenmerken');

    # get the names of the deceased attributes
    my $attributes = join ", ", map { $_->naam } $bibliotheek_kenmerken->search({
        id => {
            '-in' => [keys $deleted]
        }
    })->all;

    # if you delete a bunch of fields, the logging line get too long
    # we need to solve this in the logging module, danger, danger
    $attributes = substr($attributes, 0, 100) . '...' if length $attributes > 100;

    $arguments->{zaak}->trigger_logging('case/attribute/removebulk', {
        component => 'kenmerk',
        data => {
            attributes => $attributes,
            reason => 'verborgen door regels'
        }
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 create_kenmerk

TODO: Fix the POD

=cut

=head2 create_kenmerken

TODO: Fix the POD

=cut

=head2 get

TODO: Fix the POD

=cut

=head2 log_field_create

TODO: Fix the POD

=cut

=head2 log_field_update

TODO: Fix the POD

=cut

=head2 replace_kenmerk

TODO: Fix the POD

=cut

=head2 strip_identical_fields

TODO: Fix the POD

=cut

=head2 update_field

TODO: Fix the POD

=cut

=head2 update_fields

TODO: Fix the POD

=cut

