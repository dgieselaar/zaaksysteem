package Zaaksysteem::Schema::RemoteApiKeys;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::RemoteApiKeys

=cut

__PACKAGE__->table("remote_api_keys");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'remote_api_keys_id_seq'

=head2 key

  data_type: 'varchar'
  is_nullable: 0
  size: 60

=head2 permissions

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "remote_api_keys_id_seq",
  },
  "key",
  { data_type => "varchar", is_nullable => 0, size => 60 },
  "permissions",
  { data_type => "text", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 11:07:01
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:AGFEqR5gpZ9L2qIjysmP6w


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

