package Zaaksysteem::CLI::NatuurlijkPersoon::Searchable;
use Moose;

extends 'Zaaksysteem::CLI';

use Zaaksysteem::Tools;
use DateTime;

around run => sub {
    my $orig = shift;
    my $self = shift;

    $self->$orig(@_);

    my $found = $self->schema->resultset('NatuurlijkPersoon')->search(undef, { order_by => { '-asc' => 'deleted_on' }});
    while (my $np = $found->next) {
        $self->do_transaction(
            sub {
                my ($self, $schema) = @_;
                $self->log->info(
                    sprintf(
                        "Updating search terms for NP %s with BSN %09d",
                        $np->id, $np->bsn
                    )
                );
                $np->update();
            }
        );
    }
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::CLI::NatuurlijkPersoon::Deduplicate - Deduplicate Natuurlijk Personen.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
