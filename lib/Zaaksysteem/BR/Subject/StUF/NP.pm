package Zaaksysteem::BR::Subject::StUF::NP;

use Moose::Role;

use Zaaksysteem::Tools;
use List::Util qw/first/;

=head1 NAME

Zaaksysteem::BR::Subject::StUFBG0310::STUFNPS - Bridge helpers for module STUFNPS

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge for inflating results from StUF-questions.

=head1 ATTRIBUTES

=head1 METHODS

=head2 search

See L<Zaaksysteem::BR::Subject#search> for usage information

=cut

around 'search' => sub {
    my $method      = shift;
    my $self        = shift;
    my ($params)    = @_;

    return $self->$method(@_) if !$self->_is_remote_nps($params);

    my $interface = $self->_assert_stuf_interface;

    my %cleanparams = map { my $ckey = $_; $ckey =~ s/^subject\.//; $ckey => $params->{$_} } keys %$params;
    delete($cleanparams{subject_type});

    my $transaction         = $interface->process_trigger('search_nps', \%cleanparams);
    my $result              = $transaction->get_processor_params->{result};

    if ($transaction->get_processor_params->{error}) {
        throw(
            "br/subject/search/remote_failure",
            "Remote error: " . $transaction->get_processor_params->{error}->{message}
        );
    }

    ### We really want a new Iterator class which supportys arrays. Where we can call "next,fist,search" etc
    ### on. For now, only allow list context
    if (!wantarray()) {
        throw('br/subject/search_remote', 'Error: remote searching requires list context');
    }

    my @objects;
    for my $person (@$result) {
        push(@objects, $self->object_from_params($person));
    }

    return @objects;
};

=head2 remote_import

See L<Zaaksysteem::BR::Subject#remote_import> for usage information

=cut

around 'remote_import' => sub {
    my $method      = shift;
    my $self        = shift;
    my ($params)    = @_;

    my $type        = (blessed($params) ? $params->subject_type : $params->{subject_type});

    return $self->$method(@_) if !$self->_is_remote_nps({ subject_type => $type });

    my $interface = $self->_assert_stuf_interface;

    ### Create this entry into our system.
    my $object = (blessed($params) ? $params : $self->object_from_params($params));

    ### Before trying to import an existing subject, let's see if we already have an authenticated
    ### person with this BSN in our system
    my @existing_objects = $self->search_rs({
        'subject_type'              => 'person',
        'subject.personal_number'   => $object->subject->personal_number,
    })->all;

    ### Make sure we pick the one with a subscription, e.g.: the authenticated one.
    my $existing_object  = first { $_->subscription_id } @existing_objects;

    return $existing_object->as_object if $existing_object;

    ### Now import it into StUF
    try {
        $self->schema->txn_do(
            sub {
                $object         = $self->save($object);

                my $trigger_params = {
                    id          => $object->id,
                    subject     => $self->object_as_params($object->subject)
                };

                if ($object->external_subscription && $object->external_subscription->external_identifier) {
                    $trigger_params->{external_subscription} = {
                        external_identifier => $object->external_subscription->external_identifier
                    };
                }

                my $transaction         = $interface->process_trigger(
                    'import_nps',
                    $trigger_params
                );
                my $result              = $transaction->get_processor_params->{result};

                if (!$result) {
                    throw(
                        'br/subject/remote_import/remote_error',
                        'Problem importing person because other party returned an error, transaction id: '
                        . ($transaction ? $transaction->id : '<unknown>')
                    );
                }
            }
        );
    } catch {
        throw(
            'br/subject/remote_import/remote_error',
            "$_"
        );
    };

    ### Now import it into StUF
    return $object->discard_changes(schema => $self->schema);
};

=head1 PRIVATE METHODS

=head2 _is_remote_nps

    $self->_is_remote_nps({ subject_type => 'person'});

    ### Returns 1 if $self->remote_search eq 'stuf';

Returns true when searching is remotely and given subject_type equals person.

=cut

sub _is_remote_nps {
    my $self        = shift;
    my $params      = shift;

    return 1 if (
        $params->{subject_type} &&
        $params->{subject_type} eq 'person' &&
        $self->remote_search &&
        lc($self->remote_search) eq 'stuf'
    );

    return;
}

=head2 _assert_stuf_interface

    my $interface = $self->_assert_stuf_interface;

Returns the primary STUF interface from L<Zaaksysteem::Backend::Sysin::Modules>

=cut

sub _assert_stuf_interface {
    my $self                = shift;

    ### StUF Configuration
    my $config_ifaces       = $self->schema->resultset('Interface')->search_active(
        {
            module  => 'stufconfig'
        },
    );

    throw(
        'br/subject/search_remote/multiple_cfg_ifaces',
        'Multiple config interfaces found, cannot continue'
    ) if $config_ifaces->count > 1;

    throw(
        'br/subject/search_remote/no_stuf_cfg_iface',
        'Remote search requested, but no active StUF configuration found'
    ) if $config_ifaces->count < 1;

    my $stuf_cfg_iface      = $config_ifaces->first;
    my $interface           = $stuf_cfg_iface->module_object->get_natuurlijkpersoon_interface($stuf_cfg_iface);

    ### Search object
    if (!$interface) {
        throw(
            'br/subject/search_remote/no_stuf_iface',
            'Remote search requested, but no active StUF NP Interface found'
        );
    }

    return $interface;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

