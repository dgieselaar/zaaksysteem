package Zaaksysteem::Controller::Plugins::Twofactor;
use Moose;

BEGIN { extends 'Zaaksysteem::Controller'; }

use Zaaksysteem::Constants qw(
    PARAMS_PROFILE_DEFAULT_MSGS
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_TWOFACTOR
);
use Zaaksysteem::Tools;

my $SECOND_FACTOR_RE = qr/^[A-Za-z0-9]+$/;
my $USERNAME_RE      = qr/^[^\s]+$/;

=head1 NAME

Zaaksysteem::Controller::Plugins::Twofactor - Two-factor authentication controller

=head1 ACTIONS

=head2 base

Base for all two-factor authentication controllers. Retrieves the active
"auth_twofactor" interface and puts it on the stash.

Reserves the C</auth/twofactor> URI namespace.

=cut

sub base : Chained('/') : PathPart('auth/twofactor') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->stash->{twofactor_interface} = $self->_get_interface($c)
        or throw("twofactor/unconfigured", "Two-factor authentication is not configured");

    return;
}

=head2 login

Show the "Login using two-factor auth" (or register a new account) screen

=head3 URL Path

C</auth/twofactor>

=cut

sub login : Chained('base') : PathPart('') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;

    if ($c->user_exists) {
        $self->log->debug("Found existing user (behandelaar) session, removing it.");
        $c->delete_session;
    }

    # In case of an XHR, and we get here... tell the frontend that we are not logged in
    if ($c->req->is_xhr) {
        $c->res->status('401');
    }

    $c->session->{_twofactor}{success_endpoint} = $c->req->params->{success_endpoint}
        if(exists $c->req->params->{success_endpoint});

    $c->stash->{twofactor_intro_text} = $c->stash->{twofactor_interface}->jpath('$.login_message');

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }

    # Show username & password page
    $c->stash->{template} = 'plugins/twofactor/login.tt';
}

=head2 logout

Logs out the user, and redirects to the login page.

=head3 URL Path

C</auth/twofactor/logout>

=cut

sub logout : Chained('base') : PathPart('logout') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    my $success_endpoint = $c->session->{_twofactor}{success_endpoint};

    # The success endpoint is necessary to make "log in again" work.
    delete $c->session->{_twofactor};
    $c->session->{_twofactor}{success_endpoint} = $success_endpoint;

    $c->flash->{logged_out} = 1;

    # Success endpoint should still be in session at this point.
    $c->res->redirect($c->uri_for('/auth/twofactor'));
}

=head2 process_login

Perform the first step of the login using the specified username and password.

If the login is correct, an SMS will be sent and the user will be redirected to
the "second factor" page.

If the login is incorrect, the user will be redirected back to the login page,
and see a message.

=head3 URL Path

C</auth/twofactor/process_login>

=cut

Zaaksysteem->register_profile(
    method => 'process_login',
    profile => {
        required => [qw(username password)],
        constraint_methods => {
            username => $USERNAME_RE,
        },
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    },
);

# Log in to existing account
sub process_login : Chained('base') : PathPart('process_login') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $args = assert_profile($c->req->params)->valid;

    my $result = $c->stash->{twofactor_interface}->process_trigger(
        "check_password",
        {
            username => $args->{username},
            password => $args->{password},
        }
    );

    if ($result->{success}) {
        $c->session->{_twofactor}{username} = $args->{username};

        my $properties = $result->{subject}->properties;
        $c->session->{_twofactor}{authenticated_id} = $self->_get_authenticated_id($result->{subject});
        $c->session->{_twofactor}{email} = $properties->{initial_email_address};
        $c->session->{_twofactor}{phone} = $result->{betrokkene}
            ? $result->{betrokkene}->mobiel
            : $properties->{initial_phone_number};
        $c->session->{_twofactor}{subject_type} = $result->{subject}->subject_type;

        $c->stash->{twofactor_interface}->process_trigger(
            "send_sms",
            {
                phonenumber => $c->session->{_twofactor}{phone},
                message     => "Uw inlogcode is $result->{twofactor_code}",
            },
        );
        $c->session->{_twofactor}{second_factor} = $result->{twofactor_code};
        $c->res->redirect($c->uri_for('/auth/twofactor/second_factor'));
    }
    else {
        $c->flash->{twofactor_error} = "Onjuiste gebruikersnaam of wachtwoord";
        $c->res->redirect($c->uri_for('/auth/twofactor'))
    }
}

=head2 second_factor

Show the "Enter the code you received by SMS" page.

=head3 URL Path

C</auth/twofactor/second_factor>

=cut

sub second_factor : Chained('base') : PathPart('second_factor') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;
    if (!$c->session->{_twofactor}{second_factor}) {
        throw("twofactor/sequence_error", "Sequence error: can't do second factor without first logging in");
    }

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }

    $c->stash->{template} = 'plugins/twofactor/second_factor.tt';
}

=head2 verify_second_factor

Verify that the code entered by the user is correct.

Then forwards to the "success_endpoint" to continue the action that requires logging in.

=head3 URL Path

C</auth/twofactor/verify_second_factor>

=cut

Zaaksysteem->register_profile(
    method => 'verify_second_factor',
    profile => {
        required => [qw(factor)],
        constraint_methods => {
            factor => $SECOND_FACTOR_RE,
        },
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    }
);

sub verify_second_factor : Chained('base') : PathPart('verify_second_factor') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    if (!$c->session->{_twofactor}{second_factor}) {
        throw("twofactor/sequence_error", "Sequence error: can't do second factor without first logging in");
    }

    my $args = assert_profile($c->req->params)->valid;

    my $result = $c->stash->{twofactor_interface}->process_trigger(
        "verify_second_factor",
        {
            stored  => $c->session->{_twofactor}{second_factor},
            entered => $args->{factor},
        },
    );

    if ($result->{success}) {
        $c->session->{_twofactor}{authenticated} = 1;

        $c->res->redirect(
            $c->session->{_twofactor}{success_endpoint}
        );
    }
    else {
        $c->flash->{twofactor_error} = "Onjuiste verificatiecode";
        $c->res->redirect('/auth/twofactor/second_factor');
    }
}

=head2 register

Show the "Register a new account" page.

=head3 URL Path

C</auth/twofactor/register>

=cut

# Register a new account
sub register : Chained('base') : PathPart('register') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;

    $c->stash->{aanvrager_type} = $c->session->{_zaak_create}{ztc_aanvrager_type};

    $c->stash->{twofactor_intro_text} = $c->stash->{twofactor_interface}->jpath('$.registration_message');

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }

    # Show "Register account" page
    $c->stash->{template} = 'plugins/twofactor/register.tt';
}

=head2 process_registration

Process the initial registration of the user account.

Compares the supplied passwords, checks if the email address is already in use,
etc.

=head3 URL Path

C</auth/twofactor/process_registration>

=cut

Zaaksysteem->register_profile(
    method => 'process_registration',
    profile => {
        required => [qw( username password password_check email phone) ],
        optional => [qw( bsn kvk vestigingsnummer )] ,
        constraint_methods => {
            username       => $USERNAME_RE,
            password       => qr/^.+$/,
            password_check => qr/^.+$/,
            email          => qr/^.+?\@.+\.[a-z0-9]{2,}$/,
            phone          => qr/^06\d{8}$/,
            bsn              => qr/^[0-9]{8,9}$/,
            kvknummer        => qr/^[0-9]{8}$/,
            vestigingsnummer => qr/^[0-9]{12}$/,
        },
        dependencies => {
            aanvrager_type => {
                'natuurlijk_persoon' => [ 'bsn' ],
                'niet_natuurlijk_persoon' => [ 'kvknummer' ],
            }
        },
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    },
);

sub process_registration : Chained('base') : PathPart('process_registration') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $args = assert_profile($c->req->params)->valid;

    $args->{subject_type} = $c->session->{_zaak_create}{ztc_aanvrager_type} eq 'natuurlijk_persoon'
        ? 'person'
        : 'company';

    my $result = $c->stash->{twofactor_interface}->process_trigger(
        "register_account",
        $args,
    );

    if ($result->{success}) {
        $c->session->{_twofactor}{username} = $args->{username};
        $c->session->{_twofactor}{second_factor} = $result->{twofactor_code};

        my $properties = $result->{subject}->properties;
        $c->session->{_twofactor}{authenticated_id} = $self->_get_authenticated_id($result->{subject});

        $c->session->{_twofactor}{email} = $properties->{initial_email_address};
        $c->session->{_twofactor}{phone} = $properties->{initial_phone_number};
        $c->session->{_twofactor}{subject_type} = $result->{subject}->subject_type;

        $c->stash->{twofactor_interface}->process_trigger(
            "send_sms",
            {
                phonenumber => $c->session->{_twofactor}{phone},
                message     => "Uw inlogcode is $result->{twofactor_code}",
            },
        );

        # Redirect to registration_factor
        $c->session->{_twofactor}{register_success} = 1;
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/registration_factor'),
        );
    }
    else {
        if ($result->{reason} eq 'twofactor/subject_exists') {
            $c->flash->{twofactor_error} = 'De opgegeven gebruikersnaam is al in gebruik';
        }
        elsif ($result->{reason} eq 'twofactor/password_mismatch') {
            $c->flash->{twofactor_error} = 'Wachtwoorden komen niet overeen';
        }
        # Stop variabelen in session
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/register'),
        );
    }
}

=head2 registration_factor

Shows the "Please enter the code you received" page in the new account registration flow.

=head3 URL Path

C</auth/twofactor/registration_factor>

=cut

sub registration_factor : Chained('base') : PathPart('registration_factor') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;
    if (!$c->session->{_twofactor}{second_factor}) {
        throw("twofactor/sequence_error", "Sequence error: can't do second factor without first registering");
    }

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }

    # Show "second factor" page.
    $c->stash->{template} = 'plugins/twofactor/register_second_factor.tt';
}

=head2 verify_registration_factor

Verifies that the code entered by the user is correct, and enables the account
if that's the case.

Then forwards to the "success_endpoint" to continue the action that requires logging in.

=head3 URL Path

C</auth/twofactor/verify_registration_factor>

=cut

Zaaksysteem->register_profile(
    method => 'verify_registration_factor',
    profile => {
        required => [qw(factor)],
        constraint_methods => {
            factor => $SECOND_FACTOR_RE,
        },
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    },
);

sub verify_registration_factor : Chained('base') : PathPart('verify_registration_factor') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    if (!$c->session->{_twofactor}{second_factor}) {
        throw("twofactor/sequence_error", "Sequence error: can't do second factor without first registering");
    }

    my $args = assert_profile($c->req->params)->valid;

    my $result = $c->stash->{twofactor_interface}->process_trigger(
        "verify_second_factor",
        {
            stored  => $c->session->{_twofactor}{second_factor},
            entered => $args->{factor},
        },
    );

    if ($result->{success}) {
        # Code correct: mark account "active"
        $self->log->trace("Second factor: successful for " . $c->session->{_twofactor}{username});
        $c->stash->{twofactor_interface}->process_trigger(
            "activate_account",
            {
                username => $c->session->{_twofactor}{username}
            }
        );

        $c->session->{_twofactor}{authenticated} = 1;
        $c->res->redirect(
            $c->session->{_twofactor}{success_endpoint}
        );
    }
    else {
        $self->log->trace("Second factor: failure");
        $c->flash->{twofactor_error} = "Onjuiste verificatiecode";
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/registration_factor')
        );
    }
}

=head2 lost_password

Show the "Lost password" page, where a user who forgot their password can start
the process of setting a new one.

=head3 URL Path

C</auth/twofactor/lost_password>

=cut

sub lost_password : Chained('base') : PathPart('lost_password') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;

    $c->stash->{twofactor_password_text} = $c->stash->{twofactor_interface}->jpath('$.password_lost_message');

    # Show "second factor" page.
    $c->stash->{template} = 'plugins/twofactor/lost_password.tt';
}

=head2 process_lost_password

Process the form submission by the "Lost password" page. This sends an email to
the user (if one hasn't been sent in the past 5 minutes), which contains a link
that will allow the user to reset their password.

=head3 URL Path

C</auth/twofactor/process_lost_password>

=cut

Zaaksysteem->register_profile(
    method => 'process_lost_password',
    profile => {
        required => [qw(username)],
        constraint_methods => {
            username => $USERNAME_RE,
        },
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    }
);

sub process_lost_password : Chained('base') : PathPart('process_lost_password') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    my $args = assert_profile($c->req->params)->valid;

    my $result = $c->stash->{twofactor_interface}->process_trigger(
        "lost_password_initialize",
        {
            username => $args->{username},
        }
    );

    if ($result->{success}) {
        $c->stash->{twofactor_interface}->process_trigger(
            "send_sms",
            {
                phonenumber => $result->{betrokkene}->mobiel,
                message     => "Uw inlogcode is $result->{twofactor_code}",
            },
        );
        $c->session->{_twofactor}{second_factor} = $result->{twofactor_code};
        $c->session->{_twofactor}{recovery_username} = $args->{username};
    }
    # Errors are ignored, as a method to reduce SMS spam

    $c->res->redirect($c->uri_for('/auth/twofactor/lost_password_verify_second_factor_page'));
}

=head2 lost_password_verify_second_factor_page

Landing page for two-factor verification of password reset process.

=head3 URL Path

C</auth/twofactor/lost_password_verify_second_factor_page>

=cut

# Show "SMS has been sent; please type code here" page
sub lost_password_verify_second_factor_page : Chained('base') : PathPart('lost_password_verify_second_factor_page') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }

    # Show "second factor" page.
    $c->stash->{template} = 'plugins/twofactor/lost_password_second_factor.tt';
}

=head2 lost_password_verify_second_factor

Verify if the "second factor" was correct while doing the password reset process.

=head3 URL Path

C</auth/twofactor/lost_password_verify_second_factor>

=cut

Zaaksysteem->register_profile(
    method => 'lost_password_verify_second_factor',
    profile => {
        required => [qw(factor)],
        constraint_methods => {
            factor => $SECOND_FACTOR_RE,
        },
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    }
);

# Verify the SMS, redirect to "change password" page
sub lost_password_verify_second_factor : Chained('base') : PathPart('lost_password_verify_second_factor') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    if (!$c->session->{_twofactor}{second_factor}) {
        throw("twofactor/sequence_error", "Sequence error: can't verify second factor without first requesting one");
    }

    my $args = assert_profile($c->req->params)->valid;

    my $result = $c->stash->{twofactor_interface}->process_trigger(
        "verify_second_factor",
        {
            stored  => $c->session->{_twofactor}{second_factor},
            entered => $args->{factor},
        },
    );

    if ($result->{success}) {
        $c->session->{_twofactor}{reset_password} = 1;
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/lost_password_new_password')
        );
    }
    else {
        $c->flash->{twofactor_error} = "Onjuiste verificatiecode";
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/lost_password_verify_second_factor_page')
        );
    }
}

=head2 lost_password_new_password

Show the "Pick a new password" page after verifying the second factor (SMS code)

=head3 URL Path

C</auth/twofactor/lost_password_new_password>

=cut

# Show "type a new password" page
sub lost_password_new_password : Chained('base') : PathPart('lost_password_new_password') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;
    if (!$c->session->{_twofactor}{reset_password}) {
        throw("twofactor/sequence_error", "Sequence error: can't reset the password without first verifying");
    }

    if ($c->flash->{twofactor_error}) {
        $c->stash->{twofactor_error} = $c->flash->{twofactor_error};
    }

    # Show "Type a new password twice" page.
    $c->stash->{template} = 'plugins/twofactor/lost_password_new_password.tt';
}

=head2 lost_password_update_password

Re-set the password of the user to the newly entered one.

=cut

Zaaksysteem->register_profile(
    method => 'lost_password_update_password',
    profile => {
        required => [qw(password password_check)],
        msgs => PARAMS_PROFILE_DEFAULT_MSGS,
    },
);

sub lost_password_update_password : Chained('base') : PathPart('lost_password_update_password') : Args(0) : Access(*) {
    my ($self, $c) = @_;

    if ($c->req->is_xhr && $c->req->params->{do_validation}) {
        $c->zvalidate;
        $c->detach;
    }

    if (!$c->session->{_twofactor}{reset_password}) {
        throw("twofactor/sequence_error", "Sequence error: can't reset the password without first verifying");
    }

    my $args = assert_profile($c->req->params)->valid;

    my $result = $c->stash->{twofactor_interface}->process_trigger(
        "change_password",
        {
            username       => $c->session->{_twofactor}{recovery_username},
            password       => $args->{password},
            password_check => $args->{password_check},
        },
    );

    if ($result->success_count) {
        $c->session->{_twofactor}{password_reset_done} = 1;
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/lost_password_password_changed')
        );
    }
    else {
        $c->flash->{twofactor_error} = "Wachtwoorden komen niet overeen";
        $c->res->redirect(
            $c->uri_for('/auth/twofactor/lost_password_new_password')
        );
    }
}

=head2 lost_password_password_changed

Landing page for succesfully reset password.

=head3 URL Path

C</auth/twofactor/lost_password_password_changed>

=cut

# Commit changed password, redirect to login page
sub lost_password_password_changed  : Chained('base') : PathPart('lost_password_password_changed') : Args(0) : Access(*) : Method('GET') {
    my ($self, $c) = @_;
    if (!$c->session->{_twofactor}{password_reset_done}) {
        throw("twofactor/sequence_error", "Sequence error: can't reset the password without first verifying");
    }

    $c->stash->{template} = 'plugins/twofactor/lost_password_password_changed.tt';
}

=head2 _zaak_create_security

Called by /form to get all authentication related data into the right location
in the session, OR forward to the login page if authentication wasn't
successful yet.

=cut

sub _zaak_create_security : Private {
    my ($self, $c) = @_;

    if (
        ($c->req->params->{authenticatie_methode} // '') eq 'twofactor' ||
        ($c->session->{_zaak_create}{extern}{verified} // '')  eq 'twofactor'
    ) {
        my $interface = $self->_get_interface($c);

        $self->log->trace("Twofactor authentication requested");
        if ($c->session->{_twofactor}{authenticated}) {
            $self->log->trace(sprintf(
                "Twofactor authentication successful for user '%s' (type: %s)",
                $c->session->{_twofactor}{username},
                $c->session->{_zaak_create}{ztc_aanvrager_type},
            ));
            my %extern;

            # Check if we are allowed to create this zaaktype
            $extern{aanvrager_type} = $c->session->{_zaak_create}{ztc_aanvrager_type};
            $extern{verified}       = 'twofactor';
            $extern{id}             = $c->session->{_twofactor}{authenticated_id};

            $c->session->{_zaak_create}{extern} = \%extern;

            $c->stash->{aanvrager_type} = $extern{aanvrager_type};
            $c->stash->{aanvrager}{email}  = $c->session->{_twofactor}{email};
            $c->stash->{aanvrager}{mobiel} = $c->session->{_twofactor}{phone};
        }
        else {
            $self->log->trace("Twofactor authentication not (yet) successful - forwarding to login page");

            my %arguments;

            $arguments{'authenticatie_methode'} = 'twofactor'
                if ($c->req->params->{authenticatie_methode});
            $arguments{'sessreset'} = 1
                if ($c->req->params->{sessreset});

            if (   $c->req->params->{ztc_aanvrager_type}
                && $c->req->params->{ztc_aanvrager_type} =~ /^((?:niet_)?natuurlijk_persoon)$/
            ) {
                $arguments{'ztc_aanvrager_type'} = $1;
            }

            if (   $c->req->params->{zaaktype_id}
                && $c->req->params->{zaaktype_id} =~ /^\d+$/
            ) {
                $arguments{'zaaktype_id'} = $c->req->params->{zaaktype_id};
            }

            if ($interface->jpath('$.allow_new_accounts')) {
                $c->session->{pip_login} = 0;
            }
            else {
                # This hides the "New account" link
                $c->session->{pip_login} = 1;
            }
            $c->res->redirect(
                $c->uri_for(
                    '/auth/twofactor',
                    { success_endpoint => $c->uri_for('/zaak/create/webformulier/', \%arguments) }
                )
            );

            if (
                $c->session->{_zaak_create}{extern} &&
                $c->session->{_zaak_create}{verified} eq 'twofactor'
            ) {
                delete($c->session->{_zaak_create}{extern});
            }

            $c->detach;
        }
    }
    else {
        # Different authentication method requested. Passthrough.
        return;
    }

    if ( $c->session->{_zaak_create}{ztc_aanvrager_type} eq 'natuurlijk_persoon' ) {
        $c->forward('Zaaksysteem::Controller::Plugins::Digid', '_zaak_create_aanvrager');
    }
    else {
        $c->forward('Zaaksysteem::Controller::Plugins::Bedrijfid', '_zaak_create_aanvrager');
    }
}

sub _zaak_create_load_externe_data : Private {
    my ($self, $c) = @_;

    return unless $c->session->{_zaak_create}{extern}{verified} eq 'twofactor' &&
        $c->session->{_zaak_create}{aanvrager_update};

    if($c->req->params->{aanvrager_update}) {
        my $type = ($c->session->{_zaak_create}{ztc_aanvrager_type} eq 'natuurlijk_persoon')
            ? 'natuurlijk_persoon'
            : 'bedrijf';

        my $id = $c->model('Betrokkene')->create(
            $type,
            {
                %{ $c->session->{_zaak_create}{aanvrager_update} },
                'np-authenticated'   => 0,
                'np-authenticatedby' => ZAAKSYSTEEM_GM_AUTHENTICATEDBY_TWOFACTOR,
            }
        );

        $c->session->{_zaak_create}{ztc_aanvrager_id} = "betrokkene-$type-" .  $id;
    }
}

sub _get_interface {
    my ($self, $c) = @_;

    return $c->model('DB::Interface')->search_active({module => 'auth_twofactor'})->first
}

sub _get_authenticated_id {
    my $self = shift;
    my $subject = shift;

    if ($subject->subject_type eq 'person') {
        $self->log->trace("BSN found: '" . $subject->properties->{bsn} . "'");
        return $subject->properties->{bsn};
    }
    else {
        my $rv = $subject->properties->{kvknummer};
        if (length($subject->properties->{vestigingsnummer})) {
            $rv .= sprintf(
                "%012d",
                $subject->properties->{vestigingsnummer}
            );
        }
        $self->log->trace("KVK/VN found: '" . $rv . "'");
        return $rv;
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
