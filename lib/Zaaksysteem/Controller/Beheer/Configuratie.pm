package Zaaksysteem::Controller::Beheer::Configuratie;

use Moose;

use Data::Dumper;

BEGIN { extends 'Zaaksysteem::Controller' }

sub base : Chained('/') : PathPart('beheer/configuration') : CaptureArgs(0) {
    my ($self, $c) = @_;

    $c->assert_any_user_permission('admin');
}

sub configuration : Chained('base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ allocation_notification_template_id } = [
        $c->model('DB::BibliotheekNotificaties')->search({}, { order_by => { -asc => 'label' } })->all
    ];

    $c->stash->{ feedback_email_template_id } = [
        $c->model('DB::BibliotheekNotificaties')->search({}, { order_by => { -asc => 'label' } })->all
    ];

    my $rs = $c->model('DB::Config')->search({ advanced => $c->req->param('advanced') // 0 });

    $c->stash->{ configuration } = [ $rs->all ];
    $c->stash->{ template } = 'beheer/configuration.tt';
}

sub save : Chained('base') : PathPart('save') : Args(0) {
    my ($self, $c) = @_;

    for my $param (keys %{ $c->req->params }) {
        my $item = $c->model('DB::Config')->search({ parameter => $param })->first;

        next unless $item;

        $item->value($c->req->param($param));
        $item->update;
    }

    $c->push_flash_message('Instellingen opgeslagen');

    $c->res->redirect($c->uri_for('/beheer/configuration'));
    $c->detach;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 configuration

TODO: Fix the POD

=cut

=head2 save

TODO: Fix the POD

=cut

