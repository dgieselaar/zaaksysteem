package Zaaksysteem::Controller::API::Search;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;
use Zaaksysteem::Constants qw[ZAAKSYSTEEM_CONSTANTS];

use JSON qw[];
use File::Slurp qw[read_file];
use File::Spec::Functions qw[catfile];

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Search - Helper controller for frontend data

=head1 ACTIONS

=cut

sub base : Chained('/api/base') : PathPart('search') : CaptureArgs(0) { }

=head2 configuration

This action returns a ZAPI response with the CRUD interface description for
a search UI.

When no specific configuration could be found, a default interface UI is
chosen.

=head3 URL

C</api/search/config/[OBJECT TYPE]>

=cut

sub configuration : Chained('base') : PathPart('config') : Args(1) : ZAPI {
    my ($self, $c, $type) = @_;

    my $config;

    for my $name ( $type, 'default' ) {
        my $filename = catfile(
            $c->config->{ root },
            qw[data search config],
            sprintf('%s.json', $name)
        );

        next unless -e $filename;

        my $contents = read_file($filename, binmode => ':encoding(UTF-8)');

        $config = JSON->new->utf8->decode($contents);

        last;
    }

    return unless defined $config;

    my $class = $c->model('Object')->load_object_package($type);

    # 'case' instances are special little snowflakes
    if ($type ne 'case' && $class) {
        my @attributes;

        for my $attr ($class->object_attributes) {
            push @attributes, {
                name => $attr->name,
                type => $attr->type,
                label => $attr->label,
                is_multiple => 0,
                data => {
                    options => $attr->has_data ? $attr->data->{ values } : undef
                }
            };
        }

        $config->{ attributes } = \@attributes;
    }

    $c->stash->{ zapi } = [ $config ];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 columns

TODO: Fix the POD

=cut

=head2 delete

TODO: Fix the POD

=cut

=head2 grouping

TODO: Fix the POD

=cut

=head2 groups

TODO: Fix the POD

=cut

=head2 id

TODO: Fix the POD

=cut

=head2 json_get_contents

TODO: Fix the POD

=cut

=head2 list

TODO: Fix the POD

=cut

=head2 update

TODO: Fix the POD

=cut

=head2 view

TODO: Fix the POD

=cut
