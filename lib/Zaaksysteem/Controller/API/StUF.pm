package Zaaksysteem::Controller::API::StUF;

use Moose;

use Zaaksysteem::Backend::Sysin::Modules;
use Zaaksysteem::Tools;

use XML::LibXML;

BEGIN { extends 'Zaaksysteem::General::SOAPController' }

use constant COMPONENT_KENNISGEVING => 'kennisgeving';
use constant MODEL_SBUS             => 'SBUS';

### Here for backwards compatible reasons
sub bg0204 :Local SOAP('DocumentLiteral')  {
   my ( $self, $c)      = @_;

   $c->forward('endpoint');
}

sub stuf0204 :Local SOAP('DocumentLiteral')  {
   my ( $self, $c)      = @_;

   $c->forward('endpoint');
}

sub stuf0301 :Local SOAP('DocumentLiteral')  {
   my ( $self, $c)      = @_;

   $c->forward('endpoint');
}


sub endpoint : Private {
   my ( $self, $c)      = @_;

    my $xml              = $c->stash->{soap}->envelope();
    if (my $return_xml = $c->forward('handle_kennisgeving', [ $xml ])) {
        $c->stash->{soap}->literal_return($return_xml);
    }
}


sub handle_kennisgeving : Private {
    my ($self, $c, $xml)    = @_;

    ### Detect entiteittype
    my ($entiteittype)      = $xml =~ /entiteittype.*?>(.*?)<\/(\w+:)?entiteit/si;

    unless ($entiteittype) {
        $c->log->error('Entiteittype not found');
        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'Entiteittype not found',
                detail   => 'Entiteittype could not be recognized'
            }
        );

        return;
    }

    my ($module) = Zaaksysteem::Backend::Sysin::Modules->find_module_by_id(
        'stuf' . lc($entiteittype));

    if (!$module) {
        $c->log->error('Module not found: ' . 'stuf' . lc($entiteittype));
        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'StUF module not found',
                detail   => 'StUF module for ' . $entiteittype . ' not found, be sure to configure it',
            }
        );
        return;
    }

    my $rsi = $c->model('DB::Interface')->search_active({module => $module->name});
    my $interface = $rsi->next;
    if ($rsi->next) {
        $c->log->error(sprintf 'Too many interfaces for "%s" configured. Disable one', $module->name);
        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'StUF module not found',
                detail   => 'StUF module for ' . $entiteittype . ' not found, be sure to configure it',
            }
        );
        return;
    }
    elsif (!$interface) {
        $c->log->error('Interface not active: ' . $module->name);
        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'StUF module not active',
                detail   => 'StUF module for ' . $entiteittype . ' not active, be sure to configure it',
            }
        );
        return;
    }

    my $dn                 = $c->engine->env->{SSL_CLIENT_S_DN} || $c->req->header('x-client-ssl-s-dn') // '';
    my $client_fingerprint = $c->engine->env->{SSL_CLIENT_FINGERPRINT} || $c->req->header('x-client-ssl-fingerprint') // '';

    my $certificate_match = try {
        $interface->module_object->verify_client_certificate(
            client_fingerprint => $client_fingerprint,
            dn                 => $dn,

            get_certificate_cb => sub {
                my $config_interface = $interface->module_object->get_config_interface($interface);
                my $certificate = $c->model('DB::Filestore')->find($config_interface->jpath('$.mk_cert[0].id'));

                return $certificate;
            },
        );
    } catch {
        $c->log->error("Error validating client certificate: $_");
    };

    if (!$certificate_match) {
        my $client_cert_override = $c->model('DB::Config')->get_value('disable_stuf_client_certificate_checks');

        if ($client_cert_override) {
            $c->log->warn("Override used: client presented a certificate that doesn't match the configured certificate.");
        } else {
            $c->stash->{soap}->fault(
                {
                    code     => '403',
                    reason   => 'Forbidden',
                    detail   => 'AUTHORIZATION FAILURE: Invalid SSL Certificate'
                }
            );

            return;
        }
    }

    $c->log->debug('Dispatching to interface');

    my ($response, $transaction);
    eval {
        $transaction        = $interface->process({
            input_data                  => $xml,
            external_transaction_id     => 'unknown',       # Will be replaced later
        });

        if ($transaction->transaction_records->count == 1) {
            my $record      = $transaction->transaction_records->first;

            $response       = $record->output;
        }
    };

    if ($@ || !$response) {
        ### XXX Unreadable logic:
        ### If $@ error, show error. Else, if transaction, show transaction_id
        $c->stash->{soap}->fault(
            {
                code     => '500',
                reason   => 'Transaction could not be processed',
                detail   => 'Transaction could not be processed: '
                    . (
                        $@
                            ? $@
                            : (
                                $transaction
                                    ? 'transaction id: ' . $transaction->id
                                    : ''
                            )
                        )
            }
        );

        return;
    }


    if (
        $response &&
        (
            $response =~ /^Error: / ||
            $response =~ /xml version/
        )
    ) {
        $response =~ s/^Error: //;
        $response =~ s/^.*?<\?xml versio/<?xml versio/;
        $response =~ s/> at .* line \d+$/>/;

        return XML::LibXML->load_xml(string => $response)->documentElement();
    }

    $c->stash->{soap}->fault(
        {
            code     => '500',
            reason   => 'No response given',
            detail   => 'Unknown error occured'
        }
    );

    return;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 COMPONENT_KENNISGEVING

TODO: Fix the POD

=cut

=head2 MODEL_SBUS

TODO: Fix the POD

=cut

=head2 bg0204

TODO: Fix the POD

=cut

=head2 endpoint

TODO: Fix the POD

=cut

=head2 handle_kennisgeving

TODO: Fix the POD

=cut

=head2 stuf0204

TODO: Fix the POD

=cut

=head2 stuf0301

TODO: Fix the POD

=cut

=head2 verify_authorization

TODO: Fix the POD

=cut

