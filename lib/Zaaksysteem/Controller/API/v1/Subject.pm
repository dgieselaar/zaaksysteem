package Zaaksysteem::Controller::API::v1::Subject;

use Moose;
use Moose::Util::TypeConstraints qw[enum union];

use DateTime;
use JSON qw[decode_json];

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(SubjectType UUID);

use Zaaksysteem::BR::Subject;
use Zaaksysteem::API::v1::ResultSet;


BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has 'api_capabilities' => (
    is          => 'ro',
    default     => sub { return [qw/extern/] }
);

=head1 NAME

Zaaksysteem::Controller::API::V1::Subject - APIv1 controller for subjects

=head1 DESCRIPTION

This is the controller API class for C<api/v1/subject>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Subject>

Extensive tests about the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Subject>

=head1 ACTIONS

=head2 base

=cut

### XXX TODO (Get inspiration from Controlpanel.pm)
sub base : Chained('/api/v1/base') : PathPart('subject') : CaptureArgs(0) : Scope('subject') {}

=head2 search

/api/v1/subject

=cut

sub search : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;

    return $self->_process_search_results($c);
}

=head2 remote_search

/api/v1/subject/remote_search

=cut

sub remote_search : Chained('base') : PathPart('remote_search') : Args(0) : RO {
    my ($self, $c) = @_;

    throw(
        'api/v1/subject/forbidden',
        'Searching of remote objects only allowed for logged in users'
    ) unless $c->user_exists;  

    return $self->_process_search_results($c, remote_search => 'stuf');
}

=head2 remote_import

/api/v1/subject/remote_import

=cut

sub remote_import : Chained('base') : PathPart('remote_import') : Args(0) : RO {
    my ($self, $c) = @_;

    ### Prevent creation of subjects by unknown users
    throw(
        'api/v1/subject/forbidden',
        'Importing of remote subjects only allowed for logged in users'
    ) unless $c->user_exists;

    my $person = $self->_convert_to_clean_object($c->req->params, drop_dates => 1);

    throw(
        'api/v1/subject/remote_import/incorrect_subject_type',
        'Remote import is only allowed for subject type "persoon"'
    ) unless ($person->{subject_type} eq 'person');

    my $bridge              = $self->bridge($c, remote_search => 'stuf');

    my $object              = $bridge->remote_import($person);

    $c->stash->{result}     = $object;
}


=head2 instance_base

Preperation for /api/v1/subject/UUID


=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    throw('api/v1/subject/invalid_uuid','Invalid UUID given') unless UUID->check($uuid);

    my $bridge              = $self->bridge($c);
    $c->stash->{subject}    = $bridge->find($uuid);

    throw('api/v1/subject/uuid_not_found', 'No subject found by given UUID') unless $c->stash->{subject};
}

=head2 get

/api/v1/subject/UUID

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {}


=head2 create

=head3 URL Path

C</api/v1/subject/create>

=cut

sub create : Chained('base') : PathPart('create') : Args(0) : RW {
    my ($self, $c) = @_;

    ### Prevent creation of subjects by unknown users
    throw(
        'api/v1/subject/forbidden',
        'Creation of subject objects only allowed for logged in users'
    ) unless $c->user_exists;

    my $bridge      = $self->bridge($c);

    my $object      = $bridge->object_from_params($c->req->params);

    $c->model('DB')->txn_do(sub {
        $bridge->save($object);
    });

    $c->stash->{result} = $object;
}

=head1 PRIVATE METHODS

=head2 bridge

Returns the Subject Bridge

=cut

sub bridge {
    my ($self, $c) = @_;

    return Zaaksysteem::BR::Subject->new(
        schema          => $c->model('DB')->schema,
        @_
    );
}

=head2 _get_search_params_from_dsl

    {
        query   => {
            match   => {
                'subject_type'              => 'personal',
                'subject.personal_number'   =>  '54568788',
            }
        }
    }

Turns a "elasticsearch"-style hash into a query for our subject bridge

=cut

sub _get_search_params_from_dsl {
    my ($self, $params) = @_;

    throw(
        'api/v1/subject/invalid_dsl',
        'Invalid search query, supply "match" as a key-value object'
    ) unless ($params->{match} && ref $params->{match} eq 'HASH');

    my %params;
    for my $key (keys %{ $params->{match} }) {
        $params{$key} = $params->{match}->{$key};
    }

    return \%params;
}

=head2 _process_search_results

    $self->_process_search_results($c, remote_search => 'stuf');
    $self->_process_search_results($c);

=cut

sub _process_search_results {
    my ($self, $c, %opts) = @_;

    my $bridge              = $self->bridge($c, %opts);

    my $search_params       = $self->_get_search_params_from_dsl($c->req->params->{query});

    if ($opts{remote_search}) {
        my @rs                  = $bridge->search($search_params);

        $c->stash->{set}        = Zaaksysteem::API::v1::ArraySet->new(
            content => \@rs,
            allow_rows_per_page => 50,  # Normally defaults to 20, defines the max amount of "?rows_per_page=value" to
                                        # prevent abusers from pulling too much data in 1 query.
        );
    } else {
        my $rs                  = $bridge->search($search_params);

        $c->stash->{set}        = Zaaksysteem::API::v1::ResultSet->new(
            iterator => $rs,
            allow_rows_per_page => 50,  # Normally defaults to 20, defines the max amount of "?rows_per_page=value" to
                                        # prevent abusers from pulling too much data in 1 query.
        );
    }

    $c->stash->{result}     = $c->stash->{set}->init_paging($c->req);
}

=head2 _convert_to_clean_object

    # Transforms:
    {
        type      => 'subject',
        reference => 'ab89a7b-97a8b9ab7-79a07a0b-8797897',
        instance => {
            subject => {
                instance => {
                    personal_number => 123456789,
                    surname         => 'Fuego',
                }
                type     => 'person',
                reference => 'ab89a7b-97a8b9ab7-79a07a0b-8896728342',
            }
            subject_type => 'person'
        }
    }

    # Into
    # TODO: Transfor reference to "id"
    {
        subject => {
            personal_number => 123456789,
            surname         => 'Fuego',
        }
        subject_type => 'person'
    }

Will transform a typical API-v1 structure in a plain structure which can be handled by our bridge.

=cut

sub _convert_to_clean_object {
    my $self    = shift;
    my $param   = shift;
    my %opts    = @_;

    ### Convert arrays
    if (ref $param eq 'ARRAY') {
        return [ map { $self->_convert_to_clean_object($_, @_) } @$param ];
    }

    ### Convert special hashes
    return $param unless ref $param eq 'HASH';

    if (defined $param->{type} && $param->{type} eq 'set' && $param->{instance}->{rows}) {
        return [ grep { $self->_convert_to_clean_object($_->{instance}, @_) } @{ $param->{instance}->{rows} } ];
    }

    if (defined $param->{type} && exists $param->{reference}) {
        return unless $param->{instance};

        return $self->_convert_to_clean_object($param->{instance}, @_);
    }

    my @keys;
    if ($opts{drop_dates}) {
        @keys = grep { $_ !~ /^(?:date_created|date_modified)$/ } keys %$param;
    } else {
        @keys = keys %$param;
    }

    return { map { $_ => $self->_convert_to_clean_object($param->{$_}, @_) } @keys };
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
