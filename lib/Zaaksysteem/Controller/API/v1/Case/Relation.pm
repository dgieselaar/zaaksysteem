package Zaaksysteem::Controller::API::v1::Case::Relation;
use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw(UUID);

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

has api_capabilities => (
    is          => 'ro',
    default     => sub { return [qw/intern/] }
);

has api_control_module_types => (
    is => 'rw',
    default => sub { [ 'api' ] },
);

=head1 NAME

Zaaksysteem::Controller::API::v1::Case::Relation - APIv1 controller for case relations

=head1 DESCRIPTION

This is a controller API class for C<api/v1/case>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Case>

Tests showing the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Case>

=head1 ACTIONS

=head2 relation_base

Base for the relation modification endpoints. Retrieves the case object and
that of the "related case" being referenced, and puts both on the stash.

=cut

define_profile relation_base => (
    required => {
        related_id => UUID,
    },
);

sub relation_base : Chained('/api/v1/case/instance_base') : PathPart('relation') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    $c->stash->{ related_case_object } = $self->_get_case($c, $opts->{related_id});

    $c->stash->{ zaak } = try {
        $c->stash->{ case }->get_source_object
    } catch {
        $c->log->error($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval failed, unable to continue.'
        ));
    };

    $c->stash->{ related_case } = try {
        $c->stash->{ related_case_object }->get_source_object
    } catch {
        $c->log->error($_);

        throw('api/v1/case/retrieval_fault', sprintf(
            'Case retrieval fault, unable to continue.',
        ));
    };
}

=head2 add

Add a new relation with the case specified in the C<related_id> parameter.

=cut

sub add : Chained('relation_base') : PathPart('add') : Args(0) : RW {
    my ($self, $c) = @_;

    $c->model('DB::CaseRelation')->add_relation(
        $c->stash->{zaak}->id,
        $c->stash->{related_case}->id,
    );

    $c->stash->{zaak}->_touch();
    $c->stash->{related_case}->_touch();

    $c->stash->{case}->discard_changes();

    $c->detach('/api/v1/case/get');
}

=head2 delete

Remove the relation with the case specified in the C<related_id> parameter.

=cut

sub delete : Chained('relation_base') : PathPart('remove') : Args(0) : RW {
    my ($self, $c) = @_;

    my $relation = $c->model('DB::CaseRelation')->get(
        $c->stash->{zaak}->id,
        $c->stash->{related_case}->id,
    );

    unless ($relation) {
        throw('api/v1/case/relation/retrieval_fault', sprintf(
            'Case relation to %s could not be retrieved. Unable to continue.',
            $c->stash->{related_case}->id,
        ));
    }

    $relation->delete();

    $c->model('DB::CaseRelation')->recalculate_order($c->stash->{zaak}->id);

    $c->stash->{zaak}->_touch();
    $c->stash->{related_case}->_touch();

    $c->stash->{case}->discard_changes();
    $c->detach('/api/v1/case/get');
}

=head2 move

Change the order of relations with other cases, moving the relation with the
case specified in C<related_id> to after the relation with C<after> (or to the
beginning of C<after> is not specified).

=cut

define_profile move => (
    optional => {
        after => UUID,
    },
);

sub move : Chained('relation_base') : PathPart('move') : Args(0) : RW {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    my $after_rel;
    if ($opts->{after}) {
        my $after_case_object = $self->_get_case($c, $opts->{after});
        my $after_case = try {
            $after_case_object->get_source_object
        } catch {
            $c->log->error($_);

            throw('api/v1/case/retrieval_fault', sprintf(
                'Case retrieval failed, unable to continue.'
            ));
        };

        $after_rel = $self->_get_relation(
            $c,
            $c->stash->{zaak}->id,
            $after_case->id
        );
    }

    my $relation = $self->_get_relation(
        $c,
        $c->stash->{zaak}->id,
        $c->stash->{related_case}->id
    );

    my @related_cases = $c->model('DB::CaseRelation')->get_sorted($c->stash->{zaak}->id);

    $c->log->debug(sprintf(
        "Order before move: %s",
        join(', ', map { $_->to_string } @related_cases)
    ));

    my ($hijacked) = grep { $_->id eq $relation->id } @related_cases;
    @related_cases = grep { $_->id ne $relation->id } @related_cases;

    my @sorted_cases;

    $c->log->debug(sprintf(
        'Moving relation %s after %s',
        $hijacked->to_string,
        $after_rel ? $after_rel->to_string : 'null'
    ));

    if($after_rel) {
        @sorted_cases = map {
            $_->id eq $after_rel->id ? ($_, $hijacked) : ($_)
        } @related_cases;
    } else {
        @sorted_cases = ($hijacked, @related_cases);
    }

    $c->log->debug(sprintf(
        'New order after move: %s',
        join(', ', map { $_->to_string } @sorted_cases)
    ));

    my $iter = 1;

    for my $rel (@sorted_cases) {
        $rel->order_seq($iter);
        $rel->update;

        $iter++;
    }

    $c->stash->{zaak}->_touch();
    $c->stash->{case}->discard_changes();

    $c->detach('/api/v1/case/get');
}

sub _get_case {
    my ($self, $c, $uuid) = @_;

    my $case = try {
        $c->stash->{ cases }->find($uuid);
    } catch {
        $c->log->error($_);

        throw('api/v1/case/relation/retrieval_fault', sprintf(
            'Related case retrieval fault, unable to continue.',
        ));
    };

    unless (defined $case) {
        throw('api/v1/case/not_found', sprintf(
            "The case related object with UUID '%s' could not be found.",
            $uuid,
        ), { http_code => 404 });
    }

    unless ($case->object_class eq 'case') {
        throw('api/v1/case/relation/not_found', sprintf(
            "The case related object with UUID '%s' could not be found.",
            $uuid,
        ), { http_code => 404 });
    }

    return $case;
}

sub _get_relation {
    my ($self, $c, $a, $b) = @_;

    my $relation = $c->model('DB::CaseRelation')->get($a, $b);

    unless ($relation) {
        throw('api/v1/case/relation/retrieval_fault', sprintf(
            "Case relation between '%s' and '%s' could not be found, unable to continue.",
            $a, $b
        ));
    }

    return $relation;
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
