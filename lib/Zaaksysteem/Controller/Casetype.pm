package Zaaksysteem::Controller::Casetype;

use Moose;
use namespace::autoclean;

use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::Casetype - ZAPI Controller

=head1 SYNOPSIS

 # /casetype

=head1 DESCRIPTION

Zaaksysteem API Controller for Casetypes

=head1 METHODS

=head2 /casetype [GET READ]

Returns a resultset of zaaktypen, possibly filtered by a query filter

B<Query Parameters>

=over 4

=item query

Type: STRING

 # /casetype?query=test

Filters the results with the given query string

=back

=cut

sub index
    : Chained('/')
    : PathPart('casetype')
    : Args(0)
    : ZAPI
{
    my ($self, $c) = @_;

    throw('httpmethod/post', 'Invalid HTTP Method, no GET', []) unless
        lc($c->req->method) eq 'get';

    my $offline = $c->req->params->{inactive} ? 1 : 0;

    my $search = {
            "me.deleted" => undef,
            "me.active" => (
                $offline
                    ? ([0, 1])
                    : (1)
            ),
        };
    my $query = $c->req->params->{query} || '';
    if ($query) {
        $search->{search_term} = { 'ilike' => '%' . lc($query) . '%' },
    }

    $c->stash->{zapi} = $c->model('DB::Zaaktype')->search($search);
}

sub base
    : Chained('/')
    : PathPart('casetype')
    : CaptureArgs(1)
{
    my ($self, $c, $id)     = @_;

    unless ($id eq 'all') {
        $c->stash->{zaaktype}   = $c->model('DB::Zaaktype')->find($id);

        $c->error('Invalid zaaktype id given') unless $c->stash->{zaaktype};
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 base

TODO: Fix the POD

=cut

=head2 index

TODO: Fix the POD

=cut

