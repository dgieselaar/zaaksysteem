package Zaaksysteem::Access;

use Moose::Exporter;

Moose::Exporter->setup_import_methods(
    with_meta => ['access_profile', 'default_profile'],
    class_metaroles => {
        class => ['Zaaksysteem::Metarole::AccessProfileTrait']
    }
);

sub default_profile {
    shift->_default_security_profile(@_);
}

sub access_profile {
    my ($meta, %profiles) = @_;

    my %defined_profiles = %{ $meta->_security_profiles || {} };

    for my $key (keys %profiles) {
        $defined_profiles{ $key } = $profiles{ $key };
    }

    $meta->_security_profiles(\%defined_profiles);

    $meta->_init_class_modifiers;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 access_profile

TODO: Fix the POD

=cut

=head2 default_profile

TODO: Fix the POD

=cut

