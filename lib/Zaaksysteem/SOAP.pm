package Zaaksysteem::SOAP;
use Moose;

=head1 NAME

Zaaksysteem::SOAP - A SOAP client for Zaaksysteem

=head1 SYNOPSIS

    use Zaaksysteem::SOAP;
    my $client = Zaaksysteem::SOAP->new(
        wsdl_file       => $file,
        home            => catfile(qw(/ vagrant)),
        soap_endpoint   => 'https://10.44.0.11',
        soap_ssl_key    => 'key',
        soap_ssl_cert   => 'cert',
        soap_ssl_verify => 1, # defaults to 0,
    );

=cut

use FindBin qw/$Bin/;

use File::Spec::Functions;

use XML::Compile::WSDL11;      # use WSDL version 1.1
use XML::Compile::SOAP11;      # use SOAP version 1.1
use XML::Compile::Transport::SOAPHTTP;
use XML::Compile::Translate::Reader;
use XML::Compile::Util;
use XML::Tidy;

use Zaaksysteem::Tools;

with 'MooseX::Log::Log4perl';

has wsdl_file => (
    is       => 'rw',
    isa      => 'Str|ArrayRef[Str]',
    required => 1,
);

has home => (
    is      => 'rw',
    lazy    => 1,
    default => sub {
        return $Bin . '/..';
    }
);

has soap_endpoint => (
    is => 'rw',
);

has soap_port => (
    is => 'rw',
);

has soap_service => (
    is => 'rw',
);

has soap_action => (
    is => 'rw',
);

has is_test => (
    is => 'rw',
);

=head2 test_responses (HASHREF)

Use this to stock the SOAP client with preset xml responses for given
calls. The SOAP server will not talk to the actual server, but will
pretend the server has returned the provided XML. Useful for regression
testing.

Works on a per-call basis, so for every call this hash will be consulted,
if there's an answer that is used, otherwise, business as usual.

    my $soap = new Zaaksysteem::SOAP({
        test_responses => {
            call1 => '<?xml><bla>1</bla>',
            call2 => '<?xml><bla>2</bla>'
        }
    });

=cut

has test_responses => (
    is       => 'rw',
    default  => sub { {} },
    lazy => 1,
);

has 'reader_writer_config'  => (
    'is'        => 'rw',
    'default'   => sub { return {}; }
);

has 'xml_definitions' => (
    'is'      => 'rw',
    'default' => sub { return []; }
);

has 'soap_ssl_key' => (
    'is' => 'rw',
);

has 'soap_ssl_crt' => (
    'is' => 'rw',
);

has soap_ssl_verify => (
    is      => 'ro',
    isa     => 'Bool',
    default => 0,
);


has wsdl => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;

        my %opts = ();
        $opts{opts_readers} = $self->reader_writer_config->{READER}
            if $self->reader_writer_config->{READER};
        $opts{opts_writers} = $self->reader_writer_config->{WRITER}
            if $self->reader_writer_config->{WRITER};

        my $wsdl;
        if (ref $self->wsdl_file eq 'ARRAY') {
            $wsdl = XML::Compile::WSDL11->new(undef, %opts);
            foreach (@{ $self->wsdl_file }) {
                $wsdl->addWSDL($self->_file_basename($_));
            }
        }
        else {
            $wsdl = XML::Compile::WSDL11->new(
                $self->_file_basename($self->wsdl_file), %opts);
        }

        $wsdl->importDefinitions($self->xml_definitions);
        return $wsdl;
    }
);

sub _file_basename {
    my ($self, $filename) = @_;
    $filename = $filename =~ /^\// ? $filename : catfile($self->home, qw(share wsdl), $filename);
    unless (-f $filename) {
        $self->log->error("Unable to open $filename");
        throw('SOAPClient/file/does_not_exist',
            "Unable to open file $filename");
    }
    return $filename;
}

=head2 transport

TODO: Fix the POD

=cut

sub transport {
    my $self         = shift;

    my $transport   = XML::Compile::Transport::SOAPHTTP->new(
        'address'   => $self->soap_endpoint,
        'timeout'   => 25,
    );

    if ($self->soap_ssl_key && $self->soap_ssl_crt) {
        $self->log->trace("Setting SSL key to " . $self->soap_key_file);
        $transport->userAgent->ssl_opts(
            'SSL_key_file' =>
                $self->soap_ssl_key
        );

        $self->log->trace("Setting SSL cert to " . $self->soap_cert_file);
        $transport->userAgent->ssl_opts(
            'SSL_cert_file' =>
                $self->soap_ssl_crt
        );

        $self->log->trace("Setting SSL verify to " . $self->soap_ssl_verify);
        $transport->userAgent->ssl_opts(
            'verify_hostname' => $self->soap_ssl_verify,
        );
    }

    if ($self->soap_endpoint && $self->soap_endpoint =~ /^https/) {
        $self->log->trace("Setting SSL verify to " . $self->soap_ssl_verify);
        $transport->userAgent->ssl_opts(
            verify_hostname => $self->soap_ssl_verify,
        );
    }

    if ($self->soap_action) {
        $self->log->trace("Setting default header 'SOAPAction' to " . $self->soap_action);
        $transport->userAgent->default_header('SOAPAction', $self->soap_action);
    }

    return $transport;
}

=head2 get_dispatch_xml

TODO: Fix the POD

=cut

sub get_dispatch_xml {
    my $self                        = shift;
    my ($call, $params)             = @_;

    my $client_options = {
        transport_hook => sub {
            my $xml = shift;
            return $xml;
        },
    };

    my ($answer, $trace) = $self->_dispatch_call(
        {
            call           => $call,
            params         => $params,
            client_options => $client_options,
        }
    );

    return $trace->request->content;
}

=head2 dispatch

TODO: Fix the POD

=cut

sub dispatch {
    my $self = shift;
    my ($call, $params) = @_;

    my $client_options = {
        transport => $self->transport(),
    };

    # when creating this object, you can pass a list of test responses.
    # if one of these hardcoded responses is requested, it is returned
    # instead of contacting the remote server. the purpose is to be able
    # to test a large part of the chain without invoking the actual remote
    # server, because that may be both impolite (spammy) and change data
    if (my $fake = $self->test_responses->{$call}) {
        $self->log->info("Calling test response for $call");
        $client_options->{transport_hook} = sub {
            return HTTP::Response->new(
                200,
                'Mocking the answers',
                ['Content-Type' => 'text/xml'],
                $fake,
            );
            }
    }

    $self->log->info(sprintf('Calling %s with params %s', $call, dump_terse($params)));
    my ($answer, $trace) = $self->_dispatch_call(
        {
            call           => $call,
            params         => $params,
            client_options => $client_options,
        }
    );

    if (!$answer) {
        my $msg = sprintf('Error calling %s with params %s: %s', $call, dump_terse($params), $trace->error);
        $self->log->error($msg);
        throw('stuf/soap/no_answer', $msg);
    }

    return ($answer, $trace);
}

define_profile _dispatch_call => (
    required        => [qw/call params client_options/],
);

sub _dispatch_call {
    my $self                        = shift;
    my $options                     = assert_profile(shift)->valid;

    my %soap_options = (
        $self->soap_service ? (service => $self->soap_service) : (),
        $self->soap_port    ? (port    => $self->soap_port)    : (),
        operation => $options->{call},
        transport => $options->{client_options}{transport}->compileClient(hook => $options->{client_options}{transport_hook}),
    );

    if ($self->log->is_trace) {
        $self->log->trace("Dispatching " . dump_terse(\%soap_options));
    }

    my $client = $self->wsdl->compileClient(%soap_options);

    my ($answer, $trace) = $client->($options->{params});
    return ($answer, $trace);
}

=head2 $soap->from_xml($XML)

Return value: $HASH_REF

    my $perl = $soap->from_xml('<xml><bla>test</bla>');

    # returns: { bla => 1 }

Convert an xml string into a perl datastructure. The idea is that when
you send a SOAP request and it's not received succesfully, you want to
fire the exact same request at a later time. Xml is a clean way to serialize,
so the xml is stored in the transaction table and revived a later time.
Since the SOAP transporter works from a Perl structure, this converts back.
Wouldn't it be possible to send the XML directly without further back-and-
forth conversion.

B<Return HASHREF>

=over 4

=item content

The perl structure compiled from xml

=item name

The xml node name

=back

=cut

sub from_xml {
    my $self    = shift;
    my $xml     = shift;

    die('xml needs to be set to be able to transform xml')
        unless $xml;

    my $doc     = XML::LibXML->load_xml(string => $xml);

    my ($node)  = $doc->findnodes('//soap:Envelope/soap:Body/*');

    my $elem    = pack_type $node->namespaceURI, $node->localName;

    my $reader  = $self->wsdl->compile(
        READER => $elem,
        %{ $self->reader_writer_config->{READER} }
    );

    return {
        name    => $node->localName,
        content => $reader->($node)
    };
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
