package Zaaksysteem::DB::Component::NatuurlijkPersoon;
use Moose;

use base qw/Zaaksysteem::DB::Component::GenericNatuurlijkPersoon/;

use Zaaksysteem::Backend::Subject::Naamgebruik qw/naamgebruik/;

extends 'Zaaksysteem::Backend::Component';
with qw/
    MooseX::Log::Log4perl
    Zaaksysteem::BR::Subject::Component
/;

=head2 insert

TODO: Fix the POD

=cut

sub insert {
    my ($self) = @_;

    # we need the database to generate the new searchable_id param.
    # if we don't supply it, it will take its chance. so get rid
    # of whatever the app thinks it should pass.
    # is there a way to put this in the Schema? that's be less hacky.
    delete $self->{_column_data}->{searchable_id};

    $self->naamgebruik($self->achternaam);
    $self->_set_search_term();

    return $self->next::method(@_);
}

=head2 update

TODO: Fix the POD

=cut

sub update {
    my $self    = shift;
    my $params  = shift;

    if ($params && UNIVERSAL::isa($params, 'HASH')) {
        for my $key (keys %{ $params }) {
            $self->$key($params->{ $key });
        }
    }

    $self->naamgebruik($self->achternaam);
    $self->_set_search_term();

    return $self->next::method(@_);
}

=head2 bid

Convenience method to get the betrokkene ID of a natural person.

=cut

sub bid {
    my $self = shift;
    return 'betrokkene-natuurlijk_persoon-' . $self->id;
}

=head2 bsn

Convenience method to get the 9 digit BSN from a burgerservice number.
Returns 0 if there is no burgerservicenummer found.

=cut

sub bsn {
    my $self = shift;
    if ($self->burgerservicenummer) {
        return sprintf('%09d', int($self->burgerservicenummer));
    }
    return 0;
}


sub _set_search_term {
    my ($self) = @_;

    my $search_term = '';

    if ($self->deleted_on) {
        $self->search_term($search_term);
        return;
    }

    if($self->voornamen) {
        $search_term .= $self->voornamen . ' ';
    }
    if($self->voorvoegsel) {
        $search_term .= $self->voorvoegsel . ' ';
    }
    if($self->achternaam) {
        $search_term .= $self->achternaam . ' ';
    }
    if($self->burgerservicenummer) {
        $search_term .= $self->bsn . ' ';
    }
    if($self->geboortedatum) {
        my $geboortedatum = $self->geboortedatum;
        $geboortedatum =~ s|T.*$||is;
        my ($y, $m, $d) = split /-/, $geboortedatum;

        my $birthday_dmy = "$d-$m-$y";

        $search_term .= $geboortedatum . ' '. $birthday_dmy . ' ';
    }
    if($self->adres_id && $self->adres_id->straatnaam) {
        $search_term .= $self->adres_id->straatnaam . ' ';
    }
    if($self->adres_id && $self->adres_id->huisnummer) {
        $search_term .= $self->adres_id->huisnummer;
        if($self->adres_id->huisnummertoevoeging) {
            $search_term .= $self->adres_id->huisnummertoevoeging;
        }
        $search_term .= ' ';
    }
    if($self->adres_id && $self->adres_id->woonplaats) {
        $search_term .= $self->adres_id->woonplaats . ' ';
    }
    if($self->adres_id && $self->adres_id->postcode) {
        $search_term .= $self->adres_id->postcode . ' ';
    }

    $search_term .= $self->get_contact_data_search_term;

    $self->search_term($search_term);
    $self->search_order($self->naamgebruik . ' ' . $search_term);
}

=head2 get_contact_data_search_term

TODO: Fix the POD

=cut

sub get_contact_data_search_term {
    my $self = shift;

    my $contact_data = $self->result_source->schema->resultset('ContactData')->search({
        gegevens_magazijn_id => $self->id,
        betrokkene_type => 1 # 1 for natuurlijk_persoon, 2 for bedrijf
    })->first;

    return $contact_data && $contact_data->email || '';
}

=head2 achternaam

Returns the achternaam based on naamsgebruik.

=cut


sub achternaam {
    my ($self) = @_;

    return naamgebruik({
        aanduiding => $self->aanduiding_naamgebruik || '',
        partner_voorvoegsel   => $self->partner_voorvoegsel,
        partner_geslachtsnaam => $self->partner_geslachtsnaam,
        voorvoegsel           => $self->voorvoegsel,
        geslachtsnaam         => $self->geslachtsnaam,
    });
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
