package Zaaksysteem::Model::Geo;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory';

__PACKAGE__->config(
    class       => 'Zaaksysteem::Geo',
    constructor => 'new',
);


sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        'key'       => $c->config->{google_api_key},
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 prepare_arguments

TODO: Fix the POD

=cut

